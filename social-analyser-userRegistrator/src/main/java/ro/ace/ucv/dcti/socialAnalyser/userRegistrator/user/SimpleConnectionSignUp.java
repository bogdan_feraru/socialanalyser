package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;

import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

/**
 * Simple little {@link ConnectionSignUp} command that allocates new userIds in memory.
 *
 * @author Bogdan Feraru
 */
public final class SimpleConnectionSignUp implements ConnectionSignUp {

  public static final String SELECT_MAX_USER_ID =
      "select MAX(CAST(userconnection.userId AS UNSIGNED)) from social_analyser_clients.userconnection;";

  private final AtomicLong userIdSequence = new AtomicLong();

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public String execute(Connection<?> connection) {
    return Long.toString(userIdSequence.incrementAndGet());
  }

  @PostConstruct
  public void initialize() {
    Long greatestId = jdbcTemplate.queryForObject(SELECT_MAX_USER_ID, (resultSet, i) -> resultSet.getLong(1));
    userIdSequence.set(greatestId);
  }
}
