package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user.UserInterceptor;

/**
 * Spring MVC Configuration.
 *
 * @author Bogdan Feraru
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  @Autowired
  private UsersConnectionRepository usersConnectionRepository;

  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new UserInterceptor(usersConnectionRepository));
  }

  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/signin");
    registry.addViewController("/signout");
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
  }

  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
    internalResourceViewResolver.setPrefix("/WEB-INF/views/");
    internalResourceViewResolver.setSuffix(".jsp");
    return internalResourceViewResolver;
  }
}
