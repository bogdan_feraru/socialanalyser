package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user;

import org.springframework.web.util.CookieGenerator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Utility class for managing the social_analyser_user user cookie that remembers the signed-in user.
 *
 * @author Bogdan Feraru
 */
final class UserCookieGenerator {

  public static final String SOCIAL_ANALYSER_USER = "social_analyser_user1";

  private final CookieGenerator userCookieGenerator = new CookieGenerator();

  public UserCookieGenerator() {
    userCookieGenerator.setCookieName(SOCIAL_ANALYSER_USER);
  }

  public void addCookie(String userId, HttpServletResponse response) {
    userCookieGenerator.addCookie(response, userId);
  }

  public void removeCookie(HttpServletResponse response) {
    userCookieGenerator.addCookie(response, "");
  }

  public String readCookieValue(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      return null;
    }
    for (Cookie cookie : cookies) {
      if (cookie.getName().equals(userCookieGenerator.getCookieName())) {
        return cookie.getValue();
      }
    }
    return null;
  }

}
