package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

  private static final String SELECT_USER_IDS = "SELECT userconnection.userId FROM userconnection;";
  private static final String SELECT_FROM_USERCONNECTION = "SELECT * FROM userconnection;";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<Long> getAllUserId() {
    return jdbcTemplate.queryForList(SELECT_USER_IDS, Long.class);
  }
}
