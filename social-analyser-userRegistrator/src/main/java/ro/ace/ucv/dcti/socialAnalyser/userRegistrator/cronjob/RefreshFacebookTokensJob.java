package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.cronjob;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user.UserRepository;

public class RefreshFacebookTokensJob {

  private static final String FACEBOOK = "facebook";
  private final UserRepository userConnectionRepository;
  private final UsersConnectionRepository usersConnectionRepository;

  public RefreshFacebookTokensJob(UserRepository userConnectionRepository,
                                  UsersConnectionRepository usersConnectionRepository) {
    this.userConnectionRepository = userConnectionRepository;
    this.usersConnectionRepository = usersConnectionRepository;
  }

  public void run() {
    List<Long> allUserId = userConnectionRepository.getAllUserId();
    for (Long userId : allUserId) {
      ConnectionRepository connectionRepository =
          usersConnectionRepository.createConnectionRepository(String.valueOf(userId));
      Connection<?> connection = connectionRepository.getPrimaryConnection(Facebook.class);
      connection.refresh();
      connectionRepository.updateConnection(connection);
    }
  }
}
