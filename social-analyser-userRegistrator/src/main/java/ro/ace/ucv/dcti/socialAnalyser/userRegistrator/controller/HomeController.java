package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * Simple little @Controller that invokes Facebook and renders the result.
 * The injected {@link Facebook} reference is configured with the required authorization credentials for the current
 * user behind the scenes.
 *
 * @author Bogdan Feraru
 */
@Controller
public class HomeController {

  private final Facebook facebook;

  @Autowired
  public HomeController(Facebook facebook) {
    this.facebook = facebook;
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String home(Model model) {
    List<Reference> friends = facebook.friendOperations().getFriends();
    String userName = facebook.userOperations().getUserProfile().getName();
    model.addAttribute("userName", userName);
    model.addAttribute("friends", friends);
    return "home";
  }

  @RequestMapping(value = "/info", method = RequestMethod.GET, produces = "application/json")
  public String profile(Model model) throws JsonProcessingException {
    User userProfile = facebook.userOperations().getUserProfile();
    ObjectMapper objectMapper = new ObjectMapper();
    String userStr = objectMapper.writeValueAsString(userProfile);
    model.addAttribute("information", userStr);
    return "jsonView";
  }
}
