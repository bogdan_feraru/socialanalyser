package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.config;

import com.mysql.cj.jdbc.MysqlDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

import ro.ace.ucv.dcti.socialAnalyser.userRegistrator.user.UserRepository;


/**
 * Main configuration class for the application.
 * Turns on @Component scanning, loads externalized application.properties, and sets up the database.
 *
 * @author Bogdan Feraru
 */
@Configuration
@ComponentScan(basePackages = "ro.ace.ucv.dcti.socialAnalyser.userRegistrator", excludeFilters = {
    @ComponentScan.Filter(Configuration.class)})
@EnableScheduling
@PropertySource("classpath:userTokenDataSource.properties")
@PropertySource("classpath:application.properties")
public class MainConfig {

  @Autowired
  private UserRepository userConnectionRepository;

  @Bean
  public DataSource dataSource(Environment env) {
    MysqlDataSource mysqlDataSource = new MysqlDataSource();

    mysqlDataSource.setURL(env.getProperty("user.token.database.url"));
    mysqlDataSource.setUser(env.getProperty("user.token.database.user"));
    mysqlDataSource.setPassword(env.getProperty("user.token.database.password"));

    return mysqlDataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(DataSource dataSource) {
    JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    return jdbcTemplate;
  }

//  @Autowired
////  @Scheduled(fixedDelay = 10000)
//  public void doSomething(UserRepository userConnectionRepository,
//                          UsersConnectionRepository userConnectionsRegistry) {
//    RefreshFacebookTokensJob refreshFacebookTokensJob =
//        new RefreshFacebookTokensJob(userConnectionRepository, userConnectionsRegistry);
//    refreshFacebookTokensJob.run();
//    System.out.println("**************asdlkajsdljasd asdlkjasdlkja");
//  }
}
