package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.User;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestCalls {

  private final Facebook facebook;

  @Autowired
  public RestCalls(Facebook facebook) {
    this.facebook = facebook;
  }

  @RequestMapping(value = "/user", method = RequestMethod.GET)
  public User user(Model model) {
    User userProfile = facebook.userOperations().getUserProfile();
    return userProfile;
  }

  @RequestMapping(value = "/checkin", method = RequestMethod.GET)
  public PagedList<Post> checkin(Model model) {
    facebook.userOperations();
    PagedList<Post> checkins = facebook.feedOperations().getFeed();
    return checkins;
  }
}
