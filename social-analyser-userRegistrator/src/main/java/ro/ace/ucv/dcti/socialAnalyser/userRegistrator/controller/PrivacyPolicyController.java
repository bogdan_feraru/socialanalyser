package ro.ace.ucv.dcti.socialAnalyser.userRegistrator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Controller
public class PrivacyPolicyController {

  @RequestMapping("privacy")
  public String privacyPolicy() {
    return "register";
  }
}
