<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <base href="/"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Social Analyser Registration Page</title>
  <link href="../../assets/css/bootstrap.css" rel="stylesheet">
  <link href="<s:url value="../../assets/css/font-awesome.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/docs.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/bootstrap-social.css"/>" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
  <div class="page-header">
    <h1>Welcome to Social Analyser</h1>
  </div>
  <div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <form id="tw_signin" action="<c:url value="/signin/facebook"/>" method="POST">
        <input type="hidden" name="scope"
               value="user_hometown"/>
        <a type="submit" class="btn btn-block btn-social btn-facebook">
          <span class="fa fa-facebook"></span> Register with Facebook
        </a>
      </form>
    </div>
    <div class="col-sm-4"></div>
  </div>
</div>

</body>
</html>
