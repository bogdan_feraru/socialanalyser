<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <base href="/"/>
  <title>Sign in</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Social Analyser Registration Page</title>
  <link href="../../assets/css/bootstrap.css" rel="stylesheet">
  <link href="<s:url value="../../assets/css/font-awesome.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/docs.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/bootstrap-social.css"/>" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
  <div class="page-header">
    <h1 align="center">Welcome to Social Analyser</h1>
  </div>
  <div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <p>If you wanna help a student please register! Thank you!</p>
      <form id="tw_signin" action="<c:url value="/signin/facebook"/>" method="POST">
        <input type="hidden" name="scope"
               value="email,user_hometown,user_religion_politics,user_likes,
               user_status,user_about_me,user_location,user_tagged_places,
               user_birthday,user_photos,user_videos,user_education_history,
               user_posts,user_website,user_friends,user_relationship_details,
               user_work_history,user_games_activity,user_relationships,
               user_actions.books,user_actions.music,user_actions.video,
               user_actions.fitness,user_actions.news,read_custom_friendlists"/>
        <button type="submit" class="btn btn-block btn-social btn-facebook">
          <span class="fa fa-facebook"></span> Register with Facebook
        </button>
      </form>
    </div>
    <div class="col-sm-4"></div>
  </div>
</div>

</body>
</html>
