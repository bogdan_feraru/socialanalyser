<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <base href="/"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Social Analyser Registration Page</title>
  <link href="../../assets/css/bootstrap.css" rel="stylesheet">
  <link href="<s:url value="../../assets/css/font-awesome.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/docs.css"/>" rel="stylesheet">
  <link href="<s:url value="../../assets/css/bootstrap-social.css"/>" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <h2>THANK YOU ${userName} !!!</h2>
      <p>You can see a list of your friends who also registered for the study below: </p>
      <c:if test="${not empty friends}">
        <ul>
          <c:forEach var="friend" items="${friends}">
            <li>
              <img src="<s:url value="http://graph.facebook.com/${friend.id}/picture"/>" align="middle">
              <span>${friend.name}</span>
            </li>
            <br>
          </c:forEach>
        </ul>
      </c:if>
    </div>
    <div class="col-sm-4"></div>
  </div>
</div>

</body>
</html>
