package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public interface DomainUserRepository extends GraphRepository<User> {

  @Query("MATCH (user:USER)-[:HAS]->(userStatus:USER_STATUS) WHERE userStatus.userStatusType='APPLICATION_USER' RETURN user;")
  List<User> findAllApplicationUsers();
}
