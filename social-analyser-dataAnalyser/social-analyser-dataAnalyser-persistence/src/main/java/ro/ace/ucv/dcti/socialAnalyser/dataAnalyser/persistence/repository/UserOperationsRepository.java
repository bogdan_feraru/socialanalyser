package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionsPerUserPerTypeCount;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.RelationshipStrength;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserReactionsCount;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserSocialRank;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
public interface UserOperationsRepository extends GraphRepository<User> {

  @Query("MATCH (user:USER)-[:HAS]->(userFeed:USER_FEED)-[:HAS]->(post:POST) WHERE user.facebookId={0} "
         + "WITH post MATCH (post)-[:HAS]->(reactions:REACTIONS)-[:HAS]->(reaction:REACTION)-[:HAS]->(reactionType:REACTION_TYPE)"
         + " RETURN reactionType.name AS reactionType, count(reaction) AS reactionCount ORDER BY reactionCount DESC;")
  List<UserReactionsCount> getReceivedUserReactionCount(String userFacebookId);

  @Query(
      "MATCH (user:USER)<-[:CREATED_BY]-(reaction:REACTION)-[:HAS]->(reactionType:REACTION_TYPE) WHERE user.facebookId={0} "
      + "RETURN reactionType.name AS reactionType, count(reaction) AS reactionCount ORDER BY reactionCount DESC;")
  List<UserReactionsCount> getGivenUserReactionCount(String userFacebookId);

  @Query(
      "MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(:REACTION)-[:CREATED_BY]->(userWhoReacted:USER) WHERE user.facebookId={0}"
      + "WITH  DISTINCT user, userWhoReacted "
      + "MATCH (user)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(reaction:REACTION)-[:CREATED_BY]->(userWhoReacted) "
      + "WITH user, userWhoReacted, count(reaction) AS reactionCount ORDER BY reactionCount DESC LIMIT {1} "
      + "MATCH (user)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(reaction:REACTION)-[:CREATED_BY]->(userWhoReacted) "
      + "WITH user,userWhoReacted, reaction AS reaction "
      + "MATCH (user)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(reaction)-[:HAS]->(reactionType:REACTION_TYPE) "
      + "WITH userWhoReacted, reactionType.name AS reactionType, count(reaction) AS reactionsCount "
      + "RETURN userWhoReacted, reactionType, reactionsCount")
  List<ReactionsPerUserPerTypeCount> getMyBiggestFans(String userFacebookId, Long limit);

  @Query("MATCH (user:USER)<-[:CREATED_BY]-(reaction:REACTION)<-[:HAS]-(:REACTIONS)<-[:HAS]-(item)"
         + " WHERE reaction.reactionType={1} AND user.facebookId={0} "
         + "WITH DISTINCT count(user) AS reactionCount RETURN reactionCount")
  Long getReactionsCount(String userFacebookId, ReactionType reactionType);

  @Query(
      "MATCH (user:USER)-[:HAS_SR]-(sr:SOCIAL_RANK) WHERE user.facebookId={0} RETURN user.name AS name, user.facebookId AS facebookId, sr.socialRank AS socialRank, "
      + "sr.normalizedTaggedRatio AS normalizedTaggedRatio, sr.normalizedReceivedPerGivenReactions AS normalizedReceivedPerGivenReactions, "
      + "sr.normalizedAveragePostPower AS normalizedAveragePostPower, sr.normalizedAveragePostImpactTime AS normalizedAveragePostImpactTime, "
      + "sr.normaliedReceivedPerGivenCommentRatio AS normaliedReceivedPerGivenCommentRatio;")
  UserSocialRank getUserSocialRank(String userFacebookId);

  @Query("MATCH (user:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(friend:USER) WHERE user.facebookId = {0} RETURN friend;")
  List<User> getUserFriends(String facebookId);

  @Query("MATCH (user:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType='APPLICATION_USER' AND user.facebookId = {0} "
      + "WITH user "
      + "MATCH (user:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(friend:USER) WHERE friend.facebookId={1} "
      + "WITH user, friend "
      + "MATCH (user)-[rs:RS]->(friend) WHERE rs.relationshipStrength>0 "
      + "RETURN  user, friend, rs.relationshipStrength AS relationshipStrength ORDER BY relationshipStrength DESC;")
  RelationshipStrength getRelationshipStrength(String facebookId, String friendFacebookId);
}
