package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountHourly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.TagPower;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
public interface TagOperationsRepository extends GraphRepository<Tag> {


  @Query("MATCH (p:POST)-[]->(:REACTIONS)-[]->(r:REACTION) "
         + "WITH p, count(r) AS reactions "
         + "MATCH (p)-[]->(:COMMENTS)-[]->(c:COMMENT) "
         + "WITH  p, reactions, count(c) AS comments  "
         + "MATCH (p)-[]->(:COMMENTS)-[]->(c:COMMENT)-[]->(:REACTIONS)-[]->(r:REACTION) "
         + "WITH p, reactions, comments, count(r) AS commentReactions "
         + "OPTIONAL MATCH (p)-[]->(:COMMENTS)-[]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT) "
         + "WITH p, reactions, comments, commentReactions, count(ic) AS replies "
         + "OPTIONAL MATCH (p)-[]->(:COMMENTS)-[]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT)-[]->(:REACTIONS)-[]->(r:REACTION) "
         + "WITH p, reactions, comments, commentReactions, replies, count(r) AS replyReactions "
         + "WITH p, reactions, comments, commentReactions, replies, replyReactions "
         + "WITH p, reactions, comments + replies AS totalComments, commentReactions + replyReactions AS totalCommentReactions "
         + "WITH p, 5 * totalComments + 3 * reactions + 1 * totalCommentReactions AS power "
         + "WITH p, power ORDER BY power DESC "
         + "MATCH (p)-[]->(t:TAG) WITH t, sum(power) AS totalPower "
         + "RETURN t.name AS tag, totalPower AS power ORDER BY power DESC LIMIT {0};")
  List<TagPower> getTopTags(Integer limit);
}
