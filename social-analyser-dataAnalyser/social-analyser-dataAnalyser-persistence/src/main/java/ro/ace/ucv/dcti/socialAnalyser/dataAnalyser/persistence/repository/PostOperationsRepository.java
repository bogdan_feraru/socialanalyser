package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.PostPower;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
public interface PostOperationsRepository extends GraphRepository<Post> {

  @Query(
      "MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(p:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) WHERE user.facebookId={0} "
      + "WITH p, count(r) AS reactions "
      + "MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT) "
      + "WITH  p, reactions, count(c) AS comments  "
      + "MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) "
      + "WITH p, reactions, comments, count(r) AS commentReactions "
      + "OPTIONAL MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT) "
      + "WITH p, reactions, comments, commentReactions, count(ic) AS replies \n"
      + "OPTIONAL MATCH (p)-[]->(:COMMENTS)-[]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) "
      + "WITH p, reactions, comments, commentReactions, replies, count(r) AS replyReactions "
      + "WITH p, reactions, comments, commentReactions, replies, replyReactions "
      + "WITH p, reactions, comments + replies AS totalComments, commentReactions + replyReactions AS totalCommentReactions "
      + "WITH p, totalComments, reactions, totalCommentReactions, 5 * totalComments + 3 * reactions + 1 * totalCommentReactions AS power "
      + "RETURN p.link as link, power, 5 * totalComments AS totalComments, 3 * reactions + 1 * totalCommentReactions  AS totalReactions "
      + "ORDER BY power DESC LIMIT {1}")
  List<PostPower> getTopPost(String facebookUserId, Integer count);

  @Query(
      "MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(p:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) "
      + "WITH p, count(r) AS reactions "
      + "MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT) "
      + "WITH  p, reactions, count(c) AS comments  "
      + "MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) "
      + "WITH p, reactions, comments, count(r) AS commentReactions "
      + "OPTIONAL MATCH (p)-[:HAS]->(:COMMENTS)-[:HAS]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT) "
      + "WITH p, reactions, comments, commentReactions, count(ic) AS replies \n"
      + "OPTIONAL MATCH (p)-[]->(:COMMENTS)-[]->(c)-[:HAS]->(:COMMENTS)-[:HAS]->(ic:COMMENT)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION) "
      + "WITH p, reactions, comments, commentReactions, replies, count(r) AS replyReactions "
      + "WITH p, reactions, comments, commentReactions, replies, replyReactions "
      + "WITH p, reactions, comments + replies AS totalComments, commentReactions + replyReactions AS totalCommentReactions "
      + "WITH p, totalComments, reactions, totalCommentReactions, 5 * totalComments + 3 * reactions + 1 * totalCommentReactions AS power "
      + "RETURN p.link as link, power, 5 * totalComments AS totalComments, 3 * reactions + 1 * totalCommentReactions  AS totalReactions "
      + "ORDER BY power DESC LIMIT {0}")
  List<PostPower> getTopPosts(Integer noOfPosts);
}
