package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
@QueryResult
public class TagPower {

  private String tag;
  private Integer power;

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }

  public Integer getPower() {
    return power;
  }

  public void setPower(Integer power) {
    this.power = power;
  }
}
