package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
@QueryResult
public class UserReactionsCount {

  private String reactionType;

  private Long reactionCount;

  public String getReactionType() {
    return reactionType;
  }

  public void setReactionType(String reactionType) {
    this.reactionType = reactionType;
  }

  public Long getReactionCount() {
    return reactionCount;
  }

  public void setReactionCount(Long reactionCount) {
    this.reactionCount = reactionCount;
  }
}
