package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@QueryResult
public class UserSocialRank {

  private String facebookId;
  private String name;
  private Double socialRank;
  private Double normalizedTaggedRatio;
  private Double normalizedReceivedPerGivenReactions;
  private Double normalizedAveragePostPower;
  private Double normalizedAveragePostImpactTime;
  private Double normaliedReceivedPerGivenCommentRatio;

  public String getFacebookId() {
    return facebookId;
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getSocialRank() {
    return socialRank;
  }

  public void setSocialRank(Double socialRank) {
    this.socialRank = socialRank;
  }

  public Double getNormalizedTaggedRatio() {
    return normalizedTaggedRatio;
  }

  public void setNormalizedTaggedRatio(Double normalizedTaggedRatio) {
    this.normalizedTaggedRatio = normalizedTaggedRatio;
  }

  public Double getNormalizedReceivedPerGivenReactions() {
    return normalizedReceivedPerGivenReactions;
  }

  public void setNormalizedReceivedPerGivenReactions(Double normalizedReceivedPerGivenReactions) {
    this.normalizedReceivedPerGivenReactions = normalizedReceivedPerGivenReactions;
  }

  public Double getNormalizedAveragePostPower() {
    return normalizedAveragePostPower;
  }

  public void setNormalizedAveragePostPower(Double normalizedAveragePostPower) {
    this.normalizedAveragePostPower = normalizedAveragePostPower;
  }

  public Double getNormalizedAveragePostImpactTime() {
    return normalizedAveragePostImpactTime;
  }

  public void setNormalizedAveragePostImpactTime(Double normalizedAveragePostImpactTime) {
    this.normalizedAveragePostImpactTime = normalizedAveragePostImpactTime;
  }

  public Double getNormaliedReceivedPerGivenCommentRatio() {
    return normaliedReceivedPerGivenCommentRatio;
  }

  public void setNormaliedReceivedPerGivenCommentRatio(Double normaliedReceivedPerGivenCommentRatio) {
    this.normaliedReceivedPerGivenCommentRatio = normaliedReceivedPerGivenCommentRatio;
  }
}
