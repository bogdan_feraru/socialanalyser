package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.List;
import java.util.Map;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
@QueryResult
public class ReactionsPerUserPerTypeCount {

  private User userWhoReacted;
  private String reactionType;
  private Long reactionsCount;

  public User getUserWhoReacted() {
    return userWhoReacted;
  }

  public void setUserWhoReacted(User userWhoReacted) {
    this.userWhoReacted = userWhoReacted;
  }

  public String getReactionType() {
    return reactionType;
  }

  public void setReactionType(String reactionType) {
    this.reactionType = reactionType;
  }

  public Long getReactionsCount() {
    return reactionsCount;
  }

  public void setReactionsCount(Long reactionsCount) {
    this.reactionsCount = reactionsCount;
  }
}
