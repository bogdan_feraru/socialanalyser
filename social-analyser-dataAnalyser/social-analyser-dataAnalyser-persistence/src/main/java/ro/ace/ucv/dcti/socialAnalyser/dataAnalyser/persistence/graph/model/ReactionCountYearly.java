package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
@QueryResult
public class ReactionCountYearly {

  private String year;
  private Integer reactions;

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }
}
