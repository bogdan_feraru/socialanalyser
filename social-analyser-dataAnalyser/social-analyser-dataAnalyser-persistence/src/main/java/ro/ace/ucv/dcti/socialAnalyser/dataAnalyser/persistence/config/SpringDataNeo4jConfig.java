package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories(basePackages = "ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository")
@ComponentScan(basePackages = "ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.config")
@EnableTransactionManagement
public class SpringDataNeo4jConfig {

  @Bean
  public SessionFactory sessionFactory() {
    return new SessionFactory("ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain",
                              "ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model");
  }

  @Bean
  public Neo4jTransactionManager transactionManager() {
    return new Neo4jTransactionManager(sessionFactory());
  }


}
