package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@QueryResult
public class PostPower {

  String link;
  Integer power;
  Integer totalComments;
  Integer reactions;
  Integer totalCommentReactions;
  Integer totalReactions;

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public Integer getPower() {
    return power;
  }

  public void setPower(Integer power) {
    this.power = power;
  }

  public Integer getTotalComments() {
    return totalComments;
  }

  public void setTotalComments(Integer totalComments) {
    this.totalComments = totalComments;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }

  public Integer getTotalCommentReactions() {
    return totalCommentReactions;
  }

  public void setTotalCommentReactions(Integer totalCommentReactions) {
    this.totalCommentReactions = totalCommentReactions;
  }

  public Integer getTotalReactions() {
    return totalReactions;
  }

  public void setTotalReactions(Integer totalReactions) {
    this.totalReactions = totalReactions;
  }
}
