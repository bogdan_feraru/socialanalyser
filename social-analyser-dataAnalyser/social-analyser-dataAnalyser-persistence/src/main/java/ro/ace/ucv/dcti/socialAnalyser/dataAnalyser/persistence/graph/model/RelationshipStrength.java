package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model;

import org.springframework.data.neo4j.annotation.QueryResult;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 7/5/2017.
 */
@QueryResult
public class RelationshipStrength {

  private User user;
  private User friend;
  private Double relationshipStrength;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getFriend() {
    return friend;
  }

  public void setFriend(User friend) {
    this.friend = friend;
  }

  public Double getRelationshipStrength() {
    return relationshipStrength;
  }

  public void setRelationshipStrength(Double relationshipStrength) {
    this.relationshipStrength = relationshipStrength;
  }
}
