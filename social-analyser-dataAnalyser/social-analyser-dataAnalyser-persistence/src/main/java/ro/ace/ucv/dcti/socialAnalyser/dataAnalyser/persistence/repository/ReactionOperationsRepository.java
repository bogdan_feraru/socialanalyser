package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountHourly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountMonthly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountYearly;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reaction;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;

/**
 * Created by bogdan.feraru on 6/28/2017.
 */
public interface ReactionOperationsRepository extends GraphRepository<Reaction> {

  @Query("MATCH (:REACTION)-[:HAS]->(reactionType:REACTION_TYPE) RETURN DISTINCT reactionType;")
  List<ReactionType> getReactionTypes();

  @Query("MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(post:POST)-[:HAS]->(:REACTIONS)-[:HAS]->(reaction:REACTION) "
         + "WITH post, reaction, user "
         + "MATCH (post)-[:CREATED_AT]->(:HOUR)<-[:HAS_HOUR]-(:DAY)<-[:HAS_DAY]-(month:MONTH)<-[:HAS_MONTH]-(year:YEAR) "
         + "WHERE year.externalId={1} AND user.facebookId={0} "
         + "RETURN toString(month.month) AS month, count(reaction) AS reactions ORDER BY month DESC;")
  List<ReactionCountMonthly> getReceivedReactionsInYearMonthly(String facebookId, String year);

  @Query("MATCH (r:REACTION)<-[:HAS]-(:REACTIONS)<-[:HAS]-(p:POST)-[:CREATED_AT]->(h:HOUR)<-[:HAS_HOUR]-(d:DAY)<-[:HAS_DAY]-(m:MONTH)<-[:HAS_MONTH]-(y:YEAR) "
         + "WHERE toInteger(y.year) < 2017 "
         + "RETURN COUNT(r) AS reactions, toString(y.year) as year "
         + "ORDER BY year DESC;")
  List<ReactionCountYearly> getReactionsGloballyYearly();

  @Query("MATCH (r:REACTION)<-[:HAS]-(:REACTIONS)<-[:HAS]-(p:POST)-[:CREATED_AT]->(h:HOUR)<-[:HAS_HOUR]-(d:DAY)<-[:HAS_DAY]-(m:MONTH)<-[:HAS_MONTH]-(y:YEAR) "
         + "WHERE toInteger(y.year) < 2017 "
         + "RETURN COUNT(r) AS reactions, toString(m.month) as month "
         + "ORDER BY month ASC;")
  List<ReactionCountMonthly> getReactionsGloballyMonthly();

  @Query("MATCH (r:REACTION)<-[:HAS]-(:REACTIONS)<-[:HAS]-(p:POST)-[:CREATED_AT]->(h:HOUR)<-[:HAS_HOUR]-(d:DAY)<-[:HAS_DAY]-(m:MONTH)<-[:HAS_MONTH]-(y:YEAR) "
         + "WHERE toInteger(y.year) < 2017 "
         + "RETURN COUNT(r) AS reactions, toString(h.hour) as hour "
         + "ORDER BY hour ASC;")
  List<ReactionCountHourly> getReactionsGloballyHourly();

}
