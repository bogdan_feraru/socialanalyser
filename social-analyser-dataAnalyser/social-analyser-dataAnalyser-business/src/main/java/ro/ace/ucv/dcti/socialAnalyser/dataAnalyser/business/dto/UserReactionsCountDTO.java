package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/28/2017.
 */
public class UserReactionsCountDTO {

  private User userWhoReacted;
  private List<CountPerReactionDTO> countPerReactionList;

  public User getUserWhoReacted() {
    return userWhoReacted;
  }

  public void setUserWhoReacted(User userWhoReacted) {
    this.userWhoReacted = userWhoReacted;
  }

  public List<CountPerReactionDTO> getCountPerReactionList() {
    return countPerReactionList;
  }

  public void setCountPerReactionList(List<CountPerReactionDTO> countPerReactionList) {
    this.countPerReactionList = countPerReactionList;
  }
}
