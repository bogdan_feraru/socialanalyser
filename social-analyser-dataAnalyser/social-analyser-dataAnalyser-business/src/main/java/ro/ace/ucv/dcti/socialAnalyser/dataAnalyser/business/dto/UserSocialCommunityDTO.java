package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bogdan.feraru on 6/28/2017.
 */

public class UserSocialCommunityDTO {

  private UserSocialRankDTO userSocialRank;

  private List<EdgeDTO> edgeList = new ArrayList<>();

  public UserSocialRankDTO getUserSocialRank() {
    return userSocialRank;
  }

  public void setUserSocialRank(UserSocialRankDTO userSocialRank) {
    this.userSocialRank = userSocialRank;
  }

  public List<EdgeDTO> getEdgeList() {
    return edgeList;
  }

  public void setEdgeList(List<EdgeDTO> edgeList) {
    this.edgeList = edgeList;
  }
}
