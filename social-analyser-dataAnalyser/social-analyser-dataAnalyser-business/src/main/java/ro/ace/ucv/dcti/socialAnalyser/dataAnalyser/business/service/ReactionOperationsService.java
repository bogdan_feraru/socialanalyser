package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountHourlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountMonthlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountYearlyDTO;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
public interface ReactionOperationsService {

  List<String> getAllReactionsTypes();

  List<ReactionCountMonthlyDTO> getReceivedReactionsInYearMonthly(String facebookId, Integer year);

  List<ReactionCountYearlyDTO> getReactionsGloballyYearly();

  List<ReactionCountHourlyDTO> getReactionsGloballyHourly();

  List<ReactionCountMonthlyDTO> getReactionsGloballyMonthly();
}
