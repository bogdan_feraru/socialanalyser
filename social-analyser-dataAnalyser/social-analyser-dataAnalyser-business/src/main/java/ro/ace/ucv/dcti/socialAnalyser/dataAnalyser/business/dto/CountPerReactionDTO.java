package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

/**
 * Created by bogdan.feraru on 6/28/2017.
 */
public class CountPerReactionDTO {

  private String reactionType;

  private Long reactionCount;

  public String getReactionType() {
    return reactionType;
  }

  public void setReactionType(String reactionType) {
    this.reactionType = reactionType;
  }

  public Long getReactionCount() {
    return reactionCount;
  }

  public void setReactionCount(Long reactionCount) {
    this.reactionCount = reactionCount;
  }
}
