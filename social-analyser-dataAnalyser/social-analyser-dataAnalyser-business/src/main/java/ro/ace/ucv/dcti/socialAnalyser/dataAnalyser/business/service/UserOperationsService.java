package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserReactionsCountDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserSocialCommunityDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserSocialRankDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserReactionsCount;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
public interface UserOperationsService {

  List<UserReactionsCount> getReceivedUserReactionCount(String userFacebookId);

  List<UserReactionsCountDTO> getMyBiggestFans(String userFacebookId, Long limit);

  List<UserReactionsCount> getGivenUserReactionCount(String userFacebookId);

  UserSocialCommunityDTO getUserSocialCommunity(String userFacebookId);
}
