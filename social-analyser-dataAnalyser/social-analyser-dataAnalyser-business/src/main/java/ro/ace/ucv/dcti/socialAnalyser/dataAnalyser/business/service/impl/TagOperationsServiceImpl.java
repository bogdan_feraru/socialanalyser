package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.TagPowerDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.TagOperationsService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.TagPower;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository.TagOperationsRepository;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@Service
public class TagOperationsServiceImpl implements TagOperationsService {

  @Autowired
  private Mapper mapper;

  @Autowired
  private TagOperationsRepository tagOperationsRepository;

  @Override
  public List<TagPowerDTO> getTopTags(Integer limit) {
    List<TagPower> topTags = tagOperationsRepository.getTopTags(limit);
    List<TagPowerDTO> tagPowerDTOList = new ArrayList<>();
    for (TagPower tagPower : topTags) {
      TagPowerDTO tagPowerDTO = mapper.map(tagPower, TagPowerDTO.class);
      tagPowerDTOList.add(tagPowerDTO);
    }

    return tagPowerDTOList;
  }
}
