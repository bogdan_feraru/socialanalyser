package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 6/28/2017.
 */
@QueryResult
public class EdgeDTO {

  private UserSocialRankDTO userSocialRank;

  private RelationshipStrength relationshipStrength;

  public UserSocialRankDTO getUserSocialRank() {
    return userSocialRank;
  }

  public void setUserSocialRank(UserSocialRankDTO userSocialRank) {
    this.userSocialRank = userSocialRank;
  }

  public RelationshipStrength getRelationshipStrength() {
    return relationshipStrength;
  }

  public void setRelationshipStrength(
      RelationshipStrength relationshipStrength) {
    this.relationshipStrength = relationshipStrength;
  }
}
