package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
public class ReactionCountHourlyDTO {

  private String hour;
  private Integer reactions;

  public String getHour() {
    return hour;
  }

  public void setHour(String hour) {
    this.hour = hour;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }
}
