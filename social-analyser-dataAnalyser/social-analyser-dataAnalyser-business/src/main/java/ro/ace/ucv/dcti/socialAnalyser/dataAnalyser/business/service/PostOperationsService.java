package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.PostPowerDTO;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
public interface PostOperationsService {

  List<PostPowerDTO> getTopPost(String facebookUserId, Integer count);

  List<PostPowerDTO> getTopPosts(Integer noOfPosts);
}
