package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountHourlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountMonthlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountYearlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.ReactionOperationsService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountHourly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountMonthly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionCountYearly;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository.ReactionOperationsRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;

/**
 * Created by bogdan.feraru on 6/11/2017.
 */
@Service
public class ReactionOperationsServiceImpl implements ReactionOperationsService {

  @Autowired
  private ReactionOperationsRepository reactionRepository;
  @Autowired
  private Mapper mapper;

  @Override
  public List<String> getAllReactionsTypes() {
    return reactionRepository.getReactionTypes().stream().map(ReactionType::getName).collect(Collectors.toList());
  }

  @Override
  public List<ReactionCountMonthlyDTO> getReceivedReactionsInYearMonthly(String facebookId, Integer year) {
    List<ReactionCountMonthlyDTO> reactionCountMonthlyList = new ArrayList<>();
    List<ReactionCountMonthly> reactionsInYearMonthly =
        reactionRepository.getReceivedReactionsInYearMonthly(facebookId, year + "");
    for (ReactionCountMonthly reactionCountMonthly : reactionsInYearMonthly) {
      reactionCountMonthlyList.add(mapper.map(reactionCountMonthly, ReactionCountMonthlyDTO.class));
    }

    String[] months = Arrays.copyOf(new DateFormatSymbols().getMonths(), 12);
    List<ReactionCountMonthlyDTO> reactionCountMonthlyFinalList = new ArrayList<>();
    for (int i = 0; i < 12; i++) {
      final int count = i;
      Optional<ReactionCountMonthlyDTO> reactionCountMonthly =
          reactionCountMonthlyList.stream().filter(item -> item.getMonth().equals(count + "")).findFirst();
      ReactionCountMonthlyDTO reactionCountMonthlyDTO;
      if (!reactionCountMonthly.isPresent()) {
        reactionCountMonthlyDTO = new ReactionCountMonthlyDTO();
        reactionCountMonthlyDTO.setMonth(months[i]);
        reactionCountMonthlyDTO.setReactions(0);
      } else {
        reactionCountMonthlyDTO = reactionCountMonthly.get();
        reactionCountMonthlyDTO.setMonth(months[Integer.valueOf(reactionCountMonthlyDTO.getMonth())]);
      }
      reactionCountMonthlyFinalList.add(reactionCountMonthlyDTO);
    }

    return reactionCountMonthlyFinalList;
  }

  @Override
  public List<ReactionCountYearlyDTO> getReactionsGloballyYearly() {
    List<ReactionCountYearly> reactionsGloballyYearly = reactionRepository.getReactionsGloballyYearly();
    List<ReactionCountYearlyDTO> reactionCountYearlyDTOList = new ArrayList<>();

    for (ReactionCountYearly reactionCountYearly : reactionsGloballyYearly) {
      ReactionCountYearlyDTO reactionCountYearlyDTO = mapper.map(reactionCountYearly, ReactionCountYearlyDTO.class);
      reactionCountYearlyDTOList.add(reactionCountYearlyDTO);
    }

    reactionCountYearlyDTOList.sort(Comparator.comparing(ReactionCountYearlyDTO::getYear));

    return reactionCountYearlyDTOList;
  }

  @Override
  public List<ReactionCountHourlyDTO> getReactionsGloballyHourly() {
    List<ReactionCountHourly> reactionsGloballyHourly = reactionRepository.getReactionsGloballyHourly();
    List<ReactionCountHourlyDTO> reactionCountHourlyDTOList = new ArrayList<>();

    for (ReactionCountHourly reactionCountHourly : reactionsGloballyHourly) {
      ReactionCountHourlyDTO reactionCountYearlyDTO = mapper.map(reactionCountHourly, ReactionCountHourlyDTO.class);
      reactionCountHourlyDTOList.add(reactionCountYearlyDTO);
    }

    return reactionCountHourlyDTOList;
  }

  @Override
  public List<ReactionCountMonthlyDTO> getReactionsGloballyMonthly() {
    List<ReactionCountMonthlyDTO> reactionCountMonthlyList = new ArrayList<>();
    List<ReactionCountMonthly> reactionsInYearMonthly = reactionRepository.getReactionsGloballyMonthly();
    for (ReactionCountMonthly reactionCountMonthly : reactionsInYearMonthly) {
      reactionCountMonthlyList.add(mapper.map(reactionCountMonthly, ReactionCountMonthlyDTO.class));
    }

    String[] months = Arrays.copyOf(new DateFormatSymbols().getMonths(), 12);
    List<ReactionCountMonthlyDTO> reactionCountMonthlyFinalList = new ArrayList<>();

    for (int i = 0; i < 12; i++) {
      final int count = i + 1;
      Optional<ReactionCountMonthlyDTO> reactionCountMonthly =
          reactionCountMonthlyList.stream().filter(item -> item.getMonth().equals(count + "")).findFirst();
      ReactionCountMonthlyDTO reactionCountMonthlyDTO;
      if (!reactionCountMonthly.isPresent()) {
        reactionCountMonthlyDTO = new ReactionCountMonthlyDTO();
        reactionCountMonthlyDTO.setMonth(months[i]);
        reactionCountMonthlyDTO.setReactions(0);
      } else {
        reactionCountMonthlyDTO = reactionCountMonthly.get();
        reactionCountMonthlyDTO.setMonth(months[Integer.valueOf(reactionCountMonthlyDTO.getMonth()) - 1]);
      }
      reactionCountMonthlyFinalList.add(reactionCountMonthlyDTO);
    }

    return reactionCountMonthlyFinalList;
  }
}
