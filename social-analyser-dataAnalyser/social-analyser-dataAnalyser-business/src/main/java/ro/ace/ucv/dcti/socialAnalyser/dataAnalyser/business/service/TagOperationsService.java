package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.TagPowerDTO;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
public interface TagOperationsService {

  List<TagPowerDTO> getTopTags(Integer limit);
}
