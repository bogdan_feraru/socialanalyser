package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service;


import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

public interface UserService {

  List<User> getAllApplicationUsers();
}
