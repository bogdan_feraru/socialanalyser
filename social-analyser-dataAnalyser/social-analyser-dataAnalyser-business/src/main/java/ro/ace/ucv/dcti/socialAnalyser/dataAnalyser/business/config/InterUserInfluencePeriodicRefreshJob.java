package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.config;

import org.neo4j.ogm.session.Session;

import java.util.HashMap;

/**
 * Created by bogdan.feraru on 7/5/2017.
 */
public class InterUserInfluencePeriodicRefreshJob implements Runnable {

  private final Session session;

  private static final String
      CREATE_RS =
      "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType='APPLICATION_USER' "
      + "WITH n\n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, collect(o) AS friends\n"
      + "FOREACH (user IN friends |\n"
      + "         MERGE (n)-[r:RS]->(user)\n"
      + "         ON CREATE SET r.mutualTagsPercentage = 0, r.mutualCommentsPercentage = 0, r.mutualReactionsPercentage = 0\n"
      + ")";
  private static final String
      COMPUTE_RS_STEP_1 =
      "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType=\"APPLICATION_USER\" \n"
      + "WITH n \n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, o\n"
      + "MATCH (n)-[]->(:USER_FEED)-[]->(:POST)-[]->(:REACTIONS)-[]->(r:REACTION)-[:CREATED_BY]->(o)  \n"
      + "WITH n, o, count(r) AS receivedPostReactions\n"
      + "OPTIONAL MATCH (o)<-[:CREATED_BY]-(r:REACTION)<-[:HAS]-(:REACTIONS)<-[:HAS]-(:COMMENT)-[:CREATED_BY]->(n)\n"
      + "WITH n, o, receivedPostReactions, count(r) AS receivedCommentReactions\n"
      + "WITH n, o, receivedPostReactions + receivedCommentReactions AS receivedReactions\n"
      + "MATCH (o)-[]->(:USER_FEED)-[]->(:POST)-[]->(:REACTIONS)-[]->(r:REACTION)-[]->(n) \n"
      + "WITH n, o, receivedReactions, count(r) AS givenPostReactions\n"
      + "OPTIONAL MATCH (o)<-[:CREATED_BY]-(:COMMENT)-[:HAS]->(:REACTIONS)-[:HAS]->(r:REACTION)-[:CREATED_BY]->(n)\n"
      + "WITH n, o, receivedReactions, givenPostReactions, count(r) AS givenCommentReactions\n"
      + "WITH n, o, receivedReactions, givenPostReactions + givenCommentReactions AS givenReactions\n"
      + "WITH n, o, receivedReactions, givenReactions, \n"
      + "CASE WHEN receivedReactions < givenReactions THEN toFloat(receivedReactions)/givenReactions ELSE toFloat(givenReactions)/receivedReactions END AS mutualReactionsPercentage \n"
      + "ORDER BY mutualReactionsPercentage DESC\n"
      + "MATCH (n)-[rs:RS]->(o)\n"
      + "SET rs.mutualReactionsPercentage = mutualReactionsPercentage\n"
      + "RETURN n, o, rs.mutualReactionsPercentage\n";
  private static final String COMPUTE_RS_STEP_2 =
      "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType=\"APPLICATION_USER\"  \n"
      + "WITH n \n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, o\n"
      + "MATCH (o)<-[:CREATED_BY]-(p:POST)-[:TAGGED]->(n:USER)\n"
      + "WITH n, o, count(p) AS taggedBy\n"
      + "MATCH (n)<-[:CREATED_BY]-(p:POST)-[:TAGGED]->(o:USER)\n"
      + "WITH n, o, taggedBy, count(p) AS tagged\n"
      + "WITH n, o, taggedBy, tagged\n"
      + "WITH n, o, CASE WHEN taggedBy < tagged THEN toFloat(taggedBy)/tagged ELSE toFloat(tagged)/taggedBy END AS mutualTagsPercentage\n"
      + "ORDER BY mutualTagsPercentage DESC\n"
      + "MATCH (n)-[rs:RS]->(o)\n"
      + "SET rs.mutualTagsPercentage = mutualTagsPercentage\n" +
      "RETURN n, o, rs.mutualTagsPercentage\n";

  private static final String COMPUTE_RS_STEP_3 =
      "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType=\"APPLICATION_USER\" \n"
      + "WITH n \n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, o\n"
      + "MATCH (n)-[]->(:USER_FEED)-[]->(p:POST)-[:HAS]-(:COMMENTS)-[:HAS]->(c:COMMENT)-[:CREATED_BY]->(o:USER), (p:POST)-[:CREATED_BY]->(o2:USER) WHERE ID(o) <> ID(o2)\n"
      + "WITH n, o, count(c) AS receivedPostComments\n"
      + "OPTIONAL MATCH (n)-[:HAS]->(:USER_FEED)-[]->(p:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(o:USER), \n"
      + "(p:POST)-[:CREATED_BY]->(o2:USER)\n"
      + "WHERE ID(o) <> ID(o2)\n"
      + "WITH n, o, receivedPostComments, count(r) AS receivedPostReplies\n"
      + "OPTIONAL MATCH (n)<-[:CREATED_BY]-(c:COMMENT)<-[:HAS]-(:COMMENTS)<-[:HAS]-(p:POST),\n"
      + "(c)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(o:USER) \n"
      + "WHERE NOT (n)-[]->(:USER_FEED)-[]->(p:POST)\n"
      + "WITH n, o, receivedPostComments, receivedPostReplies, count(r) as receivedRepliesOnOtherPosts\n"
      + "WITH n, o, receivedPostComments + receivedPostReplies + receivedRepliesOnOtherPosts AS receivedComments\n"
      + "\n"
      + "\n"
      + "\n"
      + "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType=\"APPLICATION_USER\" \n"
      + "WITH n \n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, o\n"
      + "MATCH (n)-[]->(:USER_FEED)-[]->(p:POST)-[:HAS]-(:COMMENTS)-[:HAS]->(c:COMMENT)-[:CREATED_BY]->(o:USER), (p:POST)-[:CREATED_BY]->(o2:USER) WHERE ID(o) <> ID(o2)\n"
      + "WITH n, o, count(c) AS receivedPostComments\n"
      + "OPTIONAL MATCH (n)-[:HAS]->(:USER_FEED)-[]->(p:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(o:USER), \n"
      + "(p:POST)-[:CREATED_BY]->(o2:USER)\n"
      + "WHERE ID(o) <> ID(o2)\n"
      + "WITH n, o, receivedPostComments, count(r) AS receivedPostReplies\n"
      + "OPTIONAL MATCH (n)<-[:CREATED_BY]-(c:COMMENT)<-[:HAS]-(:COMMENTS)<-[:HAS]-(p:POST),\n"
      + "(c)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(o:USER) \n"
      + "WHERE NOT (n)-[]->(:USER_FEED)-[]->(p:POST)\n"
      + "WITH n, o, receivedPostComments, receivedPostReplies, count(r) as receivedRepliesOnOtherPosts\n"
      + "WITH n, o, receivedPostComments + receivedPostReplies + receivedRepliesOnOtherPosts AS receivedComments\n"
      + "MATCH (o)-[]->(:USER_FEED)-[]->(p:POST)-[:HAS]-(:COMMENTS)-[:HAS]->(c:COMMENT)-[:CREATED_BY]->(n:USER), (p:POST)-[:CREATED_BY]->(o2:USER) WHERE ID(n) <> ID(o2)\n"
      + "WITH n, o, receivedComments, count(c) AS givenPostComments\n"
      + "OPTIONAL MATCH (o)-[:HAS]->(:USER_FEED)-[]->(p:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(c:COMMENT)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(n:USER), \n"
      + "(p:POST)-[:CREATED_BY]->(o2:USER)\n"
      + "WHERE ID(n) <> ID(o2)\n"
      + "WITH n, o , receivedComments, givenPostComments, count(r) AS givenPostReplies\n"
      + "OPTIONAL MATCH (o)<-[:CREATED_BY]-(c:COMMENT)<-[:HAS]-(:COMMENTS)<-[:HAS]-(p:POST),\n"
      + "(c)-[:HAS]->(:COMMENTS)-[:HAS]->(r:COMMENT)-[:CREATED_BY]->(n:USER) \n"
      + "WHERE NOT (o)-[]->(:USER_FEED)-[]->(p:POST)\n"
      + "WITH n, o, receivedComments, givenPostComments, givenPostReplies, count(r) as givenRepliesOnOtherPosts\n"
      + "WITH n, o, receivedComments, givenPostComments + givenPostReplies + givenRepliesOnOtherPosts AS givenComments\n"
      + "WITH n, o, receivedComments, givenComments\n"
      + "WITH n, o, CASE WHEN receivedComments < givenComments THEN toFloat(receivedComments)/givenComments ELSE toFloat(givenComments)/receivedComments END AS mutualCommentsPercentage\n"
      + "ORDER BY mutualCommentsPercentage DESC\n"
      + "MATCH (n)-[rs:RS]->(o)\n"
      + "SET rs.mutualCommentsPercentage = mutualCommentsPercentage\n"
      + "RETURN n, o, rs.mutualCommentsPercentage\n";
  private static final String COMPUTE_RS_STEP_4 =
      "MATCH (n:USER)-[:HAS]->(us:USER_STATUS) WHERE us.userStatusType=\"APPLICATION_USER\" \n"
      + "WITH n \n"
      + "MATCH (n:USER)-[:HAS]->(:FRIEND_LIST)-[:HAS]->(o:USER)\n"
      + "WITH n, o\n"
      + "MATCH (n)-[rs:RS]->(o)\n"
      + "WITH n, rs, o\n"
      + "SET rs.relationshipStrength = 0.45 * rs.mutualTagsPercentage + 0.35 * rs.mutualCommentsPercentage + 0.20 * rs.mutualReactionsPercentage\n"
      + "RETURN n, rs.relationshipStrength, o ORDER BY rs.relationshipStrength DESC";

  public InterUserInfluencePeriodicRefreshJob(Session session) {
    this.session = session;
  }

  @Override
  public void run() {
    session.query(CREATE_RS, new HashMap<>());
    session.query(COMPUTE_RS_STEP_1, new HashMap<>());
    session.query(COMPUTE_RS_STEP_2, new HashMap<>());
    session.query(COMPUTE_RS_STEP_3, new HashMap<>());
    session.query(COMPUTE_RS_STEP_4, new HashMap<>());
  }
}
