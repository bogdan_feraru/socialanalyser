package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.PostPowerDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.PostOperationsService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.PostPower;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository.PostOperationsRepository;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@Service
public class PostOperationsServiceImpl implements PostOperationsService {

  @Autowired
  private Mapper mapper;
  @Autowired
  private PostOperationsRepository postOperationsRepository;

  @Override
  public List<PostPowerDTO> getTopPost(String facebookUserId, Integer count) {
    List<PostPower> topPosts = postOperationsRepository.getTopPost(facebookUserId, count);
    List<PostPowerDTO> postPowerDTOList = new ArrayList<>();
    for (PostPower postPower : topPosts) {
      postPowerDTOList.add(mapper.map(postPower, PostPowerDTO.class));
    }

    return postPowerDTOList;
  }

  @Override
  public List<PostPowerDTO> getTopPosts(Integer noOfPosts) {
    List<PostPower> topPosts = postOperationsRepository.getTopPosts(noOfPosts);
    List<PostPowerDTO> postPowerDTOList = new ArrayList<>();
    for (PostPower postPower : topPosts) {
      postPowerDTOList.add(mapper.map(postPower, PostPowerDTO.class));
    }

    return postPowerDTOList;
  }
}
