package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dozer.converter;

import org.dozer.DozerConverter;

import java.text.DateFormatSymbols;
import java.util.Date;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
public class MonthYearToMonthConverter extends DozerConverter<String, String> {

  public MonthYearToMonthConverter() {
    super(String.class, String.class);
  }

  @Override
  public String convertTo(String source, String destination) {
    if (source.length() != 6) {
      return "";
    }
    Integer month = Integer.parseInt(source.substring(4));
    return new DateFormatSymbols().getMonths()[month - 1];
  }

  @Override
  public String convertFrom(String source, String destination) {
    if (source.length() != 6) {
      return "";
    }
    Integer month = Integer.parseInt(source.substring(4));
    return new DateFormatSymbols().getMonths()[month - 1];
  }
}