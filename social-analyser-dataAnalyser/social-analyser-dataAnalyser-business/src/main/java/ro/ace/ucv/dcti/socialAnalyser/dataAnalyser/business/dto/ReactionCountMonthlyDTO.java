package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
public class ReactionCountMonthlyDTO {

  private String month;
  private Integer reactions;

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }
}
