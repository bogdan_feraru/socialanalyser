package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.CountPerReactionDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.EdgeDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.RelationshipStrength;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserReactionsCountDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserSocialCommunityDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserSocialRankDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.UserOperationsService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.ReactionsPerUserPerTypeCount;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserReactionsCount;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserSocialRank;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository.UserOperationsRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
@Service
public class UserOperationsServiceImpl implements UserOperationsService {

  @Autowired
  private UserOperationsRepository userOperationsRepository;

  @Autowired
  private Mapper mapper;

  @Override
  @Transactional(readOnly = true)
  public List<UserReactionsCount> getReceivedUserReactionCount(String userFacebookId) {
    return userOperationsRepository.getReceivedUserReactionCount(userFacebookId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<UserReactionsCountDTO> getMyBiggestFans(String userFacebookId, Long limit) {
    List<ReactionsPerUserPerTypeCount> myBiggestFans = userOperationsRepository.getMyBiggestFans(userFacebookId, limit);
    Set<User> users = myBiggestFans.stream().map(item -> item.getUserWhoReacted()).collect(Collectors.toSet());
    List<UserReactionsCountDTO> userReactionsCounts = new ArrayList<>();

    for (User user : users) {
      UserReactionsCountDTO userReactionsCount = new UserReactionsCountDTO();
      userReactionsCount.setUserWhoReacted(user);
      List<CountPerReactionDTO> countPerReactionDTOS = new ArrayList<>();
      for (ReactionsPerUserPerTypeCount userReactionCount : myBiggestFans) {
        if (user.equals(userReactionCount.getUserWhoReacted())) {
          CountPerReactionDTO countPerReactionDTO = mapper.map(userReactionCount, CountPerReactionDTO.class);
          countPerReactionDTOS.add(countPerReactionDTO);
        }
      }
      userReactionsCount.setCountPerReactionList(countPerReactionDTOS);
      userReactionsCounts.add(userReactionsCount);
    }

    return userReactionsCounts;
  }

  @Override
  @Transactional(readOnly = true)
  public List<UserReactionsCount> getGivenUserReactionCount(String userFacebookId) {
    return userOperationsRepository.getGivenUserReactionCount(userFacebookId);
  }

  @Override
  @Transactional(readOnly = true)
  public UserSocialCommunityDTO getUserSocialCommunity(String userFacebookId) {
    UserSocialCommunityDTO userSocialCommunity = new UserSocialCommunityDTO();

    UserSocialRank userSocialRank = userOperationsRepository.getUserSocialRank(userFacebookId);
    userSocialCommunity.setUserSocialRank(mapper.map(userSocialRank, UserSocialRankDTO.class));

    List<User> friends = userOperationsRepository.getUserFriends(userFacebookId);
    List<EdgeDTO> edges = new ArrayList<>();
    for (User user : friends) {
      UserSocialRank friendSocialRank = userOperationsRepository.getUserSocialRank(user.getFacebookId());
      ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.RelationshipStrength relationshipStrength1 =
          userOperationsRepository.getRelationshipStrength(userFacebookId, user.getFacebookId());
      if (friendSocialRank != null && friendSocialRank.getSocialRank() != null
          && friendSocialRank.getSocialRank() != 0.0
          && relationshipStrength1 != null && relationshipStrength1.getRelationshipStrength() > 0.0001) {
        EdgeDTO edgeDTO = new EdgeDTO();
        edgeDTO.setUserSocialRank(mapper.map(friendSocialRank, UserSocialRankDTO.class));

        RelationshipStrength relationshipStrength = new RelationshipStrength();
        relationshipStrength.setStrength((int) (relationshipStrength1.getRelationshipStrength() * 1000));
        edgeDTO.setRelationshipStrength(relationshipStrength);

        edges.add(edgeDTO);
      }
    }
    userSocialCommunity.setEdgeList(edges);

    return userSocialCommunity;
  }

}
