package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

/**
 * Created by bogdan.feraru on 6/29/2017.
 */
public class ReactionCountYearlyDTO {

  private String year;
  private Integer reactions;

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }
}
