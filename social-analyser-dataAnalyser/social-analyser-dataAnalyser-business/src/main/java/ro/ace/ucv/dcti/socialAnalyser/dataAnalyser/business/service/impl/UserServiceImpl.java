package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.repository.DomainUserRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

@Service
public class UserServiceImpl implements UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

  @Autowired
  private Mapper mapper;

  @Autowired
  private DomainUserRepository domainUserRepository;


  @Override
  public List<User> getAllApplicationUsers() {
    return domainUserRepository.findAllApplicationUsers();
  }
}
