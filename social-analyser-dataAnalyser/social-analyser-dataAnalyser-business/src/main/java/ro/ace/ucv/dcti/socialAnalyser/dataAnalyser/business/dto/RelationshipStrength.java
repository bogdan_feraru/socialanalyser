package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto;

import org.springframework.data.neo4j.annotation.QueryResult;

/**
 * Created by bogdan.feraru on 7/2/2017.
 */
@QueryResult
public class RelationshipStrength {

  private Integer strength;

  public Integer getStrength() {
    return strength;
  }

  public void setStrength(Integer strength) {
    this.strength = strength;
  }
}
