package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.config;

import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.PeriodicTrigger;

import java.util.concurrent.TimeUnit;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.config.SpringDataNeo4jConfig;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
@Configuration
@ComponentScan(basePackages = {"ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service"})
@ImportResource("classpath:metadata/dozer-springConfig.xml")
@Import(SpringDataNeo4jConfig.class)
@EnableScheduling
public class BusinessConfig {


  @Configuration
  static class RegisterTaskSchedulerViaSchedulingConfigurer implements SchedulingConfigurer {


    @Autowired
    private Session session;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
      taskRegistrar.setTaskScheduler(poolScheduler());

      taskRegistrar.addTriggerTask(new InfluencePeriodicRefreshJob(session), new PeriodicTrigger(1,
                                                                                                 TimeUnit.DAYS));
      taskRegistrar.addTriggerTask(new InterUserInfluencePeriodicRefreshJob(session), new PeriodicTrigger(1,
                                                                                                          TimeUnit.DAYS));
    }

    @Bean("concurrentTaskScheduler")
    public TaskScheduler poolScheduler() {
      return new ConcurrentTaskScheduler();
    }
  }
}
