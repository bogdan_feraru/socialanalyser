"use strict";

function MostPowerfulPostsView(container, url) {
  this.container = container;
  this.mostPowerfulPostsDataLoader = new MostPowerfulPostsDataLoader(url);
}

MostPowerfulPostsView.prototype = {

  renderView: function (biggestFans) {
    Highcharts.chart(this.container, {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Most Powerfull Posts Chart'
      },
      xAxis: {
        allowDecimals: false,
        categories: biggestFans.categories
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Power per reactions and comments'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: biggestFans.series
    });
  },

  render: function () {
    var mostPowerfulPosts = this.mostPowerfulPostsDataLoader.loadData();
    this.renderView(mostPowerfulPosts);
  }
};
