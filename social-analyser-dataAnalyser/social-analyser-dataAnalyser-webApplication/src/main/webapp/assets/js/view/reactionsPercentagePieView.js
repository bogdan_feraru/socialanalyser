"use strict";

function ReactionsPercentagePieView(container, url) {
  this.container = container;
  this.reactionsPercentagePieDataLoader = new ReactionsPercentagePieDataLoader(url);
}

ReactionsPercentagePieView.prototype = {

  renderView: function (reactionCounts) {
    var chart;
    var legend;

    chart = new AmCharts.AmPieChart();
    chart.dataProvider = reactionCounts;
    chart.titleField = "reactionType";
    chart.valueField = "reactionCount";
    chart.outlineColor = "#FFFFFF";
    chart.outlineAlpha = 0.8;
    chart.outlineThickness = 2;
    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
    chart.depth3D = 10;
    chart.angle = 30;

    legend = new AmCharts.AmLegend();
    legend.align = "center";
    legend.markerType = "circle";
    legend.position = "right";
    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
    chart.addLegend(legend);
    var reactionsPercentagePieContainer = document.getElementById(this.container);
    chart.write(reactionsPercentagePieContainer);
  },

  render: function () {
    var reactionsCounts = this.reactionsPercentagePieDataLoader.loadData();
    this.renderView(reactionsCounts);
  }
};
