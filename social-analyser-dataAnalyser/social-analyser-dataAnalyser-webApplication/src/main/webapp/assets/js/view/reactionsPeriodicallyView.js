"use strict";

function ReactionsPeriodicallyView(container, url) {
  this.container = container;
  this.reactionsPeriodicallyDataLoader = new ReactionsPeriodicallyDataLoader(url);
}

ReactionsPeriodicallyView.prototype = {

  renderView: function (reactionsPeriodically) {
    var reactionsPeriodicallyView = this;
    Highcharts.chart(reactionsPeriodicallyView.container, {
      chart: {
        zoomType: 'x'
      },
      title: {
        text: 'Received Reactions Monthly'
      },
      xAxis: [{
        categories: reactionsPeriodically.categories,
        crosshair: true
      }],
      yAxis: [{ // Primary yAxis
        labels: {
          format: '{value} R',
          style: {
            color: Highcharts.getOptions().colors[1]
          }
        },
        title: {
          text: 'Reactions',
          style: {
            color: Highcharts.getOptions().colors[1]
          }
        }
      }, { // Secondary yAxis
        title: {
          text: 'Reactions difference from last month',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        labels: {
          format: '{value} R',
          style: {
            color: Highcharts.getOptions().colors[0]
          }
        },
        opposite: true
      }],
      tooltip: {
        shared: true
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        x: 120,
        verticalAlign: 'top',
        y: 100,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
        name: ' Reactions',
        type: 'column',
        yAxis: 1,
        data: reactionsPeriodically.reactionsCounts,
        tooltip: {
          valueSuffix: 'Reactions'
        }

      }, {
        name: 'Reactions difference from last month',
        type: 'spline',
        data: reactionsPeriodically.reactionsCountsDifference,
        tooltip: {
          valueSuffix: ' Reactions'
        }
      }]
    });
  },

  render: function () {
    var reactionsPeriodically = this.reactionsPeriodicallyDataLoader.loadData();
    this.renderView(reactionsPeriodically);
  }
};
