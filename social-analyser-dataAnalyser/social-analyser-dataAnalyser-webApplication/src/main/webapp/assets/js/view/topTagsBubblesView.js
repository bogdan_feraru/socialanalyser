"use strict";

function TopTagBubblesView(container, url) {
  this.container = container;
  this.topTagBubblesDataLoader = new TopTagBubblesDataLoader(url);
}

TopTagBubblesView.prototype = {

  renderView: function (topTags) {
    var data = {
      "name": "flare",
      "children": [
        {
          "name": "analytics",
          "children": [
            {
              "name": "cluster",
              "children": topTags
            }
          ]
        }
      ]
    };

    var colorIndex = 0;

    function myColor() {
      var c = [
        "#3182bd",
        "#dc3912",
        "#ff9900",
        "#d9d9d9",
        "#a1d99b",
        "#756bb1",
        "#e6550d",
        "#fd8d3c",
        "#fdae6b",
        "#fdd0a2",
        "#31a354",
        "#3366cc",
        "#6baed6",
        "#636363",
        "#990099",
        "#0099c6",
        "#dd4477",
        "#9ecae1",
        "#c6dbef",
        "#74c476",
        "#3182bd",
        "#dc3912",
        "#ff9900",
        "#d9d9d9",
        "#a1d99b",
        "#756bb1",
        "#e6550d",
        "#fd8d3c",
        "#fdae6b",
        "#fdd0a2",
        "#31a354",
        "#3366cc",
        "#6baed6",
        "#636363",
        "#990099",
        "#0099c6",
        "#dd4477",
        "#9ecae1",
        "#c6dbef",
        "#74c476"
      ];
      return c[colorIndex++];
    }

    var diameter = 850,
        format = d3.format(",d"),
        color = d3.scale.category20c();

    var bubble = d3.layout.pack()
        .sort(null)
        .size([diameter, diameter])
        .padding(1.5);

    var svg = d3.select("#" + this.container).insert("svg", ":first-child")
        .attr("width", 1050)
        .attr("height", 850)
        .attr("class", "bubble");

    //d3.json("flare.json", function(error, root) {
    var node = svg.selectAll(".node")
        .data(bubble.nodes(classes(data))
                  .filter(function (d) {
                    return !d.children;
                  }))
        .enter().append("g")
        .attr("class", "node")
        .attr("transform", function (d) {
          return "translate(" + d.x + "," + d.y + ")";
        });

    node.append("title")
        .text(function (d) {
          return d.className + ": " + format(d.value);
        });

    function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    }

    node.append("circle")
        .attr("r", function (d) {
          return d.r;
        })
        .style("fill", function (d) {
          return myColor()
        });

    node.append("text")
        .attr("dy", ".3em")
        .style("text-anchor", "middle")
        .text(function (d) {
          return d.className.substring(0, d.r / 3);
        });

    node.append("text")
        .attr("dy", ".3em")
        .attr("y", 12)
        .style("text-anchor", "middle")

        .text(function (d) {
          return d.value;
        });

    var selection = d3.selectAll('.selected');

    var x_original = undefined;
    var y_original = undefined;

    var drag = d3.behavior.drag()
        .on("drag", function (d, i) {

          if (selection[0].indexOf(this) == -1) {
            selection.classed("selected", false);
            selection = d3.select(this);
            selection.classed("selected", true);
          }

          // backup original position
          if (x_original == undefined && y_original == undefined) {
            x_original = d.x;
            y_original = d.y;
          }
          ;

          // move the circle with the cursor
          selection.attr("transform", function (d, i) {
            d.x += d3.event.dx;
            d.y += d3.event.dy;
            return "translate(" + [d.x, d.y] + ")"
          });

          // rearrange other circles
          //node.data(bubble.nodes(classes(data)));

          // reappend dragged element as last
          // so that its stays on top
          d3.select(this).attr("transform", function (d, i) {
            return "translate(" + [d3.event.x, d3.event.y] + ")"
          });
          this.parentNode.appendChild(this);
          d3.event.sourceEvent.stopPropagation();
        });

    node.call(drag);

    // Returns a flattened hierarchy containing all leaf nodes under the root.
    function classes(root) {
      var classes = [];

      function recurse(name, node) {
        if (node.children) {
          node.children.forEach(function (child) {
            recurse(node.name, child);
          });
        } else {
          classes.push({packageName: name, className: node.name, value: node.size});
        }
      }

      recurse(null, root);
      return {children: classes};
    }

    d3.select(self.frameElement).style("height", diameter + "px");
  },

  render: function () {
    var topTags = this.topTagBubblesDataLoader.loadData();
    this.renderView(topTags);
  }
};
