"use strict";

function MyCommunityView(container, url) {
  this.container = container;
  this.myCommunityDataLoader = new MyCommunityDataLoader(url);
}

MyCommunityView.prototype = {

  renderView: function (community) {
    var nodes = null;
    var edges = null;
    var network = null;

    var container = document.getElementById(this.container);
    var data = {
      nodes:community.nodes,
      edges: community.edges
    };
    var options = {
      nodes: {
        shape: 'dot',
        scaling: {
          label: {
            min: 8,
            max: 20
          }
        }
      },
//      physics:false
    };
    network = new vis.Network(container, data, options);
  },

  render: function () {
    var community = this.myCommunityDataLoader.loadData();
    this.renderView(community);
  }
};
