"use strict";

function BiggestFansView() {
  this.myBiggestFansDataLoader = new MyBiggestFansDataLoader();
}

BiggestFansView.prototype = {

  renderView: function (biggestFans) {
    Highcharts.chart('biggestFans', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Biggest fans chart'
      },
      xAxis: {
        allowDecimals: false,
        categories: biggestFans.categories
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Number of reactions per type'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: biggestFans.series
    });
  },

  render: function () {
    var reactionsCounts = this.myBiggestFansDataLoader.loadData();
    this.renderView(reactionsCounts);
  }
};
