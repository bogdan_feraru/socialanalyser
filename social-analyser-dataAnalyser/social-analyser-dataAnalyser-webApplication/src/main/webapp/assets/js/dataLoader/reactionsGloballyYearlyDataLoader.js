"use strict";

function ReactionsGloballyYearlyDataLoader(url) {
  this.url = url;
}

ReactionsGloballyYearlyDataLoader.prototype = {

  loadData: function () {
    var url = this.url;
    var reactionsCountsPeriodically;
    $.get({url: url, async: false}, function (data) {
      reactionsCountsPeriodically = data;
    });
    var categories = this.getCategories(reactionsCountsPeriodically);
    var reactionsCounts = this.getReactionsCounts(reactionsCountsPeriodically);

    var data = {
      categories: categories,
      reactionsCounts: reactionsCounts
    };
    return data;
  },
  getCategories: function (reactionsCountsPeriodically) {
    var categories = [];
    $.each(reactionsCountsPeriodically, function (index, reactionCountsPeriodically) {
      categories.push(reactionCountsPeriodically.year);
    });

    return categories;
  },
  getReactionsCounts: function (reactionsCountsPeriodically) {
    var reactionsCounts = [];
    $.each(reactionsCountsPeriodically, function (index, reactionCountsPeriodically) {
      reactionsCounts.push(reactionCountsPeriodically.reactions);
    });

    return reactionsCounts;
  }

};