"use strict";

function MyBiggestFansDataLoader() {
}

MyBiggestFansDataLoader.prototype = {

  loadData: function () {
    var url = "/user/biggestFans";
    var biggestFans;
    var preprocessedData;

    $.get({url: url, async: false}, function (data) {
      biggestFans = data;
    });
    preprocessedData = this.preprocessData(biggestFans);

    return preprocessedData;
  },
  preprocessData: function (dataToProcess) {
    var reactionTypes = this.loadReactionsTypes();
    var categories = this.getCategories(dataToProcess);
    var series = [];
    var dataLoader = this;
    $.each(reactionTypes, function (index, reactionType) {
      var serie = dataLoader.getSerie(reactionType, dataToProcess);
      series.push(serie);
    });
    var data = {
      categories: categories,
      series: series
    };
    return data;
  },
  loadReactionsTypes: function () {
    var url = "/reactions/types";
    var reactionsTypes;
    $.get({url: url, async: false}, function (data) {
      reactionsTypes = data;
    });
    return reactionsTypes;
  },
  getCategories: function (biggestFans) {
    var categories = [];
    var dataLoader=this;
    $.each(biggestFans, function (index, biggestFan) {
      categories.push(biggestFan.userWhoReacted.name);
    });

    return categories;
  },
  getSerie: function (reactionType, dataToProcess) {
    var reactionCount = 0;
    var reactionCounts = [];
    var dataLoader = this;
    $.each(dataToProcess, function (index, biggestFan) {
      reactionCount = dataLoader.getReactionCount(reactionType, biggestFan);
      reactionCounts.push(reactionCount);
    });
    var serie = {
      name: reactionType,
      data: reactionCounts
    };
    return serie;
  },
  getReactionCount: function (reactionType, biggestFan) {
    var userReactionsCounts = biggestFan.countPerReactionList;
    var reactionsCountAux = 0;
    $.each(userReactionsCounts, function (index, reactionsCount) {
      if (reactionsCount.reactionType == reactionType) {
        reactionsCountAux = reactionsCount.reactionCount;
      }
    });
    return reactionsCountAux;
  },
  getLabel: function (userSocialRank) {
    var names = [
      'Joie',
      'Jinny',
      'Jeannetta',
      'Jama',
      'Heidy',
      'Gilberte',
      'Gema',
      'Faviola',
      'Evelynn',
      'Enda',
      'Elli',
      'Ellena',
      'Divina',
      'Dagny',
      'Collene',
      'Codi',
      'Cindie',
      'Chassidy',
      'Chasidy',
      'Catrice',
      'Catherina',
      'Cassey',
      'Caroll',
      'Carlena',
      'Candra',
      'Calista',
      'Bryanna',
      'Britteny',
      'Beula',
      'Bari',
      'Audrie',
      'audria',
      'ardelia',
      'annelle',
      'angila',
      'alona',
      'allyn'
    ];

    return names[Math.floor((Math.random() * 37) + 1)];
  }
};