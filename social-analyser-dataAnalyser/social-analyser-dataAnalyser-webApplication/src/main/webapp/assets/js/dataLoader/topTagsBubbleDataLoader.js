"use strict";

function TopTagBubblesDataLoader(url) {
  this.url = url;
}

TopTagBubblesDataLoader.prototype = {

  loadData: function () {
    var url = this.url;
    var topTags;
    $.get({url: url, async: false}, function (data) {
      topTags = data;
    });

    var data = this.preprocessData(topTags);
    return data;
  },
  preprocessData: function (topTags) {
    var topTagsData = [];
    $.each(topTags, function (index, tag) {
      var tagInfo = {
        "name": tag.tag,
        "size": tag.power
      };
      topTagsData.push(tagInfo);
    });

    return topTagsData;
  }
};