"use strict";

function ReactionsPeriodicallyDataLoader(url) {
  this.url = url;
}

ReactionsPeriodicallyDataLoader.prototype = {

  loadData: function () {
    var url = this.url;
    var reactionsCountsPeriodically;
    $.get({url: url, async: false}, function (data) {
      reactionsCountsPeriodically = data;
    });
    var categories = this.getCategories(reactionsCountsPeriodically);
    var reactionsCounts = this.getReactionsCounts(reactionsCountsPeriodically);
    var reactionsCountsDifference = this.getReactionsCountsDifference(reactionsCountsPeriodically);

    var data = {
      categories: categories,
      reactionsCounts: reactionsCounts,
      reactionsCountsDifference: reactionsCountsDifference
    };
    return data;
  },
  getCategories: function (reactionsCountsPeriodically) {
    var categories = [];
    $.each(reactionsCountsPeriodically, function (index, reactionCountsPeriodically) {
      categories.push(reactionCountsPeriodically.month);
    });

    return categories;
  },
  getReactionsCounts: function (reactionsCountsPeriodically) {
    var reactionsCounts = [];
    var aux = 0;
    $.each(reactionsCountsPeriodically, function (index, reactionCountsPeriodically) {
      reactionsCounts.push(reactionCountsPeriodically.reactions + aux);
      aux += reactionCountsPeriodically.reactions;
    });

    return reactionsCounts;
  },
  getReactionsCountsDifference: function (reactionsCountsPeriodically) {
    var reactionsCountsDifference = [];
    $.each(reactionsCountsPeriodically, function (index, reactionCountsPeriodically) {
      if (index != 0) {
        var prevReactions = reactionsCountsPeriodically[index - 1].reactions;
        var reactions = reactionCountsPeriodically.reactions;
        if (prevReactions > reactions) {
          reactionsCountsDifference.push(0);
        }
        else {
          reactionsCountsDifference.push(reactions - prevReactions);

        }
      }
      else {
        reactionsCountsDifference.push(0);
      }
    });

    return reactionsCountsDifference;
  }

};