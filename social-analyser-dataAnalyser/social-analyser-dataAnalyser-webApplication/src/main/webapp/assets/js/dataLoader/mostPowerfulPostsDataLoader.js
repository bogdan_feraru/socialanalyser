"use strict";

function MostPowerfulPostsDataLoader(url) {
  this.url = url;
}

MostPowerfulPostsDataLoader.prototype = {

  loadData: function () {
    var powerfulPosts;
    var preprocessedData;

    $.get({url: this.url, async: false}, function (data) {
      powerfulPosts = data;
    });
    preprocessedData = this.preprocessData(powerfulPosts);

    return preprocessedData;
  },
  preprocessData: function (dataToProcess) {
    var categories = this.getCategories(dataToProcess);
    var series = [];
    var serie = this.getReactionsPowerSerie(dataToProcess);
    series.push(serie);
    var serie = this.getCommentsPowerSerie(dataToProcess);
    series.push(serie);

    var data = {
      categories: categories,
      series: series
    };
    return data;
  },
  getCategories: function (powerfulPosts) {
    var categories = [];
    var i = 1;
    $.each(powerfulPosts, function (index, powerfulPost) {
      var link = '<a target="_blank" href="' + powerfulPost.link + '">Post ' + i + '</a>';
      categories.push(link);
      i++;
    });

    return categories;
  },
  getReactionsPowerSerie: function (powerfulPosts) {
    var reactionsPowerSerie = [];
    var dataLoader = this;
    $.each(powerfulPosts, function (index, powerfulPost) {
      var reactionsPower = powerfulPost.totalReactions;
      reactionsPowerSerie.push(reactionsPower);
    });
    var serie = {
      name: 'Reactions Power',
      data: reactionsPowerSerie
    };
    return serie;
  },
  getCommentsPowerSerie: function (powerfulPosts) {
    var commentsPowerSerie = [];
    var dataLoader = this;
    $.each(powerfulPosts, function (index, powerfulPost) {
      var comentsPower = powerfulPost.totalComments;
      commentsPowerSerie.push(comentsPower);
    });
    var serie = {
      name: 'Comments Power',
      data: commentsPowerSerie
    };
    return serie;
  }
};