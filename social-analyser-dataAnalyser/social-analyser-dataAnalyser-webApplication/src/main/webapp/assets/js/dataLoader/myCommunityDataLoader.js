"use strict";

function MyCommunityDataLoader(url) {
  this.url = url;
}

MyCommunityDataLoader.prototype = {

  loadData: function () {
    var userCommunity;
    var preprocessedData;

    $.get({url: this.url, async: false}, function (data) {
      userCommunity = data;
    });
    preprocessedData = this.preprocessData(userCommunity);

    return preprocessedData;
  },
  preprocessData: function (dataToProcess) {
    var nodes = this.loadNodes(dataToProcess);
    var edges = this.loadEdges(dataToProcess);

    var data = {
      nodes: nodes,
      edges: edges
    };
    return data;
  },
  loadNodes: function (dataToProcess) {
    var nodes = [];
    var userSocialRank = dataToProcess.userSocialRank;
    var edgeList = dataToProcess.edgeList;

    nodes.push(this.getNode(userSocialRank));
    var dataLoader = this;
    $.each(edgeList, function (index, edge) {
      nodes.push(dataLoader.getNode(edge.userSocialRank));
    });
    return nodes;
  },
  getNode: function (userSocialRank) {
    var node;
    node = {
      id: userSocialRank.facebookId,
      shape: 'circularImage',
      image: 'http://graph.facebook.com/v2.8/' + userSocialRank.facebookId + '/picture',
      value: userSocialRank.socialRank*10,
      label: userSocialRank.name,
      title: this.getTitle(userSocialRank)
    };
    return node;
  },
  getTitle: function (userSocialRank) {
    var title = '<ul class=\"list-group\">';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-primary\">Tagged Ratio:'
        + userSocialRank.normalizedTaggedRatio.toFixed(2) + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-success\">Received Per Given Reactions:'
        + userSocialRank.normalizedReceivedPerGivenReactions.toFixed(2) + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-info\">Average Post Power:'
        + userSocialRank.normalizedAveragePostPower.toFixed(2) + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-warning\">Average Post Impact Time:'
        + (userSocialRank.normalizedAveragePostImpactTime != null
            ? userSocialRank.normalizedAveragePostImpactTime.toFixed(2) : 0.00) + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-danger\">Received Per Given Comment Ratio:'
        + (userSocialRank.normalizedReceivedPerGivenCommentRatio != null
            ? userSocialRank.normalizedReceivedPerGivenCommentRatio.toFixed(2) : 0.00) + '</span></li>';
    title += '</ul>';

    return title;
  },
  getLabel: function (userSocialRank) {
    var names = [
      'Joie',
      'Jinny',
      'Jeannetta',
      'Jama',
      'Heidy',
      'Gilberte',
      'Gema',
      'Faviola',
      'Evelynn',
      'Enda',
      'Elli',
      'Ellena',
      'Divina',
      'Dagny',
      'Collene',
      'Codi',
      'Cindie',
      'Chassidy',
      'Chasidy',
      'Catrice',
      'Catherina',
      'Cassey',
      'Caroll',
      'Carlena',
      'Candra',
      'Calista',
      'Bryanna',
      'Britteny',
      'Beula',
      'Bari',
      'Audrie',
      'audria',
      'ardelia',
      'annelle',
      'angila',
      'alona',
      'allyn'
    ];

    return names[Math.floor((Math.random() * 37) + 1)];
  },
  loadEdges: function (dataToProcess) {
    var edges = [];
    var userSocialRank = dataToProcess.userSocialRank;
    var edgeList = dataToProcess.edgeList;

    var dataLoader = this;
    $.each(edgeList, function (index, edge) {
      edges.push(dataLoader.getEdge(userSocialRank, edge));
    });
    return edges;
  },
  getEdge: function (userSocialRank, edge) {
    var edge;
    edge = {
      from: userSocialRank.facebookId,
      to: edge.userSocialRank.facebookId,
      value: edge.relationshipStrength.strength,
      title: this.getEdgeTitle(edge.relationshipStrength.strength)
    };

    return edge;
  },
  getEdgeTitle: function (strength) {
    var title = '<ul class=\"list-group\">';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-primary\">Relationship Strength:'
        + strength + '</span></li>';
    title += '</ul>';

    return title;
  }
};