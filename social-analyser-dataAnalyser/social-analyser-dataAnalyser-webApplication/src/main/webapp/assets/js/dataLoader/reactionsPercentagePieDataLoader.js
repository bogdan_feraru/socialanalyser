"use strict";

function ReactionsPercentagePieDataLoader(url) {
  this.url=url;
}

ReactionsPercentagePieDataLoader.prototype = {

  loadData: function () {
    var url = this.url;
    var reactionsCounts;
    $.get({url: url, async: false}, function (data) {
      reactionsCounts = data;
    });
    return reactionsCounts;
  }
};