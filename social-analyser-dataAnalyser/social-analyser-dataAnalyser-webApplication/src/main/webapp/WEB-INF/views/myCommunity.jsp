<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <title>General Statistics</title>
  <%-- JQuery --%>
  <script src="../../assets/libs/js/jquery.js" type="text/javascript"></script>
  <%-- Bootstrap --%>
  <link href="../../assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
  <script src="../../assets/libs/bootstrap/js/bootstrap.js" rel="stylesheet"></script>
  <%-- HighCharts --%>
  <script src="../../assets/libs/highcharts/code/highcharts.js"></script>
  <script src="../../assets/libs/highcharts/code/modules/exporting.js"></script>
  <script src="../../assets/libs/highcharts/code/themes/grid-light.js" type="text/javascript"></script>
  <%-- Vis --%>
  <script type="text/javascript" src="../../assets/libs/js/vis/vis.js"></script>
  <link href="../../assets/libs/js/vis/vis-network.min.css" rel="stylesheet" type="text/css"/>
  <%-- MyBiggestFans --%>
  <script src="../../assets/js/dataLoader/myBiggestFansDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myBiggestFansView.js" type="text/javascript"></script>
  <%-- Amcharts --%>
  <script src="../../assets/libs/js/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../assets/libs/js/amcharts/pie.js" type="text/javascript"></script>
  <%-- ReactionPercentagePie --%>
  <script src="../../assets/js/dataLoader/reactionsPercentagePieDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPercentagePieView.js" type="text/javascript"></script>
  <%-- ReactionPeriodically --%>
  <script src="../../assets/js/dataLoader/reactionsPeriodicallyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPeriodicallyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyYearly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyYearlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyYearlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyMonthlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyMonthlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyHourlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyHourlyView.js" type="text/javascript"></script>
  <%-- MostPowerfullTags --%>
  <script src="../../assets/js/dataLoader/topTagsBubbleDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/topTagsBubblesView.js" type="text/javascript"></script>
  <%-- D3 --%>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.js"></script>
  <%-- MostPowerfullPosts --%>
  <script src="../../assets/js/dataLoader/myCommunityDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myCommunityView.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/dashboard">SocialBuddy</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/dashboard">Dashboard</a></li>
      <li><a href="/myCommunity">My Community</a></li>
      <li><a href="/statistics">General Statistics</a></li>
      <sec:authorize access="hasAuthority('ADMIN')">
        <li><a href="/admin">Admin Console</a></li>
      </sec:authorize>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/signout"><span class="glyphicon glyphicon-log-in"></span> Signout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="page-header">
    <h2>My Community</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div id="myCommunity"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script type="text/javascript">
  $(window).on('load', function (e) {
    if (window.location.hash == '#_=_') {
      window.location.hash = '';
      history.pushState('', document.title, window.location.pathname);
      e.preventDefault();
    }
  });

</script>

<script type="text/javascript">
  $(function () {
    var myCommunityView = new MyCommunityView("myCommunity", "/user/community");
    myCommunityView.render();
  });
</script>
</html>
