<!doctype html>
<html>
<head>
  <title>Network | Sizing</title>

  <style type="text/css">
    html, body {
      font: 10pt arial;
    }
    #mynetwork {
      width: 1000px;
      height: 600px;
      border: 1px solid lightgray;
    }
  </style>

  <script type="text/javascript" src="../../assets/libs/js/vis/vis.js"></script>
  <link href="../../assets/libs/js/vis/vis-network.min.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript">
    var nodes = null;
    var edges = null;
    var network = null;

    function draw() {
      // create people.
      // value corresponds with the age of the person
      var DIR = '../../assets/img/indonesia/';
      nodes = [
        {id: 1, shape: 'circularImage', image: DIR + '1.png', value: 2,  label: 'Algie' },
        {id: 2,  shape: 'circularImage', image: DIR + '2.png',value: 50, label: 'Alston'},
        {id: 3,  shape: 'circularImage', image: DIR + '3.png',value: 12, label: 'Barney'},
        {id: 4,  shape: 'circularImage', image: DIR + '4.png',value: 16, label: 'Coley' },
        {id: 5,  shape: 'circularImage', image: DIR + '5.png',value: 17, label: 'Grant' },
        {id: 6,  shape: 'circularImage', image: DIR + '6.png',value: 15, label: 'Langdon'},
        {id: 7,  shape: 'circularImage', image: DIR + '7.png',value: 6,  label: 'Lee'},
        {id: 8,  shape: 'circularImage', image: DIR + '8.png',value: 5,  label: 'Merlin'},
        {id: 9,  shape: 'circularImage', image: DIR + '9.png',value: 30, label: 'Mick'},
        {id: 10, shape: 'circularImage', image: DIR + '10.png',value: 108, label: 'Tod'},
      ];

      // create connections between people
      // value corresponds with the amount of contact between two people
      edges = [
        {from: 2, to: 8, value: 3, title: '3 emails per week'},
        {from: 2, to: 9, value: 5, title: '5 emails per week'},
        {from: 2, to: 10,value: 1, title: '1 emails per week'},
        {from: 2, to: 5, value: 8, title: '8 emails per week'},
        {from: 2, to: 6, value: 2, title: '2 emails per week'},
        {from: 4, to: 2, value: 1, title: '1 emails per week'},
        {from: 2, to: 1,value: 2, title: '2 emails per week'},
        {from: 2, to: 3, value: 6, title: '6 emails per week'},
        {from: 2, to: 7, value: 4, title: '4 emails per week'}
      ];

      // Instantiate our network object.
      var container = document.getElementById('mynetwork');
      var data = {
        nodes: nodes,
        edges: edges
      };
      var options = {
        nodes: {
          shape: 'dot',
          scaling:{
            label: {
              min:8,
              max:20
            }
          }
        }
      };
      network = new vis.Network(container, data, options);
    }
  </script>
  
</head>
<body onload="draw()">
<p>
  Scale nodes and edges depending on their value. Hover over edges to get a popup with more information.
</p>
<div id="mynetwork"></div>
</body>
</html>
