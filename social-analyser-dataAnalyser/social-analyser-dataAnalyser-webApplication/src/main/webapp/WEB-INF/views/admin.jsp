<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
  <title>Dashboard</title>
  <%-- JQuery --%>
  <script src="../../assets/libs/js/jquery.js" type="text/javascript"></script>
  <%-- Bootstrap --%>
  <link href="../../assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
  <script src="../../assets/libs/bootstrap/js/bootstrap.js" rel="stylesheet"></script>
  <%-- HighCharts --%>
  <script src="../../assets/libs/highcharts/code/highcharts.js"></script>
  <script src="../../assets/libs/highcharts/code/modules/exporting.js"></script>
  <script src="../../assets/libs/highcharts/code/themes/grid-light.js" type="text/javascript"></script>
  <%-- MyBiggestFans --%>
  <script src="../../assets/js/dataLoader/myBiggestFansDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myBiggestFansView.js" type="text/javascript"></script>
  <%-- Amcharts --%>
  <script src="../../assets/libs/js/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../assets/libs/js/amcharts/pie.js" type="text/javascript"></script>
  <%-- ReactionPercentagePie --%>
  <script src="../../assets/js/dataLoader/reactionsPercentagePieDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPercentagePieView.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/dashboard">SocialBuddy</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/dashboard">Dashboard</a></li>
      <li><a href="/myCommunity">My Community</a></li>
      <li><a href="/statistics">General Statistics</a></li>
      <sec:authorize access="hasAuthority('ADMIN')">
        <li><a class="active" href="/admin">Admin Console</a></li>
      </sec:authorize>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/signout"><span class="glyphicon glyphicon-log-in"></span> Signout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="page-header">
    <h2>Admin Console</h2>
  </div>
  <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Most Powerful Posts Globally</div>
        <div class="panel-body">
          <form action="/admin/loginAs" method="post">
            <div class="form-group">
              <label for="userName">Log in as: </label>
              <select class="form-control" id="userName" name="userName">
                <c:forEach var="applicationUser" items="${applicationUserList}">
                  <option value="${applicationUser.facebookId}">${fn:split(applicationUser.name, ' ')[0].toString()}</option>
                </c:forEach>
              </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-sm-3"></div>
  </div>
</div>

</body>
<script type="text/javascript">
  $(window).on('load', function (e) {
    if (window.location.hash == '#_=_') {
      window.location.hash = '';
      history.pushState('', document.title, window.location.pathname);
      e.preventDefault();
    }
  });
</script>
</html>
