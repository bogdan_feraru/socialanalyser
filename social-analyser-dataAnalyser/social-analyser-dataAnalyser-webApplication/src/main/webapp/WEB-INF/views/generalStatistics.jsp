<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <title>General Statistics</title>
  <%-- JQuery --%>
  <script src="../../assets/libs/js/jquery.js" type="text/javascript"></script>
  <%-- Bootstrap --%>
  <link href="../../assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
  <script src="../../assets/libs/bootstrap/js/bootstrap.js" rel="stylesheet"></script>
  <%-- HighCharts --%>
  <script src="../../assets/libs/highcharts/code/highcharts.js"></script>
  <script src="../../assets/libs/highcharts/code/modules/exporting.js"></script>
  <script src="../../assets/libs/highcharts/code/themes/grid-light.js" type="text/javascript"></script>
  <%-- MyBiggestFans --%>
  <script src="../../assets/js/dataLoader/myBiggestFansDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myBiggestFansView.js" type="text/javascript"></script>
  <%-- Amcharts --%>
  <script src="../../assets/libs/js/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../assets/libs/js/amcharts/pie.js" type="text/javascript"></script>
  <%-- ReactionPercentagePie --%>
  <script src="../../assets/js/dataLoader/reactionsPercentagePieDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPercentagePieView.js" type="text/javascript"></script>
  <%-- ReactionPeriodically --%>
  <script src="../../assets/js/dataLoader/reactionsPeriodicallyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPeriodicallyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyYearly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyYearlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyYearlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyMonthlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyMonthlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyHourlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyHourlyView.js" type="text/javascript"></script>
  <%-- MostPowerfullTags --%>
  <script src="../../assets/js/dataLoader/topTagsBubbleDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/topTagsBubblesView.js" type="text/javascript"></script>
  <%-- D3 --%>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.js"></script>
  <%-- MostPowerfullPosts --%>
  <script src="../../assets/js/dataLoader/mostPowerfulPostsDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/mostPowerfulPostsView.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/dashboard">SocialBuddy</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/dashboard">Dashboard</a></li>
      <li><a href="/myCommunity">My Community</a></li>
      <li class="active"><a href="/statistics">General Statistics</a></li>
      <sec:authorize access="hasAuthority('ADMIN')">
        <li><a href="/admin">Admin Console</a></li>
      </sec:authorize>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/signout"><span class="glyphicon glyphicon-log-in"></span> Signout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="page-header">
    <h2>DASHBOARD</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">Most Powerful Posts Globally</div>
        <div class="panel-body">
          <div id="mostPowerfulPosts"></div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">Received Reactions Globally</div>
        <div class="panel-body">
          <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#yearly" data-toggle="tab">Yearly</a></li>
            <li class=""><a href="#monthly" data-toggle="tab">Monthly</a></li>
            <li class=""><a href="#hourly" data-toggle="tab">Hourly</a></li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="yearly">
              <div id="reactionsGlobalYearly"></div>
            </div>
            <div class="tab-pane fade" id="monthly">
              <div id="reactionsGlobalMonthly"></div>
            </div>
            <div class="tab-pane fade" id="hourly">
              <div id="reactionsGlobalHourly"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">Most Used Tags</div>
        <div class="panel-body">
          <div id="bubbleTags"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</body>
<script type="text/javascript">
  $(window).on('load', function (e) {
    if (window.location.hash == '#_=_') {
      window.location.hash = '';
      history.pushState('', document.title, window.location.pathname);
      e.preventDefault();
    }
  });

  $(function () {
    var reactionsGloballyYearlyDataView = new ReactionsGloballyYearlyView('reactionsGlobalYearly', '/reactions/globally/yearly');
    reactionsGloballyYearlyDataView.render();
  });
  $(function () {
    var mostPowerfulPostsView = new MostPowerfulPostsView('mostPowerfulPosts', '/posts/top/globally');
    mostPowerfulPostsView.render();
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href");
    var reactionsPeriodicallyView;
    if (target == '#yearly') {
      var reactionsGloballyYearlyDataView = new ReactionsGloballyYearlyView('reactionsGlobalYearly', '/reactions/globally/yearly');
      reactionsGloballyYearlyDataView.render();
    } else if (target == '#monthly') {
      reactionsPeriodicallyView =
          new ReactionsGloballyMonthlyView('reactionsGlobalMonthly', '/reactions/globally/monthly');
      reactionsPeriodicallyView.render();
    } else if (target == '#hourly') {
      reactionsPeriodicallyView =
          new ReactionsGloballyHourlyView('reactionsGlobalHourly', '/reactions/globally/hourly');
      reactionsPeriodicallyView.render();
    }
  });
</script>

<script>
  $(function () {
    var topTagBubblesView = new TopTagBubblesView('bubbleTags', '/tag/mostPowerful');
    topTagBubblesView.render();
  });
</script>
</html>
