<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Biggest Fans</title>
  <script src="../../assets/libs/highcharts/code/highcharts.js"></script>
  <script src="../../assets/libs/highcharts/code/modules/exporting.js"></script>
  <script src="../../assets/libs/js/jquery.js" type="text/javascript"></script>
  <script src="../../assets/js/dataLoader/myBiggestFansDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myBiggestFansView.js" type="text/javascript"></script>
  <script src="../../assets/libs/highcharts/code/themes/grid-light.js" type="text/javascript"></script>
</head>
<body>
<div id="biggestFans" style="min-width: 310px; max-width: 1200px; height: 600px; margin: 0 auto"></div>
</body>
<script type="text/javascript">
  $(function () {
    var biggestFansView = new BiggestFansView();
    biggestFansView.render();
  });
</script>
</html>
