<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
  <title>Sign in</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Social Analyser Registration Page</title>
  <link href="../../assets/libs/css/bootstrap.css" rel="stylesheet">
  <link href="../../assets/libs/css/font-awesome.css" rel="stylesheet">
  <link href="../../assets/libs/css/docs.css" rel="stylesheet">
  <link href="../../assets/libs/css/bootstrap-social.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
  <div class=" page-header">
    <h1 align="center">Welcome to Social Buddy</h1>
  </div>

</div>

<div class="container">
  <div class="col-sm-4 col-sm-offset-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Get Started</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12 ">
            <%--<p>If you wanna help a student please register! Thank you!</p>--%>
            <form id="tw_signin" action="<c:url value="/signin/facebook"/>" method="POST">
              <input type="hidden" name="scope"
                     value="email,user_hometown,user_religion_politics,user_likes,
               user_status,user_about_me,user_location,user_tagged_places,
               user_birthday,user_photos,user_videos,user_education_history,
               user_posts,user_website,user_friends,user_relationship_details,
               user_work_history,user_games_activity,user_relationships,
               user_actions.books,user_actions.music,user_actions.video,
               user_actions.fitness,user_actions.news,read_custom_friendlists"/>
              <button type="submit" class="btn btn-block btn-social btn-facebook">
                <span class="fa fa-facebook"></span> SignIn / Get started using Facebook
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
