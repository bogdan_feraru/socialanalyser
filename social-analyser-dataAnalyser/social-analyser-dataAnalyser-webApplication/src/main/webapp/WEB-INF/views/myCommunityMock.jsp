<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
  <title>General Statistics</title>
  <%-- JQuery --%>
  <script src="../../assets/libs/js/jquery.js" type="text/javascript"></script>
  <%-- Bootstrap --%>
  <link href="../../assets/libs/bootstrap/css/bootstrap.css" rel="stylesheet">
  <script src="../../assets/libs/bootstrap/js/bootstrap.js" rel="stylesheet"></script>
  <%-- HighCharts --%>
  <script src="../../assets/libs/highcharts/code/highcharts.js"></script>
  <script src="../../assets/libs/highcharts/code/modules/exporting.js"></script>
  <script src="../../assets/libs/highcharts/code/themes/grid-light.js" type="text/javascript"></script>
  <%-- Vis --%>
  <script type="text/javascript" src="../../assets/libs/js/vis/vis.js"></script>
  <link href="../../assets/libs/js/vis/vis-network.min.css" rel="stylesheet" type="text/css"/>
  <%-- MyBiggestFans --%>
  <script src="../../assets/js/dataLoader/myBiggestFansDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/myBiggestFansView.js" type="text/javascript"></script>
  <%-- Amcharts --%>
  <script src="../../assets/libs/js/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../assets/libs/js/amcharts/pie.js" type="text/javascript"></script>
  <%-- ReactionPercentagePie --%>
  <script src="../../assets/js/dataLoader/reactionsPercentagePieDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPercentagePieView.js" type="text/javascript"></script>
  <%-- ReactionPeriodically --%>
  <script src="../../assets/js/dataLoader/reactionsPeriodicallyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsPeriodicallyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyYearly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyYearlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyYearlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyMonthlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyMonthlyView.js" type="text/javascript"></script>
  <%-- ReactionGloballyMonthly --%>
  <script src="../../assets/js/dataLoader/reactionsGloballyHourlyDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/reactionsGloballyHourlyView.js" type="text/javascript"></script>
  <%-- MostPowerfullTags --%>
  <script src="../../assets/js/dataLoader/topTagsBubbleDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/topTagsBubblesView.js" type="text/javascript"></script>
  <%-- D3 --%>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.js"></script>
  <%-- MostPowerfullPosts --%>
  <script src="../../assets/js/dataLoader/mostPowerfulPostsDataLoader.js" type="text/javascript"></script>
  <script src="../../assets/js/view/mostPowerfulPostsView.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/dashboard">SocialBuddy</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/dashboard">Dashboard</a></li>
      <li><a href="/myCommunity">My Community</a></li>
      <li><a href="/statistics">General Statistics</a></li>
      <sec:authorize access="hasAuthority('ADMIN')">
        <li><a href="/admin">Admin Console</a></li>
      </sec:authorize>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="/signout"><span class="glyphicon glyphicon-log-in"></span> Signout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
  <div class="page-header">
    <h2>My Community</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div id="myCommunity"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script type="text/javascript">
  $(window).on('load', function (e) {
    if (window.location.hash == '#_=_') {
      window.location.hash = '';
      history.pushState('', document.title, window.location.pathname);
      e.preventDefault();
    }
  });
  $(function () {
    draw();
  });
</script>

<script type="text/javascript">

  var nodes = null;
  var edges = null;
  var network = null;

  function draw() {
    // create people.
    // value corresponds with the age of the person
    var title = '<ul class=\"list-group\">';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-primary\">Tagged Ratio:'
        + 0.7 + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-success\">Received Per Given Reactions:'
        + 0.9 + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-info\">Average Post Power:'
        + 0.9 + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-warning\">Average Post Impact Time:'
        + 0.9 + '</span></li>';
    title +=
        '<li class=\"list-group-item\"><span class=\"label label-danger\">Received Per Given Comment Ratio:'
        + 0.99 + '</span></li>';
    title += '</ul>';



    var DIR = '../../assets/img/indonesia/';
    nodes = [
      {id: 1, shape: 'circularImage', image: DIR + '1.png', value: 2, label: 'Algie'},
      {id: 2, shape: 'circularImage', image: DIR + '2.png', value: 50, label: 'Alston'},
      {id: 3, shape: 'circularImage', image: DIR + '3.png', value: 12, label: 'Barney'},
      {id: 4, shape: 'circularImage', image: DIR + '4.png', value: 90, label: 'Coley'},
      {id: 5, shape: 'circularImage', image: DIR + '5.png', value: 17, label: 'Grant'},
      {id: 6, shape: 'circularImage', image: DIR + '6.png', value: 15, label: 'Langdon'},
      {id: 7, shape: 'circularImage', image: DIR + '7.png', value: 6, label: 'Lee'},
      {id: 8, shape: 'circularImage', image: DIR + '8.png', value: 5, label: 'Merlin'},
      {id: 9, shape: 'circularImage', image: DIR + '9.png', value: 50, label: 'Mick'},
      {id: 10, shape: 'circularImage', image: DIR + '10.png', value: 108, label: 'Tod', title:title},
      {id: 11, shape: 'circularImage', image: DIR + '9.png', value: 50, label: 'Mick'},
      {id: 12, shape: 'circularImage', image: DIR + '10.png', value: 108, label: 'Tod', title:title},
    ];

    // create connections between people
    // value corresponds with the amount of contact between two people
    edges = [
      {from: 2, to: 8, value: 2, title: '3 emails per week'},
      {from: 2, to: 9, value: 2, title: '5 emails per week'},
      {from: 2, to: 10, value: 2, title: '1 emails per week'},
      {from: 2, to: 5, value: 2, title: '8 emails per week'},
      {from: 2, to: 6, value: 2, title: '2 emails per week'},
      {from: 4, to: 2, value: 2, title: '1 emails per week'},
      {from: 2, to: 1, value: 2, title: '2 emails per week'},
      {from: 2, to: 3, value: 2, title: '6 emails per week'},
      {from: 2, to: 7, value: 2, title: '4 emails per week'},
      {from: 11, to: 12, value: 100, title: '4 emails per week'}
    ];

    // Instantiate our network object.
    var container = document.getElementById('myCommunity');
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {
      nodes: {
        shape: 'dot',
        scaling: {
          label: {
            min: 8,
            max: 20
          }
        }
      },
//      physics:false
    };
    network = new vis.Network(container, data, options);
    network.on("click", function (params) {
      // Check if you clicked on a node; if so, display the title (if any) in a popup
      network.interactionHandler._checkShowPopup(params.pointer.DOM);
    });
  }

</script>
</html>
