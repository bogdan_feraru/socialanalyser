package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.config.BusinessConfig;


/**
 * Main configuration class for the application.
 * Turns on @Component scanning, loads externalized application.properties, and sets up the database.
 *
 * @author Bogdan Feraru
 */
@Configuration
@ComponentScan(basePackages = "ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user", excludeFilters = {
    @ComponentScan.Filter(Configuration.class)})
@Import(BusinessConfig.class)
@PropertySource("classpath:application.properties")
@PropertySource("classpath:userTokenDataSource.properties")
public class MainConfig {

  @Bean
  public DataSource dataSource(Environment env) {
    BasicDataSource basicDataSource = new BasicDataSource();

    basicDataSource.setDriverClassName(env.getProperty("user.token.database.driverClassName"));
    basicDataSource.setUrl(env.getProperty("user.token.database.url"));
    basicDataSource.setUsername(env.getProperty("user.token.database.user"));
    basicDataSource.setPassword(env.getProperty("user.token.database.password"));
    basicDataSource.setRemoveAbandonedOnMaintenance(true);
    basicDataSource.setInitialSize(20);
    basicDataSource.setMaxTotal(30);

    return basicDataSource;
  }

  @Bean
  public JdbcTemplate jdbcTemplate(DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }
}
