package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by bogdan.feraru on 6/11/2017.
 */
@Controller
public class PagesController {

  @RequestMapping("/dashboard")
  public String dashboard() {
    return "dashboard";
  }

  @RequestMapping("/statistics")
  public String generalStatistics() {
    return "generalStatistics";
  }

  @RequestMapping("/myCommunity")
  public String myCommunity() {
    return "myCommunity";
  }

}
