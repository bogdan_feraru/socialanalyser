package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Arrays;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.User;

@Service
public class FacebookSignInAdapter implements SignInAdapter {

  private static final Logger LOGGER = LoggerFactory.getLogger(FacebookSignInAdapter.class);
  @Autowired
  private UserRepository userRepository;

  @Override
  public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
    String facebookId = connection.fetchUserProfile().getId();
    User user = userRepository.findByFacebookId(facebookId);
    SecurityContextHolder.getContext().setAuthentication(
        new UsernamePasswordAuthenticationToken(localUserId, null, Arrays.asList(new SimpleGrantedAuthority(
            user.getRole().toString()))));
    LOGGER.info(
        "USER " + user.getName() + " with facebook id: " + facebookId + "and role:" + user.getRole() + " logged in.");
    return "/dashboard";
  }
}
