package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * Simple little @Controller that invokes Facebook and renders the result.
 * The injected {@link Facebook} reference is configured with the required authorization credentials for the current
 * user behind the scenes.
 *
 * @author Bogdan Feraru
 */
@Controller
public class HomeController {


  @Autowired
  private ApplicationContext applicationContext;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String home(Model model) {
    Facebook facebook = applicationContext.getBean(Facebook.class);
    List<Reference> friends = facebook.friendOperations().getFriends();
    String userName = facebook.userOperations().getUserProfile().getName();
    model.addAttribute("userName", userName);
    model.addAttribute("friends", friends);
    return "home";
  }

  @RequestMapping(value = "/vis", method = RequestMethod.GET)
  public String vis() {
    return "scalingNodesEdgesLabels";
  }

  @RequestMapping(value = "/amcharts", method = RequestMethod.GET)
  public String amcharts() {
    return "pie3D";
  }

  @RequestMapping(value = "/biggestFans", method = RequestMethod.GET)
  public String biggestFans() {
    return "biggestFans";
  }
}
