package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.Role;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.User;

@Repository
public class UserRepository {

  private static final String SELECT_USER_IDS = "SELECT userconnection.userId FROM userconnection;";
  private static final String SELECT_FROM_USERCONNECTION = "SELECT * FROM userconnection;";
  private static final String REGISTER_USER = "INSERT INTO user (facebookId, name, email, role, registrationDate) "
                                              + "VALUES (?, ?, ?, ?, ?)";
  private static final String FIND_BY_USERNAME = "SELECT * FROM user WHERE user.email=?";
  private static final String FIND_BY_FACEBOOK_ID = "SELECT * FROM user WHERE user.facebookId=?";
  private static final String FIND_BY_NAME = "SELECT * FROM user WHERE user.name=?";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<User> getAllUserId() {
    return jdbcTemplate.query(SELECT_FROM_USERCONNECTION, new SimpleUserRowMapper());
  }

  public void registerUser(User user) {
    jdbcTemplate
        .update(REGISTER_USER, user.getFacebookId(), user.getName(), user.getEmail(), user.getRole().toString(),
                user.getRegistrationDate());
  }

  public User findByUsername(String username) {
    return jdbcTemplate.query(FIND_BY_USERNAME, new Object[]{username}, new SimpleUserRowMapper()).get(0);
  }

  public User findByName(String name) {
    return jdbcTemplate.query(FIND_BY_NAME, new Object[]{name}, new SimpleUserRowMapper()).get(0);
  }

  public User findByFacebookId(String facebookId) {
    return jdbcTemplate.query(FIND_BY_FACEBOOK_ID, new Object[]{facebookId}, new SimpleUserRowMapper()).get(0);
  }

  protected class SimpleUserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
      User user = new User();
      user.setFacebookId(rs.getString(1));
      user.setName(rs.getString(2));
      user.setEmail(rs.getString(3));
      user.setRole(Role.fromString(rs.getString(4)));
      user.setRegistrationDate(Long.parseLong(rs.getString(5)));

      return user;
    }
  }
}
