package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.stream.Collectors;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.CountPerReactionDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserReactionsCountDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.UserSocialCommunityDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.UserOperationsService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.persistence.graph.model.UserReactionsCount;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
@RestController
public class UserOperationsRestController {

  @Autowired
  private UserOperationsService userOperationsService;

  @Autowired
  private Facebook facebook;

  @RequestMapping(value = "/user/reactions/received", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<UserReactionsCount> getReceivedUserReactionCount() {
    List<UserReactionsCount> userReactionsCount = userOperationsService.getReceivedUserReactionCount(getUserId());
    return userReactionsCount;
  }

  @RequestMapping(value = "/user/reactions/given", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<UserReactionsCount> getGivenUserReactionCount() {
    List<UserReactionsCount> userReactionsCount = userOperationsService.getGivenUserReactionCount(getUserId());
    return userReactionsCount;
  }

  @RequestMapping(value = "/user/biggestFans", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<UserReactionsCountDTO> getMyBiggestFans() {
    List<UserReactionsCountDTO> myBiggestFans =
        userOperationsService.getMyBiggestFans(getUserId(), 10L);
    myBiggestFans.sort((item1, item2) -> {
      LongSummaryStatistics collect1 = item1.getCountPerReactionList().stream()
          .collect(Collectors.summarizingLong(CountPerReactionDTO::getReactionCount));
      LongSummaryStatistics collect2 = item2.getCountPerReactionList().stream()
          .collect(Collectors.summarizingLong(CountPerReactionDTO::getReactionCount));
      return -Long.valueOf(collect1.getSum()).compareTo(collect2.getSum());
    });
    return myBiggestFans;
  }

  @RequestMapping(value = "/user/community", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  UserSocialCommunityDTO getSocialCommunity() {
    UserSocialCommunityDTO userSocialCommunity = userOperationsService.getUserSocialCommunity(getUserId());
    return userSocialCommunity;
  }

  @RequestMapping(value = "/user/profilePhoto/{facebookId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  String userProfilePhoto(@PathVariable("facebookId") String userFacebookId) {
    User userProfile = facebook.userOperations().getUserProfile();
    return userProfile.getLink();
  }

  private String getUserId() {
    return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
