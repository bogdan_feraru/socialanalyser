package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.UserRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.User;

/**
 * Created by bogdan.feraru on 6/12/2017.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
  @Autowired
  private UserService userService;
  @Autowired
  private UserRepository userRepository;

  @RequestMapping("")
  public String getAllApplicationUsers(Model model) {
    model.addAttribute("applicationUserList", userService.getAllApplicationUsers());
    return "admin";
  }

  @RequestMapping("/loginAs")
  public String signInAs(@ModelAttribute("userName") String userName) {
    changeAuthentication(userName);
    return "dashboard";
  }

  private void changeAuthentication(@ModelAttribute("userName") String facebookId) {
    if (facebookId != null && !facebookId.equals("")) {
      User user = userRepository.findByFacebookId(facebookId);
      if (user != null) {
        SecurityContext context = SecurityContextHolder.getContext();
        LOGGER.info(
            "ADMIN " + context.getAuthentication().getName() + " : " + context.getAuthentication().getPrincipal()
            + " logged in as " + user.getName() + " : " + user.getFacebookId());
        context.setAuthentication(
            new UsernamePasswordAuthenticationToken(user.getFacebookId(), null,
                                                    Arrays.asList(new SimpleGrantedAuthority(
                                                        user.getRole().toString()))));
      }
    }
  }
}
