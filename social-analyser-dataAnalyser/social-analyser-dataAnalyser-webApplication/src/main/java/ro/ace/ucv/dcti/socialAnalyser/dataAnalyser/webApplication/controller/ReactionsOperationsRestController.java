package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountHourlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountMonthlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.ReactionCountYearlyDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.ReactionOperationsService;

/**
 * Created by bogdan.feraru on 6/8/2017.
 */
@RestController
public class ReactionsOperationsRestController {

  @Autowired
  private ReactionOperationsService reactionOperationsService;

  @RequestMapping(value = "/reactions/types", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<String> getAllReactionType() {
    return reactionOperationsService.getAllReactionsTypes();
  }

  @RequestMapping(value = "/reactions/received/monthly", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<ReactionCountMonthlyDTO> getReactionsMonthly() {
    List<ReactionCountMonthlyDTO> reactionsInYearMonthly =
        reactionOperationsService.getReceivedReactionsInYearMonthly(getUserId(), 2016);
    return reactionsInYearMonthly;
  }

  @RequestMapping(value = "/reactions/globally/yearly", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<ReactionCountYearlyDTO> getReactionsGloballyYearly() {
    List<ReactionCountYearlyDTO> reactionsGloballyYearly = reactionOperationsService.getReactionsGloballyYearly();
    return reactionsGloballyYearly;
  }

  @RequestMapping(value = "/reactions/globally/monthly", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<ReactionCountMonthlyDTO> getReactionsGloballyMonthly() {
    List<ReactionCountMonthlyDTO> reactionsGloballyMonthly = reactionOperationsService.getReactionsGloballyMonthly();
    return reactionsGloballyMonthly;
  }

  @RequestMapping(value = "/reactions/globally/hourly", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<ReactionCountHourlyDTO> getReactionsGloballyHourly() {
    List<ReactionCountHourlyDTO> reactionsInYearMonthly = reactionOperationsService.getReactionsGloballyHourly();
    return reactionsInYearMonthly;
  }

  private String getUserId() {
    return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
