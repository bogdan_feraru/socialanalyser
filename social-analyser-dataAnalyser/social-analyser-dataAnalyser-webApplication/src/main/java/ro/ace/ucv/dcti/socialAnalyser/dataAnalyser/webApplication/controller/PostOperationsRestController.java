package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.PostPowerDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.PostOperationsService;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@RestController
public class PostOperationsRestController {

  @Autowired
  private PostOperationsService postOperationsService;

  @RequestMapping(value = "/posts/top", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  public List<PostPowerDTO> getTopPosts() {
    List<PostPowerDTO> topPostList = postOperationsService.getTopPost(getUserId(), 10);
    return topPostList;
  }

  @RequestMapping(value = "/posts/top/globally", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  public List<PostPowerDTO> getTopPostsGlobally() {
    List<PostPowerDTO> topPostList = postOperationsService.getTopPosts(10);
    return topPostList;
  }

  private String getUserId() {
    return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
}
