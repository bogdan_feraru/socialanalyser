package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model;

/**
 * Simple little User model. Just stores the user's id for simplicity.
 *
 * @author Bogdan Feraru
 */
public final class User {

  private String facebookId;

  private String name;

  private String email;

  private Role role;

  private Long registrationDate;

  public String getFacebookId() {
    return facebookId;
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Long getRegistrationDate() {
    return registrationDate;
  }

  public void setRegistrationDate(Long registrationDate) {
    this.registrationDate = registrationDate;
  }
}
