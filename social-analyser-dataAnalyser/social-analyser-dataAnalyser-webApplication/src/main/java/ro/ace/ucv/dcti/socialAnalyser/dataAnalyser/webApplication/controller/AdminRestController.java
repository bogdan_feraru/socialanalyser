package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 6/12/2017.
 */
@RestController
public class AdminRestController {

  @Autowired
  private UserService userService;

  @RequestMapping("/admin/applicationUsers")
  public List<User> getAllApplicationUsers() {
    return userService.getAllApplicationUsers();
  }
}
