package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.UserRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.User;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(final String username) {
    final User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    return new org.springframework.security.core.userdetails.User(username, "", true, true, true, true, Arrays
        .asList(new SimpleGrantedAuthority(user.getRole().toString())));
  }
}
