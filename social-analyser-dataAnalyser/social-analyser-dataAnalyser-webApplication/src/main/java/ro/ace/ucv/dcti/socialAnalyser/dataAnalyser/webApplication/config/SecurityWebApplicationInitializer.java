package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by bogdan.feraru on 6/6/2017.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

  public SecurityWebApplicationInitializer() {
  }
}
