package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by bogdan.feraru on 6/11/2017.
 */
@Controller
public class AuthenticationController {

  @RequestMapping("/signin")
  public String signIn() {
    return "signin";
  }

  @RequestMapping("/signout")
  public String signOut(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }
    return "redirect:/signin?logout";
  }
}
