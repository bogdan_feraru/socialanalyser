package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import javax.sql.DataSource;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.CustomUserConnectionRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.FacebookConnectionSignUp;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.FacebookSignInAdapter;

/**
 * Spring Social Configuration.
 *
 * @author Bogdan Feraru
 */
@Configuration
@EnableSocial
public class SocialConfig implements SocialConfigurer {

  @Autowired
  private DataSource dataSource;

  @Autowired
  private FacebookConnectionSignUp facebookConnectionSignUp;

  @Override
  public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
    cfConfig.addConnectionFactory(
        new FacebookConnectionFactory(env.getProperty("facebook.appKey"), env.getProperty("facebook.appSecret")));
  }

  /**
   * Singleton data access object providing access to connections across all users.
   */
  @Override
  public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
    CustomUserConnectionRepository repository =
        new CustomUserConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
    repository.setConnectionSignUp(facebookConnectionSignUp);
    return repository;
  }

  @Override
  public UserIdSource getUserIdSource() {
    return () -> {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      return (String) authentication.getPrincipal();
    };
  }

  @Bean
  @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
  public Facebook facebook(ConnectionRepository repository) {
    Connection<Facebook> connection = repository.findPrimaryConnection(Facebook.class);
    return connection != null ? connection.getApi() : null;
  }

  @Bean
  public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator,
                                                           UsersConnectionRepository usersConnectionRepository,
                                                           FacebookSignInAdapter facebookSigninAdapter) {
    return new ProviderSignInController(connectionFactoryLocator, usersConnectionRepository, facebookSigninAdapter);
  }
}
