package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.dto.TagPowerDTO;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.business.service.TagOperationsService;

/**
 * Created by bogdan.feraru on 7/1/2017.
 */
@RestController
public class TagOperationsRestController {

  @Autowired
  private TagOperationsService tagOperationsService;

  @RequestMapping(value = "/tag/mostPowerful", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
  List<TagPowerDTO> getMostPowerfulTags() {
    List<TagPowerDTO> topTags = tagOperationsService.getTopTags(25);
    return topTags;
  }

}
