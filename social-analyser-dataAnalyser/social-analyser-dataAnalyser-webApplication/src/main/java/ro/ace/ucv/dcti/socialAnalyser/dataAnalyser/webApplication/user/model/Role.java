package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model;

/**
 * Created by bogdan.feraru on 6/6/2017.
 */
public enum Role {
  USER,
  ADMIN;

  public static Role fromString(String roleStr) {
    for (Role role : values()) {
      if (role.toString().toLowerCase().equals(roleStr.toLowerCase())) {
        return role;
      }
    }

    return null;
  }
}
