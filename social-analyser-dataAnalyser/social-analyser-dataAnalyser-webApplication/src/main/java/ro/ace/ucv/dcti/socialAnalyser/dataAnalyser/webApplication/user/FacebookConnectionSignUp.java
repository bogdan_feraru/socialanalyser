package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.Role;
import ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.user.model.User;

/**
 * Simple {@link ConnectionSignUp} command that consider the facebook user id as local id too.
 *
 * @author Bogdan Feraru
 */
@Component
public final class FacebookConnectionSignUp implements ConnectionSignUp {

  @Autowired
  private UserRepository userRepository;

  @Override
  public String execute(Connection<?> connection) {
    User user = new User();
    UserProfile userProfile = connection.fetchUserProfile();
    user.setEmail(userProfile.getEmail());
    user.setFacebookId(userProfile.getId());
    user.setName(userProfile.getName());
    user.setRole(Role.USER);
    user.setRegistrationDate(ZonedDateTime.now(ZoneId.of("UTC")).toEpochSecond());
    userRepository.registerUser(user);

    return userProfile.getId();
  }
}
