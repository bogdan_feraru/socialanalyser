package ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * Spring MVC Configuration.
 *
 * @author Bogdan Feraru
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"ro.ace.ucv.dcti.socialAnalyser.dataAnalyser.webApplication.controller"})
public class WebMvcConfig extends WebMvcConfigurerAdapter {

//
//  public void addInterceptors(InterceptorRegistry registry) {
////    registry.addInterceptor(new UserInterceptor(usersConnectionRepository));
//  }

  public void addViewControllers(ViewControllerRegistry registry) {
//    registry.addViewController("/signin");
//    registry.addViewController("/signout");
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
  }

  @Bean
  public ViewResolver viewResolver() {
    InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
    internalResourceViewResolver.setPrefix("/WEB-INF/views/");
    internalResourceViewResolver.setSuffix(".jsp");
    return internalResourceViewResolver;
  }
}
