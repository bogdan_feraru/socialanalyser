package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

/**
 * Created by bogdan.feraru on 6/15/2017.
 */
@NodeEntity(label="YEAR")
public class DomainYear extends DomainTimeObject {

  @Relationship(type = "HAS_MONTH", direction = Relationship.OUTGOING)
  private Set<DomainMonth> months;

  public Set<DomainMonth> getMonths() {
    return months;
  }

  public void setMonths(Set<DomainMonth> months) {
    this.months = months;
  }
}
