package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
public abstract class AttachedDomainObject<T extends FacebookDomainObject> extends DomainObject {

  @Relationship(type = "REFERENCES", direction = Relationship.OUTGOING)
  private T reference;

  public T getReference() {
    return reference;
  }

  public void setReference(T reference) {
    this.reference = reference;
  }
}
