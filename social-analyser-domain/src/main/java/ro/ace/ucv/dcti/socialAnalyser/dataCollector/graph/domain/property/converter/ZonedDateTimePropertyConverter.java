package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.property.converter;

import org.neo4j.ogm.typeconversion.AttributeConverter;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class ZonedDateTimePropertyConverter implements AttributeConverter<ZonedDateTime, Long> {

  @Override
  public Long toGraphProperty(ZonedDateTime value) {
    if (value == null) {
      return null;
    }
    return value.toEpochSecond();
  }

  @Override
  public ZonedDateTime toEntityAttribute(Long value) {
    if (value == null) {
      return null;
    }
    return ZonedDateTime.ofInstant(Instant.ofEpochSecond(value), ZoneOffset.UTC);
  }
}
