package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.config;

import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import javax.annotation.PostConstruct;

@Component
public class ConstraintCreator {

  private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintCreator.class);

  @Autowired
  private Session session;

  @PostConstruct
  public void createConstraints() {
    try {
//      cleanDatabase();
      session.query("CREATE CONSTRAINT ON (ur:POST) ASSERT ur.facebookId IS UNIQUE", new HashMap<>());
      session.query("CREATE CONSTRAINT ON (tag:TAG) ASSERT tag.name IS UNIQUE", new HashMap<>());
      session.query("CREATE CONSTRAINT ON (ur:COMMENT) ASSERT ur.facebookId IS UNIQUE", new HashMap<>());
      session.query("CREATE CONSTRAINT ON (ur:USER) ASSERT ur.facebookId IS UNIQUE", new HashMap<>());
    } catch (Exception ex) {
      LOGGER.error(ex.getMessage(), ex);
    }
  }

  private void cleanDatabase() {
    session.query("MATCH (n) DETACH DELETE n;", new HashMap<>());
    createTemporalMetadata();
  }

  private void createTemporalMetadata() {
    session.query("WITH range(2004, 2018) AS years, range(1,12) AS months, range(0,23) AS hours "
                  + "FOREACH(year IN years | "
                  + "  MERGE (y:YEAR {year: year, externalId: toString(year)}) "
                  + "  FOREACH(month IN months | "
                  + "    CREATE (m:MONTH {month: month}) "
                  + "    MERGE (y)-[:HAS_MONTH]->(m) "
                  + "    FOREACH(day IN (CASE "
                  + "                      WHEN month IN [1,3,5,7,8,10,12] THEN range(1,31) "
                  + "                      WHEN month = 2 THEN "
                  + "                        CASE "
                  + "                          WHEN year % 4 <> 0 THEN range(1,28) "
                  + "                          WHEN year % 100 <> 0 THEN range(1,29) "
                  + "                          WHEN year % 400 <> 0 THEN range(1,29) "
                  + "                          ELSE range(1,28) "
                  + "                        END "
                  + "                      ELSE range(1,30) "
                  + "                    END) |      "
                  + "      CREATE (d:DAY {day: day}) "
                  + "      MERGE (m)-[:HAS_DAY]->(d) "
                  + "      FOREACH(hour IN hours | "
                  + "        CREATE (h:HOUR {hour: hour}) "
                  + "        MERGE (d)-[:HAS_HOUR]->(h) "
                  + "      ) "
                  + "    ) "
                  + "  ) "
                  + ");", new HashMap<>());

    session.query("MATCH (year:YEAR)-[:HAS_MONTH]->(month:MONTH)-[:HAS_DAY]->(day:DAY) "
                  + "WITH year,month,day "
                  + "ORDER BY year.year, month.month, day.day "
                  + "WITH collect(day) AS days "
                  + "FOREACH(i IN range(0, size(days) - 2) | "
                  + "    FOREACH(day1 IN [days[i]] | "
                  + "        FOREACH(day2 IN [days[i + 1]] | "
                  + "            MERGE (day1)-[:NEXT_DAY]->(day2) "
                  + "        )"
                  + "    )"
                  + ");", new HashMap<>());
    session.query("MATCH (year:YEAR)-[:HAS_MONTH]->(month:MONTH)-[:HAS_DAY]->(day:DAY)-[:HAS_HOUR]->(hour:HOUR) "
                  + "WITH year, month, day, hour "
                  + "ORDER BY year.year, month.month, day.day, hour.hour "
                  + "WITH collect(hour) AS hours "
                  + "FOREACH(i IN range(0, size(hours) - 2) | "
                  + "    FOREACH(hour1 IN [hours[i]] | "
                  + "        FOREACH(hour2 IN [hours[i + 1]] | "
                  + "            MERGE (hour1)-[:NEXT_HOUR]->(hour2) "
                  + "        ) "
                  + "    ) "
                  + ");", new HashMap<>());
    session.query("MATCH (y:YEAR)-[:HAS_MONTH]->(m:MONTH) SET m.externalId = y.externalId + m.month;", new HashMap<>());
    session.query(
        "MATCH (m:MONTH) WHERE size(m.externalId) < 6 SET m.externalId = left(m.externalId, 4) + '0' + right(m.externalId,1);",
        new HashMap<>());
    session.query("MATCH (m:MONTH)-[:HAS_DAY]->(d:DAY) SET d.externalId = m.externalId + d.day;", new HashMap<>());
    session.query(
        "MATCH (d:DAY) WHERE size(d.externalId) < 8 SET d.externalId = left(d.externalId, 6) + '0' + right(d.externalId, 1);",
        new HashMap<>());
    session.query("MATCH (d:DAY)-[:HAS_HOUR]->(h:HOUR) SET h.externalId = d.externalId + h.hour;", new HashMap<>());
    session.query(
        "MATCH (h:HOUR) WHERE size(h.externalId) < 10 SET h.externalId = left(h.externalId, 8) + '0' + right(h.externalId, 1);",
        new HashMap<>());

  }
}
