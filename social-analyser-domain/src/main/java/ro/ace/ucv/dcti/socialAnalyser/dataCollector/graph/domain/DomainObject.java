package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.GraphId;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public abstract class DomainObject {

  @GraphId
  protected Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
