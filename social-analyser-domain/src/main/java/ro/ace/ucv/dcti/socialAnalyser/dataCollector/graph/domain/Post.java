package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity(label = "POST")
public class Post extends CreatedFacebookDomainObject {

  @Property
  private String message;

  @Property
  private StatusType statusType;

  @Property
  private String story;

  @Property
  private String link;

  @Property
  private Integer sharesNumber;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Place location;

  @Relationship(type = "TARGETTED_USERS", direction = Relationship.OUTGOING)
  private Set<User> targetedUsers = new HashSet<>();

  @Relationship(type = "TAGGED", direction = Relationship.OUTGOING)
  private Set<User> withTags = new HashSet<>();

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Comments comments;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Reactions reactions;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Set<Tag> messageTag = new HashSet<>();

  public String getStory() {
    return story;
  }

  public void setStory(String story) {
    this.story = story;
  }

  public Comments getComments() {
    return comments;
  }

  public void setComments(Comments comments) {
    this.comments = comments;
  }

  public void addComment(Comment comment) {
    comments.addObject(comment);
  }

  public Reactions getReactions() {
    return reactions;
  }

  public void setReactions(Reactions reactions) {
    this.reactions = reactions;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public StatusType getStatusType() {
    return statusType;
  }

  public void setStatusType(StatusType statusType) {
    this.statusType = statusType;
  }

  public Set<User> getTargetedUsers() {
    return targetedUsers;
  }

  public void setTargetedUsers(Set<User> targetedUsers) {
    if (targetedUsers != null) {
      this.targetedUsers = targetedUsers;
    }
  }

  public Set<User> getWithTags() {
    return withTags;
  }

  public void setWithTags(Set<User> withTags) {
    if (withTags != null) {
      this.withTags = withTags;
    }
  }

  public void addTargetedUserReference(User userReference) {
    this.targetedUsers.add(userReference);
  }

  public void addWithUserReference(User userReference) {
    this.withTags.add(userReference);
  }

  public Set<Tag> getMessageTag() {
    return messageTag;
  }

  public void setMessageTag(Set<Tag> messageTag) {
    this.messageTag = messageTag;
  }

  public void addTag(Tag tag) {
    this.messageTag.add(tag);
  }

  public Place getLocation() {
    return location;
  }

  public void setLocation(Place location) {
    this.location = location;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public Integer getSharesNumber() {
    return sharesNumber;
  }

  public void setSharesNumber(Integer sharesNumber) {
    this.sharesNumber = sharesNumber;
  }
}
