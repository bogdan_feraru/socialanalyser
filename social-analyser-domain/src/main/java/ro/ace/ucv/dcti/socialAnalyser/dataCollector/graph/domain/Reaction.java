package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "REACTION")
public class Reaction extends CreatedFacebookDomainObject {

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private ReactionType reactionType;

  public ReactionType getReactionType() {
    return reactionType;
  }

  public void setReactionType(ReactionType reactionType) {
    this.reactionType = reactionType;
  }
}
