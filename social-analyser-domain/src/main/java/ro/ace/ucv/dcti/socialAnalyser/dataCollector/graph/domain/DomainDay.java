package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
@NodeEntity(label = "DAY")
public class DomainDay extends DomainTimeObject {

  @Relationship(type = "HAS_HOUR", direction = Relationship.OUTGOING)
  private Set<DomainHour> hours;

  @Relationship(type = "NEXT_DAY", direction = Relationship.OUTGOING)
  private DomainDay nextDay;

  public DomainDay getNextDay() {
    return nextDay;
  }

  public void setNextDay(DomainDay nextDay) {
    this.nextDay = nextDay;
  }

  public Set<DomainHour> getHours() {
    return hours;
  }

  public void setHours(Set<DomainHour> hours) {
    this.hours = hours;
  }
}
