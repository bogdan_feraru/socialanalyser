package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "REACTION_TYPE")
public class ReactionType extends DomainObject {

  @Property
  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
