package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public enum PostType {
  LINK,
  STATUS,
  PHOTO,
  VIDEO,
  UNKNOWN
}
