package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

public abstract class SetWrapperAttachedDomainObject<T extends DomainObject, U extends FacebookDomainObject>
    extends AttachedDomainObject<U> {

  @Relationship(type = "HAS", direction = Relationship.UNDIRECTED)
  private Set<T> wrappedObjects = new HashSet<>();

  public Set<T> getWrappedObjects() {
    return wrappedObjects;
  }

  public void setWrappedObjects(Set<T> wrappedObjects) {
    this.wrappedObjects = wrappedObjects;
  }

  public void addObject(T object) {
    this.wrappedObjects.add(object);
  }
}
