package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
public abstract class
HourstampedFacebookDomainObject extends FacebookDomainObject {

  @Relationship(type = "CREATED_AT", direction = Relationship.OUTGOING)
  private DomainHour domainHour;

  public DomainHour getDomainHour() {
    return domainHour;
  }

  public void setDomainHour(DomainHour domainHour) {
    this.domainHour = domainHour;
  }
}
