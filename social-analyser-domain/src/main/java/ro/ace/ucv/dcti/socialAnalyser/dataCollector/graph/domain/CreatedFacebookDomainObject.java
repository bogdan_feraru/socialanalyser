package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.property.converter.ZonedDateTimePropertyConverter;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
public abstract class CreatedFacebookDomainObject extends HourstampedFacebookDomainObject {

  @Relationship(type = "CREATED_BY", direction = Relationship.OUTGOING)
  private User user;

  @Property
  @Convert(ZonedDateTimePropertyConverter.class)
  private ZonedDateTime creationTime;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public ZonedDateTime getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(ZonedDateTime creationTime) {
    this.creationTime = creationTime;
  }
}
