package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Property;

/**
 * Created by bogdan.feraru on 6/15/2017.
 */
public abstract class DomainTimeObject extends DomainObject {

  @Property
  private String externalId;

  public String getExternalId() {
    return externalId;
  }

  public void setExternalId(String externalId) {
    this.externalId = externalId;
  }
}
