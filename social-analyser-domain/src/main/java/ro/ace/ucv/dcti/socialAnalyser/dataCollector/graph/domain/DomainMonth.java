package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

/**
 * Created by bogdan.feraru on 6/15/2017.
 */
@NodeEntity(label = "MONTH")
public class DomainMonth extends DomainTimeObject {

  @Relationship(type = "HAS_DAY", direction = Relationship.OUTGOING)
  private Set<DomainDay> days;

  public Set<DomainDay> getDays() {
    return days;
  }

  public void setDays(Set<DomainDay> days) {
    this.days = days;
  }
}
