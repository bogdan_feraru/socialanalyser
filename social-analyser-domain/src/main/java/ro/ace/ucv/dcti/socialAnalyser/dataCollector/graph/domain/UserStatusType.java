package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
public enum UserStatusType {
  APPLICATION_USER,
  USER_REFERENCE
}
