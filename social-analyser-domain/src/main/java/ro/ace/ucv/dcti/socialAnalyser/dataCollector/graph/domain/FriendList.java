package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "FRIEND_LIST")
public class FriendList extends SetWrapperAttachedDomainObject<User,User> {

}
