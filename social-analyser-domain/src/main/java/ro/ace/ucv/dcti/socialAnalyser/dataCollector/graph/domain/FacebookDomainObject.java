package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.Property;

public abstract class FacebookDomainObject extends DomainObject {

  @Property
  private String facebookId;

  public String getFacebookId() {
    return facebookId;
  }

  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    FacebookDomainObject that = (FacebookDomainObject) o;

    return facebookId != null ? facebookId.equals(that.facebookId) : that.facebookId == null;
  }

  @Override
  public int hashCode() {
    return facebookId != null ? facebookId.hashCode() : 0;
  }
}
