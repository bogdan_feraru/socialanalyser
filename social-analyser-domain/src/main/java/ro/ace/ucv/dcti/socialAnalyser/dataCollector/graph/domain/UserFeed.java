package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity(label = "USER_FEED")
public class UserFeed extends SetWrapperAttachedDomainObject<Post, User> {
}
