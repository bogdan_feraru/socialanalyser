package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@NodeEntity(label = "USER_STATUS")
public class UserStatus extends AttachedDomainObject<User> {

  @Property
  private UserStatusType userStatusType = UserStatusType.USER_REFERENCE;

  public UserStatusType getUserStatusType() {
    return userStatusType;
  }

  public void setUserStatusType(UserStatusType userStatusType) {
    this.userStatusType = userStatusType;
  }
}
