package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
@NodeEntity(label = "HOUR")
public class DomainHour extends DomainTimeObject {

  @Relationship(type = "NEXT_HOUR", direction = Relationship.OUTGOING)
  private DomainHour nextHour;

  public DomainHour getNextHour() {
    return nextHour;
  }

  public void setNextHour(DomainHour nextHour) {
    this.nextHour = nextHour;
  }
}
