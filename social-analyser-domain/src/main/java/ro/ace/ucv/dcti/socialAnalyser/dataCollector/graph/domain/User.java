package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
@NodeEntity(label = "USER")
public class User extends FacebookDomainObject {

  @Property
  private String name;

  @Property
  private String shuffledName;

  @Property
  private String about;

  @Property
  private String gender;

  @Property
  private String email;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private UserStatus userStatus;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private FriendList friendList;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private UserFeed userFeed;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShuffledName() {
    return shuffledName;
  }

  public void setShuffledName(String shuffledName) {
    this.shuffledName = shuffledName;
  }

  public String getAbout() {
    return about;
  }

  public void setAbout(String about) {
    this.about = about;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public UserStatus getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(UserStatus userStatus) {
    this.userStatus = userStatus;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void post(Post post) {
    this.userFeed.addObject(post);
  }

  public UserFeed getUserFeed() {
    return userFeed;
  }

  public void setUserFeed(UserFeed userFeed) {
    this.userFeed = userFeed;
  }

  public FriendList getFriendList() {
    return friendList;
  }

  public void setFriendList(FriendList friendList) {
    this.friendList = friendList;
  }

  public void addFriend(User user) {
    this.friendList.addObject(user);
    user.getFriendList().addObject(user);
  }
}
