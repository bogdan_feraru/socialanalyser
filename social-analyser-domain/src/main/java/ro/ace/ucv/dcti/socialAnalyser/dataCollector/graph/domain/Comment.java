package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "COMMENT")
public class Comment extends CreatedFacebookDomainObject {

  @Property
  private String message;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Comments comments;

  @Relationship(type = "HAS", direction = Relationship.OUTGOING)
  private Reactions reactions;

  public Comments getComments() {
    return comments;
  }

  public void setComments(Comments comments) {
    this.comments = comments;
  }

  public Reactions getReactions() {
    return reactions;
  }

  public void setReactions(Reactions reactions) {
    this.reactions = reactions;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
