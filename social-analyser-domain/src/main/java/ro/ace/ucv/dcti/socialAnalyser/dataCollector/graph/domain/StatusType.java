package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public enum StatusType {
  MOBILE_STATUS_UPDATE,
  CREATED_NOTE,
  ADDED_PHOTOS,
  ADDED_VIDEO,
  SHARED_STORY,
  CREATED_GROUP,
  CREATED_EVENT,
  WALL_POST,
  APP_CREATED_STORY,
  PUBLISHED_STORY,
  TAGGED_IN_PHOTO,
  APPROVED_FRIEND,
  UNKNOWN
}
