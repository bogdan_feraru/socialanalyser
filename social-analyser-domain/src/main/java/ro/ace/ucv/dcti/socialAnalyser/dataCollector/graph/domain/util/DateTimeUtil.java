package ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public final class DateTimeUtil {

  private DateTimeUtil() {
  }

  public static Long getNowTimestampUTC() {
    return ZonedDateTime.now(ZoneId.of("UTC")).toEpochSecond();
  }

  public static ZonedDateTime getDateTimeUTCNow() {
    return ZonedDateTime.now(ZoneId.of("UTC"));
  }
}
