package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserFeed;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.UserFeedRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class UserFeedServiceImpl implements UserFeedService {

  @Autowired
  private UserFeedRepository userFeedRepository;

  @Autowired
  private UserFeedMetadataService userFeedMetadataService;

  @Override
  public synchronized void save(UserFeed userFeed) {
    if (userFeed.getId() != null) {
      userFeedRepository.save(userFeed);
      return;
    }

    UserFeedMetadata userFeedMetadata = new UserFeedMetadata();
    userFeedMetadata.setReference(userFeed);
    userFeedMetadataService.save(userFeedMetadata);
  }

  @Override
  public synchronized UserFeed findByFacebookUserId(String facebookUserId) {
    return userFeedRepository.findByFacebookUserId(facebookUserId);
  }

  @Override
  public synchronized Iterable<UserFeed> findAll() {
    return userFeedRepository.findAll();
  }
}
