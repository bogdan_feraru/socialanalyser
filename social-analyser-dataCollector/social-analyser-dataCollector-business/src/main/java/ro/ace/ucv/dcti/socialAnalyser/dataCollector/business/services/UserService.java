package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.User;


public interface UserService {

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User saveUser(User user);

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User saveUserReference(Reference userReference);

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User saveUserReference(User user);

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User saveUserReference(
      ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User userReference);

  void addPostToUserFeed(String facebookId, Post post);

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User findByFacebookId(String facebookId);

  boolean isApplicationUser(String facebookId);

  void addFriend(String userFacebookId, org.springframework.social.facebook.api.User friend);

  String getOwnerIdOfPost(String postFacebookId);

  String getOwnerIdOfComment(String commentFacebookId);

  String getOwnerIdOfCommentComment(String commentFacebookId);
}
