package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.TagService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.TagRepository;

/**
 * Created by bogdan.feraru on 5/7/2017.
 */
@Service
public class TagServiceImpl implements TagService {

  private static final Logger LOGGER = LoggerFactory.getLogger(TagServiceImpl.class);

  @Autowired
  private TagRepository tagRepository;

  @Override
  public void save(Tag tag) {
    tag.setName(tag.getName().toLowerCase());
    Tag tagDomain = tagRepository.findByName(tag.getName());
    if (tagDomain != null) {
      LOGGER.info("Tag with name " + tag.getName() + " already exists.");
      return;
    }

    tagRepository.save(tag);
  }

  @Override
  public Tag find(String name) {
    return tagRepository.findByName(name.toLowerCase());
  }
}
