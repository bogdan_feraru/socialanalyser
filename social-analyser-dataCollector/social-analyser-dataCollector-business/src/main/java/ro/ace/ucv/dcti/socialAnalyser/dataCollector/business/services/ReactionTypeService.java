package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
public interface ReactionTypeService {

  ReactionType findByName(String name);

  ReactionType save(ReactionType reactionType);

  ReactionType save(String reactionType);
}
