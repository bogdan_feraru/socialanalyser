package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FriendList;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface FriendListService {

  void save(FriendList friendList);

  FriendList findByUserFacebookId(String facebookUserId);
}
