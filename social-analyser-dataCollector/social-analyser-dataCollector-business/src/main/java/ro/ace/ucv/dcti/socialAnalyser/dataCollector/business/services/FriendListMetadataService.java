package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface FriendListMetadataService {

  void save(FriendListMetadata friendListMetadata);

  FriendListMetadata findByOwningEntityId(String userFacebookId);

  List<String> getUsersWithNonCollectedFriendLists(Long upperTimeLimit);
}
