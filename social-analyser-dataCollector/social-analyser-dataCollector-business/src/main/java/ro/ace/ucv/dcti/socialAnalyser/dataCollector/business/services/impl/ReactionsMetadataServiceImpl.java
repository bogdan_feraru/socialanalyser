package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.ReactionsMetadataRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class ReactionsMetadataServiceImpl implements ReactionsMetadataService {

  @Autowired
  private ReactionsMetadataRepository reactionsMetadataRepository;

  @Override
  public synchronized void save(ReactionsMetadata reactionsMetadata) {
    reactionsMetadataRepository.save(reactionsMetadata);
  }

  @Override
  public synchronized ReactionsMetadata findByOwningEntityId(String owningEntityId) {
    return reactionsMetadataRepository.findByOwningEntityId(owningEntityId);
  }

  @Override
  public synchronized Collection<String> getOldestVisitedPostReactions(Long offset, int limit) {
    return reactionsMetadataRepository.getOldestVisitedPostReactions(offset, limit);
  }

  @Override
  public synchronized Collection<String> getOldestVisitedPostCommentReactions(Long offset, int limit) {
    return reactionsMetadataRepository.getOldestVisitedPostCommentReactions(offset, limit);
  }

  @Override
  public synchronized Collection<String> getOldestVisitedCommentCommentReactions(Long offset, int limit) {
    return reactionsMetadataRepository.getOldestVisitedCommentCommentReactions(offset, limit);
  }
}
