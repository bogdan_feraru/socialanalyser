package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.CommentsMetadataRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class CommentsMetadataServiceImpl implements CommentsMetadataService {

  @Autowired
  private CommentsMetadataRepository commentsMetadataRepository;

  @Override
  public synchronized void save(CommentsMetadata commentsMetadata) {
    if (commentsMetadata.getId() != null) {
      update(commentsMetadata);
      return;
    }

    commentsMetadataRepository.save(commentsMetadata);
  }

  private void update(CommentsMetadata commentsMetadata) {
    CommentsMetadata existingCommentsMetadata = commentsMetadataRepository.findOne(commentsMetadata.getId());
    existingCommentsMetadata.setLastExtractedCreationDate(commentsMetadata.getLastExtractedCreationDate());
    existingCommentsMetadata.setLastVisitedDate(commentsMetadata.getLastVisitedDate());
    commentsMetadataRepository.save(existingCommentsMetadata);
  }

  @Override
  public CommentsMetadata findByOwningEntityId(String owningEntityId) {
    return commentsMetadataRepository.findByOwningEntityId(owningEntityId);
  }

  @Override
  public Comments getCommentsByMetadata(CommentsMetadata metadata) {
    return commentsMetadataRepository.getCommentsByMetadataId(metadata.getId());
  }

  @Override
  public Collection<String> getOldestVisitedPostComments(Long timestampOffset, Integer limit) {
    return commentsMetadataRepository.getOldestVisitedPostComments(timestampOffset, limit);
  }

  @Override
  public Collection<String> getOldestVisitedPostComments(Integer limit) {
    return commentsMetadataRepository.getOldestVisitedPostComments(limit);
  }

  @Override
  public Collection<String> getOldestVisitedCommentComments(Long offset, Integer limit) {
    return commentsMetadataRepository.getOldestVisitedCommentComments(offset, limit);
  }
}
