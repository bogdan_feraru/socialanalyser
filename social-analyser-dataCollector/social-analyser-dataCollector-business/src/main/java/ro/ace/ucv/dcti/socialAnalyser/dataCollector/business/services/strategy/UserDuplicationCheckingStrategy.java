package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.strategy;

/**
 * Created by bogdan.feraru on 6/17/2017.
 */
public interface UserDuplicationCheckingStrategy {

  boolean isSame(String facebookUserId1, String facebookUserId2);
}
