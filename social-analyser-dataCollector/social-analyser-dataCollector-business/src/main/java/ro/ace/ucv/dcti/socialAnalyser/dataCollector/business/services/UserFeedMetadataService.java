package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import java.time.ZonedDateTime;
import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
public interface UserFeedMetadataService {

  void save(UserFeedMetadata userFeedMetadata);

  UserFeedMetadata getFeedMetadataByFacebookUserId(String facebookUserId);

  ZonedDateTime getLatestFeedPostOfUser(String facebookUserId);

  User getUserWithOldestCollectedFeed();

  User getUserWithOldestCollectedFeed(ZonedDateTime dateTime);

  Iterable<User> getUsersWithOldestCollectedFeed(Integer limit);

  Collection<User> getUsersWithOldestCollectedFeed(Integer limit, ZonedDateTime dateTime);
}
