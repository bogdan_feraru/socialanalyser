package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface ReactionsMetadataService {

  void save(ReactionsMetadata reactionsMetadata);

  ReactionsMetadata findByOwningEntityId(String owningEntityId);

  Collection<String> getOldestVisitedPostReactions(Long offset, int limit);

  Collection<String> getOldestVisitedPostCommentReactions(Long offset, int limit);

  Collection<String> getOldestVisitedCommentCommentReactions(Long offset, int limit);
}
