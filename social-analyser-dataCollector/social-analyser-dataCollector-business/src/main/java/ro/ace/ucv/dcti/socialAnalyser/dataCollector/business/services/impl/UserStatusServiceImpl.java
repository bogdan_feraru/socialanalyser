package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserStatusService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserStatus;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.UserStatusRepository;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
@Service
public class UserStatusServiceImpl implements UserStatusService {

  @Autowired
  UserStatusRepository userStatusRepository;

  @Override
  public synchronized void save(UserStatus userStatus) {
    userStatusRepository.save(userStatus);
  }

  @Override
  public synchronized UserStatus getByFacebookUserId(String facebookUserId) {
    return userStatusRepository.getByFacebookUserId(facebookUserId);
  }
}
