package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.DomainHourService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionTypeService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reaction;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.ReactionRepository;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
@Service
public class ReactionServiceImpl implements ReactionService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReactionServiceImpl.class);

  @Autowired
  private Mapper mapper;
  @Autowired
  private ReactionRepository reactionRepository;
  @Autowired
  private ReactionsService reactionsService;
  @Autowired
  private UserService userService;
  @Autowired
  private ReactionTypeService reactionTypeService;
  @Autowired
  private DomainHourService domainHourService;

  @Override
  @Transactional
  public synchronized void save(String owningEntityFacebookId,
                                ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.Reaction reaction) {
    Reactions reactionsDomain = reactionsService.findByReferenceId(owningEntityFacebookId);
    if (reactionsDomain == null) {
      LOGGER.error(
          "No reactionable entity with id: " + owningEntityFacebookId + " was found! The reaction won't be saved");
      return;
    }
    Reaction reactionDomain = mapper.map(reaction, Reaction.class);
    reactionDomain.setUser(findAndReturn(reactionDomain.getUser()));
    reactionDomain.setReactionType(reactionTypeService.save(reactionDomain.getReactionType()));
    reactionDomain.setCreationTime(DateTimeUtil.getDateTimeUTCNow());
    reactionDomain.setDomainHour(domainHourService.getByDate(reactionDomain.getCreationTime()));
    reactionRepository.save(reactionDomain);
    updateCommentsForReference(reactionsDomain, reactionDomain);
  }

  @Override
  public synchronized boolean checkExistence(String owningEntityFacebookId,
                                             ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.Reaction reaction) {
    return reactionRepository
        .checkExistence(owningEntityFacebookId, reaction.getUser().getId(), reaction.getReactionType().getName());
  }


  private void updateCommentsForReference(Reactions reactionsDomain, Reaction reactionDomain) {
    reactionsDomain.addObject(reactionDomain);
    reactionsService.save(reactionsDomain);
  }

  private User findAndReturn(User user) {
    User userByFacebookId = userService.findByFacebookId(user.getFacebookId());
    if (userByFacebookId != null) {
      return userByFacebookId;
    } else {
      return userService.saveUserReference(user);
    }
  }
}
