package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.UserFeedMetadataRepository;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@Service
public class UserFeedMetadataServiceImpl implements UserFeedMetadataService {

  @Autowired
  private UserFeedMetadataRepository userFeedMetadataRepository;

  @Override
  public synchronized void save(UserFeedMetadata userFeedMetadata) {
    userFeedMetadataRepository.save(userFeedMetadata);
  }

  @Override
  public synchronized UserFeedMetadata getFeedMetadataByFacebookUserId(String facebookUserId) {
    return userFeedMetadataRepository.getByFacebookUserId(facebookUserId);
  }

  @Override
  public synchronized ZonedDateTime getLatestFeedPostOfUser(String facebookUserId) {
    Instant instant = Instant.ofEpochSecond(userFeedMetadataRepository.getLatestFeedPostOfUser(facebookUserId));
    return ZonedDateTime.ofInstant(instant, ZoneOffset.UTC);
  }

  @Override
  public synchronized User getUserWithOldestCollectedFeed() {
    return userFeedMetadataRepository.getUserWithOldestCollectedFeed();
  }

  @Override
  public synchronized User getUserWithOldestCollectedFeed(ZonedDateTime dateTime) {
    long timestamp = dateTime.toEpochSecond();
    return userFeedMetadataRepository.getUserWithOldestCollectedFeed(timestamp);
  }

  @Override
  public synchronized Iterable<User> getUsersWithOldestCollectedFeed(Integer limit) {
    return userFeedMetadataRepository.getUsersWithOldestCollectedFeed(limit);
  }

  @Override
  public synchronized Collection<User> getUsersWithOldestCollectedFeed(Integer limit, ZonedDateTime dateTime) {
    long timestamp = dateTime.toEpochSecond();
    return userFeedMetadataRepository.getUsersWithOldestCollectedFeed(limit, timestamp);
  }
}
