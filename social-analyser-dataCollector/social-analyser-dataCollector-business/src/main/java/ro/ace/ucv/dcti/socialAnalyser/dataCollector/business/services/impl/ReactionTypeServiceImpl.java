package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionTypeService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.ReactionTypeRepository;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
@Service
public class ReactionTypeServiceImpl implements ReactionTypeService {

  @Autowired
  private ReactionTypeRepository reactionTypeRepository;

  @Override
  public ReactionType findByName(String name) {
    return reactionTypeRepository.findByName(name);
  }

  @Override
  public ReactionType save(ReactionType reactionType) {
    ReactionType reactionTypeAux = reactionTypeRepository.findByName(reactionType.getName());
    if(reactionTypeAux!=null) {
      return reactionTypeAux;
    }

    ReactionType savedReactionType = reactionTypeRepository.save(reactionType);
    return savedReactionType;
  }

  @Override
  public ReactionType save(String reactionType) {
    ReactionType reactionTypeAux = reactionTypeRepository.findByName(reactionType);
    if(reactionTypeAux!=null) {
      return reactionTypeAux;
    }

    ReactionType reactionType1 = new ReactionType();
    reactionType1.setName(reactionType);
    ReactionType savedReactionType = reactionTypeRepository.save(reactionType1);
    return savedReactionType;
  }
}
