package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserFeed;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface UserFeedService {

  void save(UserFeed userFeed);

  UserFeed findByFacebookUserId(String facebookUserId);

  Iterable<UserFeed> findAll();
}
