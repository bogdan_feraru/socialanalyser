package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.config.SpringDataNeo4jConfig;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
@Configuration
@ComponentScan(basePackages = {"ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services",
                               "ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.parser"})
@ImportResource("classpath:metadata/dozer-springConfig.xml")
@Import(SpringDataNeo4jConfig.class)
public class BusinessConfig {

}
