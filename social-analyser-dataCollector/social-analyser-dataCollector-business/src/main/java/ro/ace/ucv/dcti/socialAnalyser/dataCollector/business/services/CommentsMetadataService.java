package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface CommentsMetadataService {

  void save(CommentsMetadata commentsMetadata);

  CommentsMetadata findByOwningEntityId(String owningEntityId);

  Comments getCommentsByMetadata(CommentsMetadata metadata);

  Collection<String> getOldestVisitedPostComments(Long timestampOffset, Integer limit);

  Collection<String> getOldestVisitedPostComments(Integer limit);

  Collection<String> getOldestVisitedCommentComments(Long offset, Integer limit);
}
