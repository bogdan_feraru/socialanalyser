package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainHour;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
public interface DomainHourService {

  DomainHour getByExternalId(String externalId);

  DomainHour getByDate(ZonedDateTime timestamp);
}
