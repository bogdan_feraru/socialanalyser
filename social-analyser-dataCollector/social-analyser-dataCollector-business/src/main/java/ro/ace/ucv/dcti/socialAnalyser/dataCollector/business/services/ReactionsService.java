package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface ReactionsService {

  void save(Reactions reactions);

  Reactions findByReferenceId(String facebookReferenceId);

  Long countReactions(String facebookReferenceId);
}
