package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.FriendListMetadataRepository;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@Service
public class FriendListMetadataServiceImpl implements FriendListMetadataService {

  @Autowired
  private FriendListMetadataRepository friendListMetadataRepository;

  @Override
  public synchronized void save(FriendListMetadata friendListMetadata) {
    if (friendListMetadata.getId() != null) {
      update(friendListMetadata);
      return;
    }
    friendListMetadataRepository.save(friendListMetadata);
  }

  private synchronized void update(FriendListMetadata friendListMetadata) {
    FriendListMetadata existingCommentsMetadata = friendListMetadataRepository.findOne(friendListMetadata.getId());
    existingCommentsMetadata.setLastVisitedDate(friendListMetadata.getLastVisitedDate());
    friendListMetadataRepository.save(existingCommentsMetadata);
  }

  @Override
  public synchronized FriendListMetadata findByOwningEntityId(String userFacebookId) {
    return friendListMetadataRepository.findByOwningEntityId(userFacebookId);
  }

  @Override
  public synchronized List<String> getUsersWithNonCollectedFriendLists(Long upperTimeLimit) {
    Long facebookReleaseDate = FriendListMetadata.FACEBOOK_RELEASE_DATE.toEpochSecond();
    return friendListMetadataRepository.getUsersWithNonCollectedFriendLists(facebookReleaseDate);
  }
}
