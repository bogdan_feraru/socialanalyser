package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FriendList;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.FriendListRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class FriendListServiceImpl implements FriendListService {

  @Autowired
  private FriendListRepository friendListRepository;

  @Autowired
  private FriendListMetadataService friendListMetadataService;

  @Override
  public synchronized void save(FriendList friendList) {
    if (friendList.getId() != null) {
      friendListRepository.save(friendList);
      return;
    }

    friendListRepository.save(friendList);
    FriendListMetadata friendListMetadata = new FriendListMetadata();
    friendListMetadata.setReference(friendList);
    friendListMetadataService.save(friendListMetadata);
  }

  @Override
  public synchronized FriendList findByUserFacebookId(String facebookUserId) {
    return friendListRepository.findByUserFacebookId(facebookUserId);
  }
}
