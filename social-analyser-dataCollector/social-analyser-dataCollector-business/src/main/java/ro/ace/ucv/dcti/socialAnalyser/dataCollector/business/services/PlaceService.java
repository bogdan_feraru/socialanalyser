package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Place;

/**
 * Created by bogdan.feraru on 5/6/2017.
 */
public interface PlaceService {

  void save(org.springframework.social.facebook.api.Place place);

  void save(Place place);

  Place findByFacebookId(String facebookId);
}
