package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;

/**
 * Created by bogdan.feraru on 5/7/2017.
 */
@Component
public class HashtagParser {

  private static Logger LOGGER = LoggerFactory.getLogger(HashtagParser.class);
  private static final String HASHTAG_REGEX = "(?<=\\s|^)#(\\w*[A-Za-z_]+\\w*)";

  public Collection<Tag> parse(String input) {
    List<Tag> tagList = new ArrayList<>();
    if (input == null || input.isEmpty()) {
      return tagList;
    }
    Pattern p = Pattern.compile(HASHTAG_REGEX, Pattern.UNICODE_CHARACTER_CLASS);
    Matcher matcher = p.matcher(input);

    while (matcher.find()) {
      String hashtagText = input.substring(matcher.start() + 1, matcher.end());
      Tag tag = new Tag();
      tag.setName(hashtagText);
      tagList.add(tag);
    }

    return tagList;
  }
}
