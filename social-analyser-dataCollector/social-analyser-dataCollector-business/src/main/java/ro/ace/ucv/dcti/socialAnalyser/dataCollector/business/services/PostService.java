package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import org.springframework.social.facebook.api.Post;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface PostService {

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post save(Post post);

  ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post findByFacebookId(String facebookId);
}
