package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;

/**
 * Created by bogdan.feraru on 5/7/2017.
 */
public interface TagService {

  void save(Tag tag);

  Tag find(String name);
}
