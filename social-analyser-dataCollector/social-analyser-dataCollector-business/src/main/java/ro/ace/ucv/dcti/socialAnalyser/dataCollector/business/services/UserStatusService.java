package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserStatus;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
public interface UserStatusService {

  void save(UserStatus userStatus);

  UserStatus getByFacebookUserId(String facebookUserId);
}
