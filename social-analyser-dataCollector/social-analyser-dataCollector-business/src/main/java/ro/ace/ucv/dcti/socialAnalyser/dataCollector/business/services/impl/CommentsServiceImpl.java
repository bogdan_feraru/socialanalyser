package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FacebookDomainObject;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.CommentsRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class CommentsServiceImpl implements CommentsService {

  @Autowired
  private CommentsRepository commentsRepository;

  @Autowired
  private CommentsMetadataService commentsMetadataService;

  @Override
  public synchronized void save(Comments comments) {
    if (comments.getId() != null) {
      commentsRepository.save(comments);
      return;
    }

    commentsRepository.save(comments);
    CommentsMetadata commentsMetadata = new CommentsMetadata();
    commentsMetadata.setReference(comments);
    commentsMetadataService.save(commentsMetadata);
  }

  @Override
  public synchronized Comments findByReferenceId(String facebookReferenceId) {
    return commentsRepository.findByReferenceId(facebookReferenceId);
  }

  @Override
  public synchronized Long countComments(Comments comments) {
    Comments commentsRefreshed = commentsRepository.findOne(comments.getId());
    FacebookDomainObject reference = commentsRefreshed.getReference();

    return commentsRepository.countComments(reference.getFacebookId());
  }

  @Override
  public synchronized Long countComments(String facebookId) {
    return commentsRepository.countComments(facebookId);
  }
}
