package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import org.springframework.social.facebook.api.Comment;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
public interface CommentService {

  void save(String postFacebookId, Comment comment);
}
