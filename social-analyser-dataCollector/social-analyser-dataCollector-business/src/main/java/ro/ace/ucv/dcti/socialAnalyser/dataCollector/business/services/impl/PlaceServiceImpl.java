package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.PlaceService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Place;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.PlaceRepository;

/**
 * Created by bogdan.feraru on 5/6/2017.
 */
@Service
public class PlaceServiceImpl implements PlaceService {

  private static final Logger LOGGER = LoggerFactory.getLogger(PlaceServiceImpl.class);

  @Autowired
  private PlaceRepository placeRepository;
  @Autowired
  private Mapper mapper;

  @Override
  public synchronized void save(org.springframework.social.facebook.api.Place place) {
    Place placeDomain = placeRepository.findByFacebookId(place.getId());
    if (placeDomain != null) {
      LOGGER.warn("Place with id: " + place.getId() + " aleready exists.");
      return;
    }
    placeDomain = mapper.map(place, Place.class);
    placeRepository.save(placeDomain);
  }

  @Override
  public synchronized void save(Place place) {
    if (place == null) {
      return;
    }
    Place placeDomain = placeRepository.findByFacebookId(place.getFacebookId());
    if (placeDomain != null) {
      place.setId(placeDomain.getId());
      LOGGER.warn("Place with id: " + place.getFacebookId() + " aleready exists.");
      return;
    }
    placeRepository.save(place);
  }

  @Override
  public synchronized ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Place findByFacebookId(
      String facebookId) {
    return placeRepository.findByFacebookId(facebookId);
  }
}
