package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.dozer.converter;

import org.dozer.DozerConverter;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
public class DateToZonedDateTimeConverter extends DozerConverter<Date, ZonedDateTime> {

  public DateToZonedDateTimeConverter() {
    super(Date.class, ZonedDateTime.class);
  }

  @Override
  public ZonedDateTime convertTo(Date source, ZonedDateTime destination) {
    return ZonedDateTime.ofInstant(source.toInstant(), ZoneOffset.UTC);
  }

  @Override
  public Date convertFrom(ZonedDateTime source, Date destination) {
    return Date.from(source.toInstant());
  }
}
