package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface CommentsService {

  void save(Comments comments);

  Comments findByReferenceId(String facebookReferenceId);

  Long countComments(Comments comments);

  Long countComments(String facebookId);
}
