package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.strategy;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by bogdan.feraru on 6/17/2017.
 */
@Component
public class UserDuplicationCheckingStrategyImpl implements UserDuplicationCheckingStrategy {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserDuplicationCheckingStrategyImpl.class);

  @Override
  public boolean isSame(String facebookUserId1, String facebookUserId2) {
    try {
      String profilePhotoUrl1 = getProfilePhotoUrl(facebookUserId1);
      String profilePhotoUrl2 = getProfilePhotoUrl(facebookUserId2);
      return profilePhotoUrl1.equals(profilePhotoUrl2);
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
    return false;
  }

  private String getProfilePhotoUrl(String facebookUserId) throws IOException {
    CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
    String profilePhotoUrl = "";
    try {
      HttpClientContext context = HttpClientContext.create();
      HttpGet httpGet = new HttpGet("https://graph.facebook.com/v2.9/" + facebookUserId + "/picture");
      System.out.println("Executing request " + httpGet.getRequestLine());
      System.out.println("----------------------------------------");

      httpClient.execute(httpGet, context);
      HttpHost target = context.getTargetHost();
      List<URI> redirectLocations = context.getRedirectLocations();
      URI location = URIUtils.resolve(httpGet.getURI(), target, redirectLocations);
      profilePhotoUrl = location.toASCIIString();

    } catch (URISyntaxException e) {
      LOGGER.error(e.getMessage());
    } finally {
      httpClient.close();
    }

    return profilePhotoUrl;
  }
}
