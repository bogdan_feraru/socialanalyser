package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.DomainHourService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comment;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.CommentRepository;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
@Service
public class CommentServiceImpl implements CommentService {
  private static final Logger LOGGER = LoggerFactory.getLogger(CommentServiceImpl.class);

  @Autowired
  private CommentRepository commentRepository;
  @Autowired
  private CommentsService commentsService;
  @Autowired
  private ReactionsService reactionsService;
  @Autowired
  private UserService userService;
  @Autowired
  private Mapper mapper;
  @Autowired
  private DomainHourService domainHourService;

  @Override
  @Transactional
  public synchronized void save(String referenceFacebookId, org.springframework.social.facebook.api.Comment comment) {
    Comments commentsDomain = commentsService.findByReferenceId(referenceFacebookId);
    if (commentsDomain == null) {
      LOGGER.error("No commentable entity with id: " + referenceFacebookId + " was found! The comment won't be saved");
      return;
    }

    if (commentRepository.findByFacebookId(comment.getId()) != null) {
      LOGGER.warn("Comment with id" + comment.getId() + " already exists! The comment won't be saved");
      return;
    }

    Comment commentDomain = mapper.map(comment, Comment.class);
    commentDomain.setUser(findAndReturn(commentDomain.getUser()));
    commentDomain.setDomainHour(domainHourService.getByDate(commentDomain.getCreationTime()));
    commentRepository.save(commentDomain);
    saveAuxiliaryProperties(commentDomain);
    updateCommentsForReference(commentsDomain, commentDomain);
  }

  private void updateCommentsForReference(Comments commentsDomain, Comment commentDomain) {
    commentsDomain.addObject(commentDomain);
    commentsService.save(commentsDomain);
  }

  private User findAndReturn(User user) {
    User userByFacebookId = userService.findByFacebookId(user.getFacebookId());
    if (userByFacebookId != null) {
      return userByFacebookId;
    } else {
      return userService.saveUserReference(user);
    }
  }

  private void saveAuxiliaryProperties(Comment commentDomain) {
    createReactionsForComments(commentDomain);
    createCommentsForComments(commentDomain);
    commentRepository.save(commentDomain);
  }

  private void createCommentsForComments(Comment commentDomain) {
    Comments comments = new Comments();
    comments.setReference(commentDomain);
    commentsService.save(comments);
    commentDomain.setComments(comments);
  }

  private void createReactionsForComments(Comment commentDomain) {
    Reactions reactions = new Reactions();
    reactions.setReference(commentDomain);
    reactionsService.save(reactions);
    commentDomain.setReactions(reactions);
  }
}
