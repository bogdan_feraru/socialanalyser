package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.DomainHourService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainHour;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.DomainHourRepository;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
@Service
public class DomainHourServiceImpl implements DomainHourService {

  @Autowired
  private DomainHourRepository domainHourRepository;

  @Override
  public DomainHour getByExternalId(String externalId) {
    return domainHourRepository.findByExternalId(externalId);
  }

  @Override
  public DomainHour getByDate(ZonedDateTime timestamp) {
    int month = timestamp.getMonthValue();
    int hour = timestamp.getHour();
    int dayOfMonth = timestamp.getDayOfMonth();
    String externalId =
        "" + timestamp.getYear() + (month < 10 ? "0" : "") + month + (dayOfMonth < 10 ? "0" : "") + dayOfMonth + (
            hour < 10 ? "0" : "") + hour;
    return domainHourRepository.findByExternalId(externalId);
  }
}
