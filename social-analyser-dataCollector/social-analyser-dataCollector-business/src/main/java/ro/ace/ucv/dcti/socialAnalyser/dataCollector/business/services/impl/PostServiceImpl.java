package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.parser.HashtagParser;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.*;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainHour;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.PostRepository;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@Service
public class PostServiceImpl implements PostService {

  private static final Logger LOGGER = LoggerFactory.getLogger(PostServiceImpl.class);

  @Autowired
  private Mapper mapper;
  @Autowired
  private PostRepository postRepository;
  @Autowired
  private CommentsService commentsService;
  @Autowired
  private ReactionsService reactionsService;
  @Autowired
  private UserService userService;
  @Autowired
  private PlaceService placeService;
  @Autowired
  private TagService tagService;
  @Autowired
  private HashtagParser hashtagParser;
  @Autowired
  private DomainHourService domainHourService;

  @Override
  public synchronized Post save(org.springframework.social.facebook.api.Post post) {
    Post postByFacebookId = postRepository.findByFacebookId(post.getId());
    if (postByFacebookId != null) {
      LOGGER.info("Post with facebook id " + post.getId() + " already exists.");
      return postByFacebookId;
    }

    LOGGER.info("Post with facebook id " + post.getId() + " is saving.");
    Post postDomain = savePost(post);
    saveAuxiliaryProperties(postDomain);

    return postDomain;
  }

  @Override
  public synchronized Post findByFacebookId(String facebookId) {
    return postRepository.findByFacebookId(facebookId);
  }

  private Post savePost(org.springframework.social.facebook.api.Post post) {
    Post postDomain = mapper.map(post, Post.class);
    User creator = findAndReturn(postDomain.getUser());
    if (creator == null) {
      LOGGER.warn("Creator of post with facebookId " + post.getId() + " is null!");
    }
    DomainHour hour = domainHourService.getByDate(postDomain.getCreationTime());
    postDomain.setDomainHour(hour);
    postDomain.setUser(creator);
    postDomain.setWithTags(findAndReturn(postDomain.getWithTags()));
    postDomain.setTargetedUsers(findAndReturn(postDomain.getWithTags()));
    placeService.save(postDomain.getLocation());

    Collection<Tag> tags = hashtagParser.parse(postDomain.getMessage());
    tags.forEach(tag -> {
      tagService.save(tag);
      Tag tagDomain = tagService.find(tag.getName());
      postDomain.addTag(tagDomain);
    });

    postRepository.save(postDomain);
    return postDomain;
  }

  private Set<User> findAndReturn(Set<User> users) {
    Set<User> userReferences = new HashSet<>();
    users.forEach(user -> userReferences.add(findAndReturn(user)));

    return userReferences;
  }

  private User findAndReturn(User user) {
    if (user == null) {
      return null;
    }
    User userByFacebookId = userService.findByFacebookId(user.getFacebookId());
    if (userByFacebookId != null) {
      return userByFacebookId;
    } else {
      return userService.saveUserReference(user);
    }
  }

  private void saveAuxiliaryProperties(Post postDomain) {
    createReactionsForPost(postDomain);
    createCommentsForPost(postDomain);
    postRepository.save(postDomain);
  }

  private void createCommentsForPost(Post postDomain) {
    Comments comments = new Comments();
    comments.setReference(postDomain);
    commentsService.save(comments);
    postDomain.setComments(comments);
  }

  private void createReactionsForPost(Post postDomain) {
    Reactions reactions = new Reactions();
    reactions.setReference(postDomain);
    reactionsService.save(reactions);
    postDomain.setReactions(reactions);
  }
}
