package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.Reference;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.PostService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserStatusService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.strategy.UserDuplicationCheckingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FriendList;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserFeed;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserStatus;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserStatusType;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

  @Autowired
  private Mapper mapper;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PostService postService;
  @Autowired
  private UserFeedService userFeedService;
  @Autowired
  private FriendListService friendListService;
  @Autowired
  private UserStatusService userStatusService;
  @Autowired
  private UserDuplicationCheckingStrategy userDuplicationCheckingStrategy;

  @Override
  public synchronized User saveUser(org.springframework.social.facebook.api.User user) {
    User userById = userRepository.findByFacebookId(user.getId());
    if (userById != null) {
      UserStatus userStatus = userStatusService.getByFacebookUserId(userById.getFacebookId());
      if (userStatus.getUserStatusType().equals(UserStatusType.USER_REFERENCE)) {
        convertToApplicationUserAndSave(user, userById);
        return userById;
      }
      LOGGER.warn("User with facebook id " + user.getId() + " already exists.");
      return userById;
    }

    User userByName = userRepository.findByName(user.getName());
    if (userByName != null) {
      if (userDuplicationCheckingStrategy.isSame(userByName.getFacebookId(), user.getId())) {
        UserStatus userStatus = userStatusService.getByFacebookUserId(userByName.getFacebookId());
        if (userStatus.getUserStatusType().equals(UserStatusType.USER_REFERENCE)) {
          convertToApplicationUserAndSave(user, userByName);
          return userById;
        }
        LOGGER.warn("User with facebook id " + user.getId() + " already exists.");
        return userById;
      }
    }

    return saveNonExistingUser(user);
  }

  @Override
  public synchronized User saveUserReference(Reference userReference) {
    User userDomain = mapper.map(userReference, User.class);
    return saveUserReference(userDomain);
  }

  @Override
  public synchronized User saveUserReference(org.springframework.social.facebook.api.User user) {
    User userDomain = mapper.map(user, User.class);
    return saveUserReference(userDomain);
  }

  @Override
  public synchronized User saveUserReference(User user) {
    User userById = userRepository.findByFacebookId(user.getFacebookId());
    if (userById != null) {
      LOGGER.warn("User with facebook id " + user.getId() + " already exists.");
      return userById;
    }

    User userByName = userRepository.findByName(user.getFacebookId());
    if (userByName != null) {
      if (userDuplicationCheckingStrategy.isSame(userByName.getFacebookId(), user.getFacebookId())) {
        LOGGER.warn("User with facebook id " + user.getId() + " already exists.");
        return userByName;
      }
    }

    User userDomain = userRepository.save(user);

    createAuxiliaryPropertiesForUserReference(userDomain);
    return userDomain;
  }

  @Override
  public synchronized void addPostToUserFeed(String facebookUserId, Post post) {
    User user = userRepository.findByFacebookId(facebookUserId);
    if (user == null) {
      LOGGER.error("User with facebook id: " + facebookUserId + " should exist.");
      return;
    }

    UserStatus userStatus = userStatusService.getByFacebookUserId(facebookUserId);
    if (userStatus.getUserStatusType().equals(UserStatusType.USER_REFERENCE)) {
      LOGGER.error("User with facebook id: " + facebookUserId
                   + " is just a user reference. It should be an application user to perform this operation");
      return;
    }

    ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post postDomainObject = postService.save(post);
    UserFeed userFeed = userFeedService.findByFacebookUserId(user.getFacebookId());
    userFeed.addObject(postDomainObject);
    userFeedService.save(userFeed);
  }

  @Override
  public synchronized User findByFacebookId(String facebookId) {
    return userRepository.findByFacebookId(facebookId);
  }

  @Override
  public synchronized boolean isApplicationUser(String facebookId) {
    User user = userRepository.findByFacebookId(facebookId);
    if (user == null) {
      return false;
    }

    UserStatus userStatus = userStatusService.getByFacebookUserId(facebookId);
    if (userStatus.getUserStatusType().equals(UserStatusType.USER_REFERENCE)) {
      return false;
    }

    return true;
  }

  @Override
  public synchronized void addFriend(String userFacebookId, org.springframework.social.facebook.api.User friend) {
    if (userRepository.areFriends(userFacebookId, friend.getId())) {
      return;
    }

    User friendDomainObject = saveUserReference(friend);

    User userDomainObject = findByFacebookId(userFacebookId);
    FriendList friendListUser = friendListService.findByUserFacebookId(userDomainObject.getFacebookId());
    friendListUser.addObject(friendDomainObject);
    friendListService.save(friendListUser);

    if (isApplicationUser(friend.getId())) {
      FriendList friendListFriend = friendListService.findByUserFacebookId(friendDomainObject.getFacebookId());
      friendListFriend.addObject(userDomainObject);
      friendListService.save(friendListFriend);
    }
  }

  @Override
  public synchronized String getOwnerIdOfPost(String postFacebookId) {
    return userRepository.getOwnerIdOfPost(postFacebookId);
  }

  @Override
  public synchronized String getOwnerIdOfComment(String commentFacebookId) {
    return userRepository.getOwnerIdOfComment(commentFacebookId);
  }

  @Override
  public synchronized String getOwnerIdOfCommentComment(String commentFacebookId) {
    return userRepository.getOwnerIdOfCommentComment(commentFacebookId);
  }

  private void convertToApplicationUserAndSave(org.springframework.social.facebook.api.User user, User userDomain) {
    mapper.map(user, userDomain, "");
    userRepository.save(userDomain);
    createAuxiliaryPropertiesForApplicationUserConversion(userDomain);
  }

  private User saveNonExistingUser(org.springframework.social.facebook.api.User user) {
    User userDomain = mapper.map(user, User.class);
    userRepository.save(userDomain);
    createAuxiliaryPropertiesForApplicationUser(userDomain);
    return userDomain;
  }

  private void createAuxiliaryPropertiesForUserReference(User userDomain) {
    createUserStatusForUserReference(userDomain);
    userRepository.save(userDomain);
  }

  private void createAuxiliaryPropertiesForApplicationUser(User userDomain) {
    createUserFeedForUser(userDomain);
    createFriendListForUser(userDomain);
    createUserStatusForApplicationUser(userDomain);
    userRepository.save(userDomain);
  }

  private void createAuxiliaryPropertiesForApplicationUserConversion(User userDomain) {
    createUserFeedForUser(userDomain);
    createFriendListForUser(userDomain);
    changeUserStatusToApplicationUser(userDomain);
    userRepository.save(userDomain);
  }

  private void createFriendListForUser(User userDomain) {
    FriendList friendList = new FriendList();
    friendList.setReference(userDomain);
    friendListService.save(friendList);
    userDomain.setFriendList(friendList);
  }

  private void createUserFeedForUser(User userDomain) {
    UserFeed userFeed = new UserFeed();
    userFeed.setReference(userDomain);
    userFeedService.save(userFeed);
    userDomain.setUserFeed(userFeed);
  }

  private void createUserStatusForUserReference(User userDomain) {
    UserStatus userStatus = new UserStatus();
    userStatus.setReference(userDomain);
    userStatus.setUserStatusType(UserStatusType.USER_REFERENCE);
    userStatusService.save(userStatus);
    userDomain.setUserStatus(userStatus);
  }

  private void createUserStatusForApplicationUser(User userDomain) {
    UserStatus userStatus = new UserStatus();
    userStatus.setReference(userDomain);
    userStatus.setUserStatusType(UserStatusType.APPLICATION_USER);
    userStatusService.save(userStatus);
    userDomain.setUserStatus(userStatus);
  }

  private void changeUserStatusToApplicationUser(User userDomain) {
    UserStatus userStatus = userStatusService.getByFacebookUserId(userDomain.getFacebookId());
    userStatus.setUserStatusType(UserStatusType.APPLICATION_USER);
    userStatusService.save(userStatus);
    userDomain.setUserStatus(userStatus);
  }
}
