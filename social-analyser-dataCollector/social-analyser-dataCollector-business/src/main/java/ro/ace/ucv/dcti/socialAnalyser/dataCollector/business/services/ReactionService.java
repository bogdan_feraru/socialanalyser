package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services;


import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.Reaction;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public interface ReactionService {

  void save(String owningEntityFacebookId, Reaction reaction);

  boolean checkExistence(String owningEntityFacebookId, Reaction reaction);
}
