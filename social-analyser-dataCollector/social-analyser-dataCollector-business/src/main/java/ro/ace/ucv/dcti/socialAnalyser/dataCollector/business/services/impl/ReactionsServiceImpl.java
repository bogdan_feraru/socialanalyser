package ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository.ReactionsRepository;

/**
 * Created by bogdan.feraru on 4/11/2017.
 */
@Service
public class ReactionsServiceImpl implements ReactionsService {

  @Autowired
  private ReactionsRepository reactionsRepository;

  @Autowired
  private ReactionsMetadataService reactionsMetadataService;

  @Override
  public synchronized void save(Reactions reactions) {
    if (reactions.getId() != null) {
      reactionsRepository.save(reactions);
      return;
    }

    reactionsRepository.save(reactions);
    ReactionsMetadata reactionsMetadata = new ReactionsMetadata();
    reactionsMetadata.setReference(reactions);
    reactionsMetadataService.save(reactionsMetadata);
  }

  @Override
  public synchronized Reactions findByReferenceId(String facebookReferenceId) {
    return reactionsRepository.findByReferenceId(facebookReferenceId);
  }

  @Override
  public synchronized Long countReactions(String facebookReferenceId) {
    return reactionsRepository.countReactions(facebookReferenceId);
  }
}
