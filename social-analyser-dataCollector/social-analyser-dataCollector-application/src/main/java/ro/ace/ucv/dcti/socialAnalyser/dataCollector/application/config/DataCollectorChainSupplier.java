package ro.ace.ucv.dcti.socialanalyser.datacollector.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl.*;

/**
 * Created by bogdan.feraru on 5/27/2017.
 */
@Component
public class DataCollectorChainSupplier implements Supplier<DataCollector> {

  @Autowired
  private ApplicationContext applicationContext;

  @Override
  public DataCollector get() {
    PostsCommentsCommentsReactionsDataCollector postsCommentsCommentsReactionsDataCollector =
        (PostsCommentsCommentsReactionsDataCollector) applicationContext
            .getBean("postsCommentsCommentsReactionsDataCollector", (Object) null);
    PostsCommentsReactionsDataCollector postsCommentsReactionsDataCollector =
        (PostsCommentsReactionsDataCollector) applicationContext
            .getBean("postsCommentsReactionsDataCollector", postsCommentsCommentsReactionsDataCollector);
    PostsReactionsDataCollector postsReactionsDataCollector =
        (PostsReactionsDataCollector) applicationContext
            .getBean("postsReactionsDataCollector", postsCommentsReactionsDataCollector);
    PostsCommentsCommentsDataCollector postsCommentsCommentsDataCollector =
        (PostsCommentsCommentsDataCollector) applicationContext
            .getBean("postsCommentsCommentsDataCollector", postsReactionsDataCollector);
    PostsCommentsDataCollector postsCommentsDataCollector =
        (PostsCommentsDataCollector) applicationContext
            .getBean("postsCommentsDataCollector", postsCommentsCommentsDataCollector);
    FeedsDataCollector feedsDataCollector =
        (FeedsDataCollector) applicationContext.getBean("feedsDataCollector", postsCommentsDataCollector);
    FriendListsDataCollector friendListsDataCollector =
        (FriendListsDataCollector) applicationContext.getBean("friendListsDataCollector", feedsDataCollector);

    return (UsersDataCollector) applicationContext.getBean("usersDataCollector", friendListsDataCollector);
  }
}
