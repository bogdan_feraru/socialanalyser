package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;
import java.util.Collection;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.POST_REACTIONS_CHECKING_INTERVAL_DAYS;

/**
 * Created by bogdan.feraru on 5/7/2017.
 */
@Component("postReactionsPickingStrategy")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class PostReactionsPickingStrategy implements PickingStrategy<String> {

  @Autowired
  private ReactionsMetadataService reactionsMetadataService;
  @Autowired
  private Environment environment;

  @Override
  public String pickNext() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Collection<String> pickNextBatch(int limit) {
    Long offset = DateTimeUtil.getDateTimeUTCNow().
        minus(Integer.valueOf(environment.getProperty(POST_REACTIONS_CHECKING_INTERVAL_DAYS)), ChronoUnit.DAYS).toEpochSecond();
    return reactionsMetadataService.getOldestVisitedPostReactions(offset, limit);
  }

  @Override
  public void reset() {
    throw new UnsupportedOperationException();
  }
}
