package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.facebook.api.Comment;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.CommentOperationsExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagedListExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
public class CommentsCollectingTask extends CollectingTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommentsCollectingTask.class);

  private final String commentsOwnerFacebookId;

  private PagingParameterProvider<CommentsMetadata> pagingParameterProvider;

  private CommentsMetadataService commentsMetadataService;

  private CommentService commentService;

  private CommentsService commentsService;

  public CommentsCollectingTask(String facebookUserId, String commentsOwnerFacebookId) {
    super(facebookUserId);
    this.commentsOwnerFacebookId = commentsOwnerFacebookId;
  }

  @Override
  public Long collect() {
    CommentsMetadata commentsMetadata = commentsMetadataService.findByOwningEntityId(commentsOwnerFacebookId);
    PagingParametersExtension pagingParameters = pagingParameterProvider.genNextPageParameters(commentsMetadata);
    CommentOperationsExtension commentOperationsExtension = new CommentOperationsExtension(getFacebook());
    Long collectedEntities = 0L;
    while (true) {
      try {
        PagedListExtension<Comment> orderedComments =
            commentOperationsExtension.getOrderedComments(commentsOwnerFacebookId, pagingParameters);
        Long commentsCount = commentsService.countComments(commentsOwnerFacebookId);
        if (orderedComments.isEmpty() && commentsCount < orderedComments.getTotalCount()) {
          collectedEntities += orderedComments.size();
          processPage(orderedComments, commentsMetadata);
          pagingParameters = orderedComments.getNextPage();
        } else {
          commentsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
          commentsMetadataService.save(commentsMetadata);
          return collectedEntities;
        }
      } catch (ResourceNotFoundException | RevokedAuthorizationException ex) {
        commentsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
        commentsMetadataService.save(commentsMetadata);
        throw ex;
      }
    }
  }

  @Override
  protected String getEntityId() {
    return commentsOwnerFacebookId;
  }

  private void processPage(PagedListExtension<Comment> orderedComments, CommentsMetadata commentsMetadata) {
    List<Comment> unsavedComments = getUnsaved(orderedComments, commentsMetadata);
    for (Comment comment : unsavedComments) {
      ZonedDateTime creationTime = getDateToZonedDateTimeConverter().convertTo(comment.getCreatedTime(), null);
      ForkJoinTask<?> saveCommentTask =
          ForkJoinPool.commonPool().submit(() -> commentService.save(commentsOwnerFacebookId, comment));
      try {
        saveCommentTask.get(10, TimeUnit.SECONDS);
        commentsMetadata.setLastExtractedCreationDate(creationTime);
        commentsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
        commentsMetadataService.save(commentsMetadata);
      } catch (InterruptedException | ExecutionException e) {
        LOGGER.error("", e);
      } catch (TimeoutException e) {
        LOGGER.error("No response in 10 seconds. The comment was not saved.", e);
        saveCommentTask.cancel(true);
      }
    }
    if (!unsavedComments.isEmpty()) {
      LOGGER.info("Finished collecting a list of: {} comments.",unsavedComments.size() );
    }
  }

  private List<Comment> getUnsaved(PagedListExtension<Comment> orderedComments, CommentsMetadata commentsMetadata) {
    int i;
    for (i = 0; i < orderedComments.size(); i++) {
      Comment comment = orderedComments.get(i);
      ZonedDateTime creationTime = getDateToZonedDateTimeConverter().convertTo(comment.getCreatedTime(), null);
      if (creationTime.isAfter(commentsMetadata.getLastExtractedCreationDate())) {
        break;
      }
    }
    if (i != 0) {
      LOGGER.info("Skipped {} comments.", i);
    }

    return orderedComments.subList(i, orderedComments.size());
  }

  public void setPagingParameterProvider(
      PagingParameterProvider<CommentsMetadata> pagingParameterProvider) {
    this.pagingParameterProvider = pagingParameterProvider;
  }

  public void setCommentsMetadataService(
      CommentsMetadataService commentsMetadataService) {
    this.commentsMetadataService = commentsMetadataService;
  }

  public void setCommentService(CommentService commentService) {
    this.commentService = commentService;
  }

  public void setCommentsService(CommentsService commentsService) {
    this.commentsService = commentsService;
  }
}
