package ro.ace.ucv.dcti.socialanalyser.datacollector.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.CollectingStatus;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.SocialAnalyserContext;

/**
 * Created by bogdan.feraru on 5/27/2017.
 */
@Component
public class CollectingTrigger implements Trigger {

  @Autowired
  private Environment environment;

  @Override
  public synchronized Date nextExecutionTime(TriggerContext triggerContext) {
    if (SocialAnalyserContext.getCollectingStatus() == CollectingStatus.NOT_STARTED) {
      SocialAnalyserContext.setCollectingStatus(CollectingStatus.SCHEDULED);
      return new Date();
    } else if (SocialAnalyserContext.getCollectingStatus() == CollectingStatus.SUCCESSFUL) {
      SocialAnalyserContext.setCollectingStatus(CollectingStatus.SCHEDULED);
      return Date.from(Instant.now().plus(1, ChronoUnit.DAYS));
    } else if (SocialAnalyserContext.getCollectingStatus() == CollectingStatus.LIMIT_EXCEEDED
               || SocialAnalyserContext.getCollectingStatus() == CollectingStatus.STARTED) {
      Instant lastScheduledExecutionTime = triggerContext.lastScheduledExecutionTime().toInstant();
      if (lastScheduledExecutionTime.plus(2, ChronoUnit.HOURS).isAfter(Instant.now())) {
        return Date.from(Instant.now().plus(2, ChronoUnit.HOURS));
      }
      return Date.from(Instant.now());
    }
    return null;
  }
}
