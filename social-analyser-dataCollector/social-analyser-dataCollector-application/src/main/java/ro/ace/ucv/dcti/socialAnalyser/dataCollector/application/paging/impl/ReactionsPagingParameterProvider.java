package ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.Order;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.DEFAULT_REACTIONS_NUMBER;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
@Component
public class ReactionsPagingParameterProvider implements PagingParameterProvider<ReactionsMetadata> {

  @Autowired
  private Environment environment;

  @Override
  public PagingParametersExtension genNextPageParameters(ReactionsMetadata metadata) {
    Long defaultCommentsNumber = Long.valueOf(environment.getProperty(DEFAULT_REACTIONS_NUMBER));

    return new PagingParametersExtension(defaultCommentsNumber.intValue(), null, null, null, Order.CHRONOLOGICAL);
  }
}
