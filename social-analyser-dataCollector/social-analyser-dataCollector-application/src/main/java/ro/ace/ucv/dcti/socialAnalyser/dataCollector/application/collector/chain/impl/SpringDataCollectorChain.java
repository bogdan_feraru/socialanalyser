package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.core.task.AsyncTaskExecutor;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollectorChain;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.factory.CollectionTasksFactory;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResult;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.handler.ResultHandler;

/**
 * Created by bogdan.feraru on 5/26/2017.
 */
abstract class SpringDataCollectorChain extends DataCollectorChain {

  @Autowired
  @Qualifier("collectorThreadPool")
  AsyncTaskExecutor collectorThreadPool;
  @Autowired
  CollectionTasksFactory collectionTasksFactory;
  @Autowired
  Environment environment;
  @Autowired
  ResultHandler<CollectingResult> collectingResultHandler;

  public SpringDataCollectorChain(DataCollector next) {
    super(next);
  }

  @Override
  protected AsyncTaskExecutor getCollectorThreadPool() {
    return collectorThreadPool;
  }

  @Override
  protected int getBatchSize() {
    return Integer.valueOf(environment.getProperty(getPropertyBatchSizeName()));
  }

  @Override
  protected ResultHandler<CollectingResult> getResultHandler() {
    return collectingResultHandler;
  }

  abstract String getPropertyBatchSizeName();
}
