package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface DataCollector {

  void collect();
}
