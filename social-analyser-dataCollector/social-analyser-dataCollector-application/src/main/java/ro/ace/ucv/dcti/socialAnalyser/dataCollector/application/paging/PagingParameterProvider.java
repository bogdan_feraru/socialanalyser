package ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.AbstractReferenceMetadata;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
public interface PagingParameterProvider<T extends AbstractReferenceMetadata> {

  PagingParametersExtension genNextPageParameters(T metadata);
}
