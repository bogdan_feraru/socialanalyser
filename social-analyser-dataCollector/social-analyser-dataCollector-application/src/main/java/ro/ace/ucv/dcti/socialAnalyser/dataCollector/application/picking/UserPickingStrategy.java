package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user.SimpleUser;

public interface UserPickingStrategy {

  SimpleUser pickNextUser();
}
