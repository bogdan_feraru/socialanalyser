package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.AsyncTaskExecutor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResult;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.handler.ResultHandler;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.CollectingStatus;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.SocialAnalyserContext;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public abstract class DataCollectorChain implements DataCollector {

  protected static final Logger LOGGER = LoggerFactory.getLogger(DataCollectorChain.class);

  protected DataCollector next;

  public DataCollectorChain(DataCollector next) {
    this.next = next;
  }

  @Override
  public void collect() {
    collectData();
    if (next != null) {
      next.collect();
    }
  }

  protected void collectData() {
    List<Future<CollectingResult>> collectingResults = new ArrayList<>();
    while (SocialAnalyserContext.getCollectingStatus() != CollectingStatus.LIMIT_EXCEEDED) {
      Collection<String> unsavedEntityBatch = getPickingStrategy().pickNextBatch(getBatchSize());
      if (unsavedEntityBatch.isEmpty()) {
        break;
      }

      unsavedEntityBatch.forEach(facebookUserId -> {
        CollectingTask collectingTask = constructCollectingTask(facebookUserId);
        Future<CollectingResult> collectingResult = getCollectorThreadPool().submit(collectingTask);
        collectingResults.add(collectingResult);
      });

      collectingResults.forEach(futureCollectingResult -> {
        try {
          CollectingResult collectingResult = futureCollectingResult.get();
          getResultHandler().handle(collectingResult);
        } catch (InterruptedException | ExecutionException e) {
          LOGGER.error("InterruptedException | ExecutionException occured", e);
        }
      });
      collectingResults.clear();
    }
  }

  protected abstract int getBatchSize();

  protected abstract PickingStrategy<String> getPickingStrategy();

  protected abstract ResultHandler<CollectingResult> getResultHandler();

  protected abstract AsyncTaskExecutor getCollectorThreadPool();

  protected abstract CollectingTask constructCollectingTask(String entityId);
}
