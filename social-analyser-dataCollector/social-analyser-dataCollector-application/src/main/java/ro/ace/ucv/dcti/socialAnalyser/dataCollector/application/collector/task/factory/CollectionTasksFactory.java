package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific.CommentsCollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific.FriendListCollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific.ReactionsCollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific.UserDetailsCollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific.UserFeedCollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.dozer.converter.DateToZonedDateTimeConverter;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.*;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.connection.FacebookConnectionRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.AbstractTimePaginatedReferenceMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Component
public class CollectionTasksFactory {

  @Autowired
  private Environment environment;
  @Autowired
  private FacebookConnectionRepository facebookConnectionRepository;

  @Autowired
  private UserService userService;
  @Autowired
  private UserFeedMetadataService userFeedMetadataService;
  @Autowired
  private PostService postService;
  @Autowired
  private CommentService commentService;
  @Autowired
  private CommentsService commentsService;
  @Autowired
  private DateToZonedDateTimeConverter dateToZonedDateTimeConverter;
  @Autowired
  private PagingParameterProvider<AbstractTimePaginatedReferenceMetadata<?>> pagingParameterProvider;
  @Autowired
  private PagingParameterProvider<CommentsMetadata> commentsPagingParameterProvider;
  @Autowired
  private CommentsMetadataService commentsMetadataService;
  @Autowired
  private ReactionService reactionService;
  @Autowired
  private ReactionsService reactionsService;
  @Autowired
  private ReactionsMetadataService reactionsMetadataService;
  @Autowired
  private PagingParameterProvider<ReactionsMetadata> reactionsPagingParameterProvider;
  @Autowired
  private FriendListMetadataService friendListMetadataService;
  @Autowired
  private PagingParameterProvider<FriendListMetadata> friendListPagingProvider;


  public CollectingTask userDetailsCollectingTask(String facebookUserId) {
    UserDetailsCollectingTask userDetailsCollectingTask = new UserDetailsCollectingTask(facebookUserId);
    populateCommon(userDetailsCollectingTask);
    userDetailsCollectingTask.setUserService(userService);

    return userDetailsCollectingTask;
  }

  public CollectingTask friendListCollectingTask(String facebookUserId) {
    FriendListCollectingTask friendListCollectingTask = new FriendListCollectingTask(facebookUserId);

    populateCommon(friendListCollectingTask);
    friendListCollectingTask.setUserService(userService);
    friendListCollectingTask.setFriendListMetadataService(friendListMetadataService);
    friendListCollectingTask.setPagingParameterProvider(friendListPagingProvider);

    return friendListCollectingTask;
  }

  public CollectingTask userFeedCollectingTask(String facebookUserId) {
    UserFeedCollectingTask userFeedCollectingTask = new UserFeedCollectingTask(facebookUserId);
    populateCommon(userFeedCollectingTask);
    userFeedCollectingTask.setUserService(userService);
    userFeedCollectingTask.setPostService(postService);
    userFeedCollectingTask.setUserFeedMetadataService(userFeedMetadataService);
    userFeedCollectingTask.setPagingParameterProvider(pagingParameterProvider);

    return userFeedCollectingTask;
  }

  public CollectingTask commentsCollectingTask(String userFacebookId, String owningEntityFacebookId) {
    CommentsCollectingTask commentsCollectingTask = new CommentsCollectingTask(userFacebookId, owningEntityFacebookId);

    populateCommon(commentsCollectingTask);
    commentsCollectingTask.setPagingParameterProvider(commentsPagingParameterProvider);
    commentsCollectingTask.setCommentsMetadataService(commentsMetadataService);
    commentsCollectingTask.setCommentService(commentService);
    commentsCollectingTask.setCommentsService(commentsService);

    return commentsCollectingTask;
  }

  public CollectingTask reactionsCollectingTask(String userFacebookId, String owningEntityFacebookId) {
    ReactionsCollectingTask reactionsCollectingTask =
        new ReactionsCollectingTask(userFacebookId, owningEntityFacebookId);

    populateCommon(reactionsCollectingTask);
    reactionsCollectingTask.setPagingParameterProvider(reactionsPagingParameterProvider);
    reactionsCollectingTask.setReactionsMetadataService(reactionsMetadataService);
    reactionsCollectingTask.setReactionService(reactionService);
    reactionsCollectingTask.setReactionsService(reactionsService);

    return reactionsCollectingTask;
  }

  private void populateCommon(CollectingTask collectingTask) {
    collectingTask.setEnvironment(environment);
    collectingTask.setFacebookConnectionRepository(facebookConnectionRepository);
    collectingTask.setDateToZonedDateTimeConverter(dateToZonedDateTimeConverter);
  }
}
