package ro.ace.ucv.dcti.socialanalyser.datacollector.application.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.config.BusinessConfig;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.config.SocialConfig;


/**
 * Main configuration class for the application.
 * Turns on @Component scanning, loads externalized application.properties, and sets up the database.
 *
 * @author Bogdan Feraru
 */
@Configuration
@ComponentScan(basePackages = {"ro.ace.ucv.dcti.socialanalyser.datacollector.application"})
@Import({SocialConfig.class, BusinessConfig.class, CollectorConfig.class})
public class MainConfig {

}
