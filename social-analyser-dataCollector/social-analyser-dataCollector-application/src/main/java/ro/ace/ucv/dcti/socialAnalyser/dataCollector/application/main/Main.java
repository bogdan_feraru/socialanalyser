package ro.ace.ucv.dcti.socialanalyser.datacollector.application.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.MainConfig;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public class Main {

  public static void main(String[] args) {
    AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(MainConfig.class);
    while (annotationConfigApplicationContext.isActive());
  }
}
