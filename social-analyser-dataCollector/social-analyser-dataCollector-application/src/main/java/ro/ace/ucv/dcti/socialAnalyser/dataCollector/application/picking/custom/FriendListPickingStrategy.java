package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;

import java.time.temporal.ChronoUnit;
import java.util.Collection;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.FRIEND_LIST_CHECKING_INTERVAL_DAYS;

/**
 * Created by bogdan.feraru on 5/1/2017.
 */
@Component("friendListPickingStrategy")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class FriendListPickingStrategy implements PickingStrategy<String> {

  @Autowired
  private FriendListMetadataService friendListService;
  @Autowired
  private Environment environment;

  @Override
  public String pickNext() {
    throw new UnsupportedOperationException("Not implemented yet.");
  }

  @Override
  public Collection<String> pickNextBatch(int limit) {
    long upperLimit = DateTimeUtil.getDateTimeUTCNow()
        .minus(Integer.valueOf(environment.getProperty(FRIEND_LIST_CHECKING_INTERVAL_DAYS)),
               ChronoUnit.DAYS).toEpochSecond();
    return friendListService.getUsersWithNonCollectedFriendLists(upperLimit);
  }

  @Override
  public void reset() {
    throw new UnsupportedOperationException("Not implemented yet.");
  }
}
