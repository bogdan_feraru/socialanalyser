package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.POST_COMMENTS_REACTIONS_PICKING_BATCH_SIZE;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Component("postsCommentsReactionsDataCollector")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class PostsCommentsReactionsDataCollector extends SpringDataCollectorChain {

  @Autowired
  @Qualifier("postCommentReactionsPickingStrategy")
  private PickingStrategy<String> postCommentsPickingStrategy;
  @Autowired
  private UserService userService;

  public PostsCommentsReactionsDataCollector(DataCollector next) {
    super(next);
  }

  @Override
  String getPropertyBatchSizeName() {
    return POST_COMMENTS_REACTIONS_PICKING_BATCH_SIZE;
  }

  @Override
  protected PickingStrategy<String> getPickingStrategy() {
    return postCommentsPickingStrategy;
  }

  @Override
  protected CollectingTask constructCollectingTask(String entityId) {
    String facebookUserId = userService.getOwnerIdOfComment(entityId);
    return collectionTasksFactory.reactionsCollectingTask(facebookUserId, entityId);
  }
}
