package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.POST_REACTIONS_PICKING_BATCH_SIZE;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Component("postsReactionsDataCollector")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class PostsReactionsDataCollector extends SpringDataCollectorChain {

  @Autowired
  @Qualifier("postReactionsPickingStrategy")
  private PickingStrategy<String> postCommentsPickingStrategy;
  @Autowired
  private UserService userService;

  public PostsReactionsDataCollector(DataCollector next) {
    super(next);
  }

  @Override
  String getPropertyBatchSizeName() {
    return POST_REACTIONS_PICKING_BATCH_SIZE;
  }

  @Override
  protected PickingStrategy<String> getPickingStrategy() {
    return postCommentsPickingStrategy;
  }

  @Override
  protected CollectingTask constructCollectingTask(String entityId) {
    String facebookUserId = userService.getOwnerIdOfPost(entityId);
    return collectionTasksFactory.reactionsCollectingTask(facebookUserId, entityId);
  }
}
