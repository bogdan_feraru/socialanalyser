package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;

import java.util.List;
import java.util.stream.Collectors;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.ReactionsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.Reaction;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.ReactionOperations;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction.ReactionTemplate;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public class ReactionsCollectingTask extends CollectingTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReactionsCollectingTask.class);

  private ReactionsMetadataService reactionsMetadataService;
  private PagingParameterProvider<ReactionsMetadata> pagingParameterProvider;
  private ReactionService reactionService;
  private ReactionsService reactionsService;

  private final String reactionsOwnerFacebookId;

  public ReactionsCollectingTask(String facebookUserId, String reactionsOwnerFacebookId) {
    super(facebookUserId);
    this.reactionsOwnerFacebookId = reactionsOwnerFacebookId;
  }

  @Override
  public Long collect() {
    ReactionsMetadata reactionsMetadata = reactionsMetadataService.findByOwningEntityId(reactionsOwnerFacebookId);
    PagingParameters pagingParameters = pagingParameterProvider.genNextPageParameters(reactionsMetadata);
    ReactionOperations reactionOperations = new ReactionTemplate(getFacebook());

    Long collectedEntities = 0L;
    while (true) {
      try {
        PagedList<Reaction> pagedReactions =
            reactionOperations.getReactions(reactionsOwnerFacebookId, pagingParameters);
        Long reactionsCount = reactionsService.countReactions(reactionsOwnerFacebookId);
        if (!pagedReactions.isEmpty() && pagedReactions.getTotalCount() > reactionsCount) {
          collectedEntities += pagedReactions.size();
          processPage(pagedReactions, reactionsMetadata);
          pagingParameters = pagedReactions.getNextPage();
        } else {
          reactionsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
          reactionsMetadataService.save(reactionsMetadata);
          return collectedEntities;
        }
      } catch (ResourceNotFoundException | RevokedAuthorizationException ex) {
        reactionsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
        reactionsMetadataService.save(reactionsMetadata);
        throw ex;
      }
    }
  }

  @Override
  protected String getEntityId() {
    return reactionsOwnerFacebookId;
  }

  private void processPage(PagedList<Reaction> reactions, ReactionsMetadata reactionsMetadata) {
    List<Reaction> unsavedReactions = getUnsaved(reactions);
    for (Reaction reaction : unsavedReactions) {
      reactionService.save(reactionsOwnerFacebookId, reaction);

      reactionsMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
      reactionsMetadataService.save(reactionsMetadata);
    }
    if (!unsavedReactions.isEmpty()) {
      LOGGER.info("Finished collecting a list of: {} reactions.", unsavedReactions.size());
    }
  }

  private List<Reaction> getUnsaved(PagedList<Reaction> reactions) {
    return reactions.stream().filter(reaction -> !reactionService.checkExistence(reactionsOwnerFacebookId, reaction))
        .collect(Collectors.toList());
  }

  public void setReactionsMetadataService(
      ReactionsMetadataService reactionsMetadataService) {
    this.reactionsMetadataService = reactionsMetadataService;
  }

  public void setPagingParameterProvider(
      PagingParameterProvider<ReactionsMetadata> pagingParameterProvider) {
    this.pagingParameterProvider = pagingParameterProvider;
  }

  public void setReactionService(
      ReactionService reactionService) {
    this.reactionService = reactionService;
  }

  public void setReactionsService(
      ReactionsService reactionsService) {
    this.reactionsService = reactionsService;
  }
}
