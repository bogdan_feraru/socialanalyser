package ro.ace.ucv.dcti.socialanalyser.datacollector.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingChainOrchestrator;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@Configuration
@EnableScheduling
@PropertySource("classpath:collectorSettings.properties")
public class CollectorConfig {

  @Bean(name = "collectorThreadPool")
  public AsyncTaskExecutor collectorThreadPool() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setThreadNamePrefix("CollectorThread");
    executor.setCorePoolSize(4);
    executor.setMaxPoolSize(4);
    executor.setQueueCapacity(100);

    return executor;
  }

  @Configuration
  static class RegisterTaskSchedulerViaSchedulingConfigurer implements SchedulingConfigurer {

    @Autowired
    DataCollectorChainSupplier dataCollectorChainSupplier;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
      taskRegistrar.setTaskScheduler(poolScheduler());

      taskRegistrar.addTriggerTask(new CollectingChainOrchestrator(dataCollectorChainSupplier), new CollectingTrigger());
    }

    @Bean("concurrentTaskScheduler")
    public TaskScheduler poolScheduler() {
      return new ConcurrentTaskScheduler();
    }
  }
}
