package ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.DEFAULT_FRIENDS_NUMBER;

/**
 * Created by bogdan.feraru on 4/30/2017.
 */
@Component
public class FriendListPagingParameterProvider implements PagingParameterProvider<FriendListMetadata> {

  @Autowired
  private Environment environment;

  @Override
  public PagingParametersExtension genNextPageParameters(FriendListMetadata metadata) {
    return new PagingParametersExtension(Integer.valueOf(environment.getProperty(DEFAULT_FRIENDS_NUMBER)), null, null,
                                         null, null);
  }
}
