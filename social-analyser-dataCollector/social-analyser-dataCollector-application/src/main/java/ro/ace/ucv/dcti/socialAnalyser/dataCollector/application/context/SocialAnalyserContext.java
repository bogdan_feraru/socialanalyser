package ro.ace.ucv.dcti.socialanalyser.datacollector.application.context;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public final class SocialAnalyserContext {

  private SocialAnalyserContext() {
  }

  private static CollectingStatus collectingStatus = CollectingStatus.NOT_STARTED;

  public static synchronized CollectingStatus getCollectingStatus() {
    return collectingStatus;
  }

  public static synchronized void setCollectingStatus(CollectingStatus collectingStatus) {
    SocialAnalyserContext.collectingStatus = collectingStatus;
  }
}
