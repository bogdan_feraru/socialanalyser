package ro.ace.ucv.dcti.socialanalyser.datacollector.application.config;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public final class ApplicationConstants {

  private ApplicationConstants() {}

  public static final String CHECKING_INTERVAL_HOURS = "checking.interval.hours";
  public static final String PAGE_TIME_SEGMENT_MONTHS = "page.time.segment.months";
  public static final String DEFAULT_COMMENTS_NUMBER = "default.comments.number";
  public static final String DEFAULT_FRIENDS_NUMBER = "default.friends.number";
  public static final String DEFAULT_REACTIONS_NUMBER = "default.reactions.number";
  public static final String USER_PICKING_BATCH_SIZE = "user.picking.batch.size";
  public static final String USER_FEED_PICKING_BATCH_SIZE = "user.feed.picking.batch.size";
  public static final String FRIEND_LIST_PICKING_BATCH_SIZE = "friend.list.picking.batch.size";
  public static final String POST_COMMENTS_PICKING_BATCH_SIZE = "post.comments.picking.batch.size";
  public static final String POST_REACTIONS_PICKING_BATCH_SIZE = "post.reactions.picking.batch.size";
  public static final String POST_COMMENTS_COMMENTS_REACTIONS_PICKING_BATCH_SIZE = "post.comments.comments.reactions.picking.batch.size";
  public static final String POST_COMMENTS_REACTIONS_PICKING_BATCH_SIZE = "post.comments.reactions.picking.batch.size";
  public static final String COMMENTS_COMMENTS_PICKING_BATCH_SIZE = "comments.comments.picking.batch.size";

  public static final String FRIEND_LIST_CHECKING_INTERVAL_DAYS = "friend.list.checking.interval.days";
  public static final String USER_FEED_CHECKING_INTERVAL_DAYS = "user.feed.checking.interval.days";
  public static final String POST_COMMENTS_CHECKING_INTERVAL_DAYS = "post.comments.checking.interval.days";
  public static final String POST_REACTIONS_CHECKING_INTERVAL_DAYS = "post.reactions.checking.interval.days";
  public static final String COMMENT_COMMENTS_CHECKING_INTERVAL_DAYS = "comment.comments.checking.interval.days";
  public static final String POST_COMMENT_REACTIONS_CHECKING_INTERVAL_DAYS = "post.comment.reactions.checking.interval.days";
  public static final String COMMENT_COMMENTS_REACTIONS_CHECKING_INTERVAL_DAYS = "comment.comments.reactions.checking.interval.days";
}
