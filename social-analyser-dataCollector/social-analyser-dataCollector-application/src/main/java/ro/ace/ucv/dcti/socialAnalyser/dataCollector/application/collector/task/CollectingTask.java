package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.social.facebook.api.Facebook;

import java.util.Objects;
import java.util.concurrent.Callable;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResult;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResultType;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.dozer.converter.DateToZonedDateTimeConverter;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.connection.FacebookConnectionRepository;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.CHECKING_INTERVAL_HOURS;
import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.PAGE_TIME_SEGMENT_MONTHS;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
public abstract class CollectingTask implements Callable<CollectingResult> {

  private static final Logger LOGGER = LoggerFactory.getLogger(CollectingTask.class);

  private FacebookConnectionRepository facebookConnectionRepository;

  private Facebook facebook;

  private Environment environment;

  private DateToZonedDateTimeConverter dateToZonedDateTimeConverter;

  private final String userFacebookId;

  private CollectingResult.Builder collectingResultBuilder;

  private Long startMilliSecond;

  public CollectingTask(String facebookId) {
    Objects.requireNonNull(facebookId, "The facebook id should not be null!");
    this.userFacebookId = facebookId;
    collectingResultBuilder = new CollectingResult.Builder(userFacebookId);
  }

  @Override
  public CollectingResult call() throws Exception {
    startMilliSecond = System.currentTimeMillis();
    try {
      if (!establishFacebookConnection()) {
        return errorEstablishingConnection();
      }
      Long numberOfEntitiesCollected = collect();
      return collectSuccessfully(numberOfEntitiesCollected);

    } catch (Exception ex) {
      LOGGER.error("Exception occurred...", ex);
      return exceptionOccurred(ex);
    }
  }

  public abstract Long collect();

  public Integer getCheckingIntervalHours() {
    return Integer.valueOf(getEnvironment().getProperty(CHECKING_INTERVAL_HOURS));
  }

  public Integer getPageTimeSegmentMonths() {
    return Integer.valueOf(getEnvironment().getProperty(PAGE_TIME_SEGMENT_MONTHS));
  }

  public void setFacebookConnectionRepository(FacebookConnectionRepository facebookConnectionRepository) {
    this.facebookConnectionRepository = facebookConnectionRepository;
  }

  protected String getUserFacebookId() {
    return userFacebookId;
  }

  protected abstract String getEntityId();

  protected String getEntityType() {
    return this.getClass().getSimpleName();
  }

  protected Environment getEnvironment() {
    return environment;
  }

  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  protected Facebook getFacebook() {
    return facebook;
  }

  protected DateToZonedDateTimeConverter getDateToZonedDateTimeConverter() {
    return dateToZonedDateTimeConverter;
  }

  public void setDateToZonedDateTimeConverter(DateToZonedDateTimeConverter dateToZonedDateTimeConverter) {
    this.dateToZonedDateTimeConverter = dateToZonedDateTimeConverter;
  }

  private boolean establishFacebookConnection() {
    LOGGER.info("Obtaining facebook connection.");
    facebook = facebookConnectionRepository.getConnection(userFacebookId);
    if (facebook == null) {
      LOGGER.error("Could not obtain facebook connection.");
      return false;
    }
    LOGGER.info("Facebook connection established.");
    return true;
  }


  private CollectingResult collectSuccessfully(Long numberOfEntitiesCollected) {
    collectingResultBuilder.duration(System.currentTimeMillis() - startMilliSecond);
    collectingResultBuilder.collectingResultType(CollectingResultType.SUCCESSFUL);
    collectingResultBuilder.message("Exception occurred");
    collectingResultBuilder.entityId(getEntityId());
    collectingResultBuilder.entityType(getEntityType());
    collectingResultBuilder.numberOfEntitiesCollected(numberOfEntitiesCollected);
    return collectingResultBuilder.build();
  }

  private CollectingResult exceptionOccurred(Exception ex) {
    collectingResultBuilder.duration(System.currentTimeMillis() - startMilliSecond);
    collectingResultBuilder.collectingResultType(CollectingResultType.UNSUCCESSFUL);
    collectingResultBuilder.message("Exception occurred");
    collectingResultBuilder.entityId(getEntityId());
    collectingResultBuilder.entityType(getEntityType());
    collectingResultBuilder.exception(ex);

    return collectingResultBuilder.build();
  }

  private CollectingResult errorEstablishingConnection() {
    collectingResultBuilder.duration(System.currentTimeMillis() - startMilliSecond);
    collectingResultBuilder.collectingResultType(CollectingResultType.UNSUCCESSFUL);
    collectingResultBuilder.message("Could not obtain facebook connection.");
    collectingResultBuilder.entityId(getEntityId());
    collectingResultBuilder.entityType(getEntityType());

    return collectingResultBuilder.build();
  }
}
