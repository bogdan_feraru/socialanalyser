
package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.facebook.api.User;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
public class UserDetailsCollectingTask extends CollectingTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsCollectingTask.class);

  private UserService userService;

  public UserDetailsCollectingTask(String facebookUserId) {
    super(facebookUserId);
  }

  @Override
  public Long collect() {
    LOGGER.info("Collecting process for user with facebook id: " + getUserFacebookId() + " started");

    User userProfile = getFacebook().userOperations().getUserProfile();
    userService.saveUser(userProfile);

    LOGGER.info("Collecting process for user with facebook id: " + getUserFacebookId() + " ended");

    return 1L;
  }

  @Override
  protected String getEntityId() {
    return "";
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }
}
