package ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.CommentsService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.Order;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.*;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
@Component
public class CommentsPagingParameterProvider implements PagingParameterProvider<CommentsMetadata> {

  @Autowired
  private CommentsService commentsService;

  @Autowired
  private CommentsMetadataService commentsMetadataService;

  @Autowired
  private Environment environment;

  @Override
  public PagingParametersExtension genNextPageParameters(CommentsMetadata metadata) {
    Long defaultCommentsNumber = Long.valueOf(environment.getProperty(DEFAULT_COMMENTS_NUMBER));
    Comments comments = commentsMetadataService.getCommentsByMetadata(metadata);
    Long commentsNo = commentsService.countComments(comments);
    if (commentsNo < defaultCommentsNumber) {
      commentsNo = defaultCommentsNumber ;
    }
    return new PagingParametersExtension(commentsNo.intValue(), null, null, null, Order.CHRONOLOGICAL);
  }
}
