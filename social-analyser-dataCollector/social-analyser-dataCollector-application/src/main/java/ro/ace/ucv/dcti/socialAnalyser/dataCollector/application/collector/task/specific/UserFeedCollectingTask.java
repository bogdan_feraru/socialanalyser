package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.Post;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.PostService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.AbstractTimePaginatedReferenceMetadata;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
public class UserFeedCollectingTask extends CollectingTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserFeedCollectingTask.class);

  private UserFeedMetadataService userFeedMetadataService;

  private PostService postService;

  private UserService userService;

  private PagingParameterProvider<AbstractTimePaginatedReferenceMetadata> pagingParameterProvider;

  public UserFeedCollectingTask(String facebookUserId) {
    super(facebookUserId);
  }

  @Override
  public Long collect() {
    UserFeedMetadata userFeedMetadata = userFeedMetadataService.getFeedMetadataByFacebookUserId(getUserFacebookId());
    PagingParameters pagingParameters = pagingParameterProvider.genNextPageParameters(userFeedMetadata);
    Long collectedEntities = 0L;
    try {
      while (true) {
        PagedList<Post> pagedPosts = getFacebook().feedOperations().getFeed(pagingParameters);
        if (pagedPosts.isEmpty()) {
          collectedEntities += pagedPosts.size();
          processPage(userFeedMetadata, pagedPosts);
          pagingParameters = pagedPosts.getNextPage();
        } else {
          long hoursDifference = hoursSinceLastCheck(userFeedMetadata);
          if (hoursDifference > getCheckingIntervalHours()) {
            incrementCursors(userFeedMetadata);
          } else {
            userFeedMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
            userFeedMetadataService.save(userFeedMetadata);
            break;
          }
          pagingParameters = pagingParameterProvider.genNextPageParameters(userFeedMetadata);
        }
      }
    } catch (UncategorizedApiException ex) {
      LOGGER.error("Unknown facebook exception occurred.", ex);
      userFeedMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
      userFeedMetadataService.save(userFeedMetadata);
    }

    return collectedEntities;
  }

  @Override
  protected String getEntityId() {
    return "";
  }

  private long hoursSinceLastCheck(UserFeedMetadata userFeedMetadata) {
    return userFeedMetadata.getUpperBoundCursor().until(DateTimeUtil.getDateTimeUTCNow(), ChronoUnit.HOURS);
  }

  private void incrementCursors(UserFeedMetadata userFeedMetadata) {
    userFeedMetadata.setLowerBoundCursor(userFeedMetadata.getUpperBoundCursor());
    ZonedDateTime upperBoundCursor = userFeedMetadata.getUpperBoundCursor().plusMonths(getPageTimeSegmentMonths());
    long until = upperBoundCursor.until(DateTimeUtil.getDateTimeUTCNow(), ChronoUnit.HOURS);
    if (until < 0) {
      upperBoundCursor = ZonedDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
    }
    userFeedMetadata.setUpperBoundCursor(upperBoundCursor);
    userFeedMetadataService.save(userFeedMetadata);
  }

  private void processPage(UserFeedMetadata userFeedMetadata, PagedList<Post> pagedPosts) {

    pagedPosts.forEach(post -> {
      userService.addPostToUserFeed(getUserFacebookId(), post);
      ZonedDateTime lastExtractedCreationDate =
          getDateToZonedDateTimeConverter().convertTo(post.getCreatedTime(), null);
      if (lastExtractedCreationDate.compareTo(userFeedMetadata.getLastExtractedCreationDate()) > 0) {
        userFeedMetadata.setLastExtractedCreationDate(lastExtractedCreationDate);
        userFeedMetadataService.save(userFeedMetadata);
      }
    });
  }

  public UserFeedMetadataService getUserFeedMetadataService() {
    return userFeedMetadataService;
  }

  public void setUserFeedMetadataService(
      UserFeedMetadataService userFeedMetadataService) {
    this.userFeedMetadataService = userFeedMetadataService;
  }

  public PostService getPostService() {
    return postService;
  }

  public void setPostService(PostService postService) {
    this.postService = postService;
  }

  public UserService getUserService() {
    return userService;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public PagingParameterProvider<AbstractTimePaginatedReferenceMetadata> getPagingParameterProvider() {
    return pagingParameterProvider;
  }

  public void setPagingParameterProvider(
      PagingParameterProvider<AbstractTimePaginatedReferenceMetadata> pagingParameterProvider) {
    this.pagingParameterProvider = pagingParameterProvider;
  }
}
