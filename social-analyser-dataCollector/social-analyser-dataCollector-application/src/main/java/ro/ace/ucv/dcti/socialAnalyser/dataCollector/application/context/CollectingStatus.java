package ro.ace.ucv.dcti.socialanalyser.datacollector.application.context;

/**
 * Created by bogdan.feraru on 5/27/2017.
 */
public enum CollectingStatus {
  NOT_STARTED,
  SCHEDULED,
  STARTED,
  LIMIT_EXCEEDED,
  SUCCESSFUL
}
