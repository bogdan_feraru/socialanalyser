package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
public class CollectingResult {

    private CollectingResultType collectingResultType;

    private String entityType;

    private String entityId;

    private String facebookUserId;

    private Exception exception;

    private String message;

    private Long duration;

    private Long numberOfEntitiesCollected;

    private CollectingResult() {
    }

    public static class Builder {

        private CollectingResultType collectingResultType;

        private final String facebookUserId;

        private String entityId;

        private String entityType;

        private Exception exception;

        private String message;

        private Long duration;

        private Long numberOfEntitiesCollected = 0L;

        public Builder(String facebookUserId) {
            this.facebookUserId = facebookUserId;
        }

        public Builder collectingResultType(CollectingResultType collectingResultType) {
            this.collectingResultType = collectingResultType;
            return this;
        }

        public Builder entityType(String entityType) {
            this.entityType = entityType;
            return this;
        }

        public Builder entityId(String entityId) {
            this.entityId = entityId;
            return this;
        }

        public Builder numberOfEntitiesCollected(Long numberOfEntitiesCollected) {
            this.numberOfEntitiesCollected = numberOfEntitiesCollected;
            return this;
        }

        public Builder exception(Exception exception) {
            this.exception = exception;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder duration(Long duration) {
            this.duration = duration;
            return this;
        }

        public CollectingResult build() {
            CollectingResult collectingResult = new CollectingResult();
            collectingResult.collectingResultType = collectingResultType;
            collectingResult.entityType = entityType;
            collectingResult.entityId = entityId;
            collectingResult.facebookUserId = facebookUserId;
            collectingResult.exception = exception;
            collectingResult.message = message;
            collectingResult.duration = duration;
            collectingResult.numberOfEntitiesCollected = numberOfEntitiesCollected;
            return collectingResult;
        }
    }

    public CollectingResultType getCollectingResultType() {
        return collectingResultType;
    }

    public String getEntityType() {
        return entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getFacebookUserId() {
        return facebookUserId;
    }

    public Exception getException() {
        return exception;
    }

    public String getMessage() {
        return message;
    }

    public Long getDuration() {
        return duration;
    }

    protected void setCollectingResultType(CollectingResultType collectingResultType) {
        this.collectingResultType = collectingResultType;
    }

    @Override
    public String toString() {
        return "CollectingResult{" +
                "collectingResultType=" + collectingResultType +
                (entityType == null ? "" : ", entityType='" + entityType + '\'') +
                (entityId == null ? "" : ", entityId='" + entityId + '\'') +
                (facebookUserId == null ? "" : ", facebookUserId='" + facebookUserId + '\'') +
                ", numberOfEntitiesCollected='" + (numberOfEntitiesCollected + '\'') +
                "}";
    }
}
