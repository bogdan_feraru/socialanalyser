package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.stream.Collectors;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FacebookDomainObject;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserFeedMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.USER_FEED_CHECKING_INTERVAL_DAYS;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
@Component("userFeedPickingStrategy")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class UserFeedPickingStrategy implements PickingStrategy<String> {

  @Autowired
  private UserFeedMetadataService userFeedMetadataService;
  @Autowired
  private Environment environment;

  private ZonedDateTime lastRetrieved;

  @Override
  public String pickNext() {
    User userWithOldestFeed;
    if (lastRetrieved == null) {
      userWithOldestFeed = userFeedMetadataService.getUserWithOldestCollectedFeed();
    } else {
      userWithOldestFeed = userFeedMetadataService.getUserWithOldestCollectedFeed(lastRetrieved);
    }

    if (userWithOldestFeed == null) {
      return null;
    }
    UserFeedMetadata feedMetadata =
        userFeedMetadataService.getFeedMetadataByFacebookUserId(userWithOldestFeed.getFacebookId());
    lastRetrieved = feedMetadata.getLastExtractedCreationDate();

    return userWithOldestFeed.getFacebookId();
  }

  @Override
  public Collection<String> pickNextBatch(int limit) {
    ZonedDateTime offset = DateTimeUtil.getDateTimeUTCNow()
        .minus(Integer.valueOf(environment.getProperty(USER_FEED_CHECKING_INTERVAL_DAYS)), ChronoUnit.DAYS);
    Collection<User> usersWithOldestCollectedFeed =
        userFeedMetadataService.getUsersWithOldestCollectedFeed(limit, offset);
    return usersWithOldestCollectedFeed.stream().map(FacebookDomainObject::getFacebookId).collect(Collectors.toList());
  }

  @Override
  public void reset() {
    lastRetrieved = null;
  }
}
