package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
public enum CollectingResultType {
  SUCCESSFUL,
  UNSUCCESSFUL
}
