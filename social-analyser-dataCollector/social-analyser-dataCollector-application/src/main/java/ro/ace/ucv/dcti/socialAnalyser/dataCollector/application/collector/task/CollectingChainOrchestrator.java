package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task;

import org.springframework.stereotype.Component;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.CollectingStatus;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.SocialAnalyserContext;

import java.util.function.Supplier;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
@Component
public class CollectingChainOrchestrator implements Runnable {

  private final Supplier<DataCollector> dataCollectorChainSupplier;

  public CollectingChainOrchestrator(Supplier<DataCollector> dataCollectorChainSupplier) {
    this.dataCollectorChainSupplier = dataCollectorChainSupplier;
  }

  @Override
  public void run() {
    if (SocialAnalyserContext.getCollectingStatus() == CollectingStatus.SCHEDULED) {
      DataCollector dataCollector = dataCollectorChainSupplier.get();
      if (dataCollector != null) {
        SocialAnalyserContext.setCollectingStatus(CollectingStatus.STARTED);
        dataCollector.collect();
      }
      SocialAnalyserContext.setCollectingStatus(CollectingStatus.SUCCESSFUL);
    }
  }
}
