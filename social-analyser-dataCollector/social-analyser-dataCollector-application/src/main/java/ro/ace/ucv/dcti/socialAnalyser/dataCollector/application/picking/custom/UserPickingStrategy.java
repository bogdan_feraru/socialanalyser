package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user.SimpleUser;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user.SimpleUserRepository;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
@Component("userPickingStrategy")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class UserPickingStrategy implements PickingStrategy<String> {

  @Autowired
  private SimpleUserRepository userRepository;

  @Autowired
  private UserService userService;

  private List<String> picked = new ArrayList<>();

  @Override
  public String pickNext() {
    List<SimpleUser> allUsers = userRepository.getAllUsers();
    for (SimpleUser user : allUsers) {
      String userFacebookId = user.getFacebookId();
      if (!userService.isApplicationUser(userFacebookId) && !picked.contains(userFacebookId)) {
        picked.add(userFacebookId);
        return userFacebookId;
      }
    }
    return null;
  }

  @Override
  public Collection<String> pickNextBatch(int limit) {
    List<SimpleUser> allUsers = userRepository.getAllUsers();
    List<String> userBatch = new ArrayList<>();

    for (SimpleUser user : allUsers) {
      String userFacebookId = user.getFacebookId();
      if (!userService.isApplicationUser(userFacebookId) && !picked.contains(userFacebookId)) {
        picked.add(userFacebookId);
        userBatch.add(userFacebookId);
      }
      if (userBatch.size() == limit) {
        return userBatch;
      }
    }

    return userBatch;
  }

  @Override
  public void reset() {
    picked.clear();
  }
}
