package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.handler;

/**
 * Created by bogdan.feraru on 5/26/2017.
 */
public interface ResultHandler<T> {

  void handle(T result);
}
