package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.specific;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.facebook.api.User;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.FriendListMetadataService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.util.DateTimeUtil;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;

/**
 * Created by bogdan.feraru on 4/30/2017.
 */
public class FriendListCollectingTask extends CollectingTask {

  private static final Logger LOGGER = LoggerFactory.getLogger(FriendListCollectingTask.class);

  private FriendListMetadataService friendListMetadataService;
  private UserService userService;
  private PagingParameterProvider<FriendListMetadata> pagingParameterProvider;

  public FriendListCollectingTask(String facebookUserId) {
    super(facebookUserId);
  }

  @Override
  public Long collect() {
    FriendListMetadata friendListMetadata = friendListMetadataService.findByOwningEntityId(getUserFacebookId());
    PagingParameters pagingParameters = pagingParameterProvider.genNextPageParameters(friendListMetadata);

    Long collectedEntities = 0L;
    while (true) {
      if (pagingParameters == null) {
        break;
      }
      PagedList<User> paginatedFriendList =
          getFacebook().friendOperations().getFriendProfiles(getUserFacebookId(), pagingParameters);
      if (paginatedFriendList.isEmpty()) {
        collectedEntities += paginatedFriendList.size();
        processPage(paginatedFriendList);
        pagingParameters = paginatedFriendList.getNextPage();
      } else {
          return saveLastVisitedDate(friendListMetadata, collectedEntities);
      }
    }

    return saveLastVisitedDate(friendListMetadata, collectedEntities);
  }

    private Long saveLastVisitedDate(FriendListMetadata friendListMetadata, Long collectedEntities) {
        friendListMetadata.setLastVisitedDate(DateTimeUtil.getDateTimeUTCNow());
        friendListMetadataService.save(friendListMetadata);
        return collectedEntities;
    }

    @Override
  protected String getEntityId() {
    return "";
  }

  private void processPage(PagedList<User> friends) {
    for (User friend : friends) {
      userService.addFriend(getUserFacebookId(), friend);
    }

    LOGGER.info("Finished collecting a list of: {} friends of user with facebookId = {}", friends.size(),getUserFacebookId());
  }

  public void setFriendListMetadataService(FriendListMetadataService friendListMetadataService) {
    this.friendListMetadataService = friendListMetadataService;
  }

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public void setPagingParameterProvider(PagingParameterProvider<FriendListMetadata> pagingParameterProvider) {
    this.pagingParameterProvider = pagingParameterProvider;
  }
}
