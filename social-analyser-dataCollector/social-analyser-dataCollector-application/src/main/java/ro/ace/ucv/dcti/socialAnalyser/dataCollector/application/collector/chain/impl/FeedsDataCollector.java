package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.USER_FEED_PICKING_BATCH_SIZE;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@Component("feedsDataCollector")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class FeedsDataCollector extends SpringDataCollectorChain {

  @Autowired
  @Qualifier("userFeedPickingStrategy")
  private PickingStrategy<String> userFeedPickingStrategy;

  public FeedsDataCollector(DataCollector next) {
    super(next);
  }

  @Override
  String getPropertyBatchSizeName() {
    return USER_FEED_PICKING_BATCH_SIZE;
  }

  @Override
  protected PickingStrategy<String> getPickingStrategy() {
    return userFeedPickingStrategy;
  }

  @Override
  protected CollectingTask constructCollectingTask(String entityId) {
    return collectionTasksFactory.userFeedCollectingTask(entityId);
  }
}
