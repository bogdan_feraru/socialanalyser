package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.RateLimitExceededException;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResult;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.result.CollectingResultType;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.CollectingStatus;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.context.SocialAnalyserContext;

/**
 * Created by bogdan.feraru on 5/26/2017.
 */
@Component
public class CollectingResultHandler implements ResultHandler<CollectingResult> {

  private static final Logger LOGGER = LoggerFactory.getLogger(CollectingResultHandler.class);

  @Override
  public void handle(CollectingResult result) {
    if (result.getCollectingResultType() == CollectingResultType.UNSUCCESSFUL) {
      String output = result.toString();
      if (result.getException() != null) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        result.getException().printStackTrace(pw);
        output += sw.toString();
      }
      else if(result.getException() instanceof RateLimitExceededException) {
        SocialAnalyserContext.setCollectingStatus(CollectingStatus.LIMIT_EXCEEDED);
      }
      LOGGER.error(output);
    }
    else {
      LOGGER.info("{}", result);
    }
  }

}
