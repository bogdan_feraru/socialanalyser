package ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking;

import java.util.Collection;

/**
 * Created by bogdan.feraru on 4/13/2017.
 */
public interface PickingStrategy<T> {

  T pickNext();

  Collection<T> pickNextBatch(int limit);

  void reset();
}
