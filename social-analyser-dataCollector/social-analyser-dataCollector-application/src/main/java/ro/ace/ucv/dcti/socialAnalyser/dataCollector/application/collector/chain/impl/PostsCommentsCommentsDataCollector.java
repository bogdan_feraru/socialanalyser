package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.business.services.UserService;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.COMMENTS_COMMENTS_PICKING_BATCH_SIZE;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Component("postsCommentsCommentsDataCollector")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class PostsCommentsCommentsDataCollector extends SpringDataCollectorChain {

  @Autowired
  @Qualifier("commentCommentsPickingStrategy")
  private PickingStrategy<String> postCommentsPickingStrategy;
  @Autowired
  private UserService userService;

  public PostsCommentsCommentsDataCollector(DataCollector next) {
    super(next);
  }

  @Override
  String getPropertyBatchSizeName() {
    return COMMENTS_COMMENTS_PICKING_BATCH_SIZE;
  }

  @Override
  protected PickingStrategy<String> getPickingStrategy() {
    return postCommentsPickingStrategy;
  }

  @Override
  protected CollectingTask constructCollectingTask(String entityId) {
    String facebookUserId = userService.getOwnerIdOfComment(entityId);
    return collectionTasksFactory.commentsCollectingTask(facebookUserId, entityId);
  }
}
