package ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.impl;

import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.paging.PagingParameterProvider;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment.PagingParametersExtension;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.AbstractTimePaginatedReferenceMetadata;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
@Component
public class TimeBasedPagingParameterProvider
    implements PagingParameterProvider<AbstractTimePaginatedReferenceMetadata<?>> {

  @Override
  public PagingParametersExtension genNextPageParameters(AbstractTimePaginatedReferenceMetadata<?> metadata) {
    long lastExtractedDate = metadata.getLastExtractedCreationDate().toEpochSecond();
    long lowerBoundCursor = metadata.getLowerBoundCursor().toEpochSecond();
    Long since = lastExtractedDate < lowerBoundCursor ? lowerBoundCursor : lastExtractedDate;
    Long until = metadata.getUpperBoundCursor().toEpochSecond();
    return new PagingParametersExtension(null, null, since, until, null);
  }
}
