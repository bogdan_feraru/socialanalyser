package ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.chain.DataCollector;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.collector.task.CollectingTask;
import ro.ace.ucv.dcti.socialanalyser.datacollector.application.picking.PickingStrategy;

import static ro.ace.ucv.dcti.socialanalyser.datacollector.application.config.ApplicationConstants.USER_PICKING_BATCH_SIZE;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
@Component("usersDataCollector")
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class UsersDataCollector extends SpringDataCollectorChain {

  @Autowired
  @Qualifier("userPickingStrategy")
  private PickingStrategy<String> userPickingStrategy;

  public UsersDataCollector(DataCollector next) {
    super(next);
  }

  @Override
  protected CollectingTask constructCollectingTask(String entityId) {
    return collectionTasksFactory.userDetailsCollectingTask(entityId);
  }

  @Override
  String getPropertyBatchSizeName() {
    return USER_PICKING_BATCH_SIZE;
  }

  @Override
  protected PickingStrategy<String> getPickingStrategy() {
    return userPickingStrategy;
  }
}
