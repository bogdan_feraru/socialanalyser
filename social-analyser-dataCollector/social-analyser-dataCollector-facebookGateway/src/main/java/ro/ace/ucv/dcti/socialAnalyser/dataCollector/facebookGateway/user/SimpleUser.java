package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user;

/**
 * Simple little SimpleUser model.
 * Just stores the user's id for simplicity.
 *
 * @author Bogdan Feraru
 */
public final class SimpleUser {

  private final String id;

  private final String facebookId;

  public SimpleUser(String id, String facebookUserId) {
    this.id = id;
    this.facebookId = facebookUserId;
  }

  public String getId() {
    return id;
  }

  public String getFacebookId() {
    return facebookId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    SimpleUser that = (SimpleUser) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    return facebookId.equals(that.facebookId);
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + facebookId.hashCode();
    return result;
  }
}
