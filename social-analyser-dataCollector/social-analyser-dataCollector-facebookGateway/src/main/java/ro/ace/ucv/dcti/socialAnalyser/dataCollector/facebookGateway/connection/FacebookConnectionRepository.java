package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.connection;

import org.springframework.social.facebook.api.Facebook;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
public interface FacebookConnectionRepository {

  Facebook getConnection(String facebookUserId);
}
