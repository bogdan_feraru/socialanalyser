package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction;

import org.springframework.social.ApiException;
import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public interface ReactionOperations {

  /**
   * Retrieves reactions for a given object.
   * @param objectId the ID of the object
   * @param pagedListParameters the parameters defining the bounds of the list to return.
   * @return a list of {@link Comment}s for the specified object
   * @throws ApiException if there is an error while communicating with Facebook.
   */
  PagedList<Reaction> getReactions(String objectId, PagingParameters pagedListParameters);
}
