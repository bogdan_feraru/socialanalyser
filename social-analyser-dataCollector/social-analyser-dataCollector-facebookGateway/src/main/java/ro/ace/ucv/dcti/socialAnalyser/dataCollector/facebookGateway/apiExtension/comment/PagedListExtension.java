package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment;

import org.springframework.social.facebook.api.PagedList;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
public class PagedListExtension<T> extends PagedList<T> {

  private final Order order;

  PagedListExtension(PagedList<T> pagedList, Order order) {
    super(pagedList, pagedList.getPreviousPage(), pagedList.getNextPage(), pagedList.getTotalCount());
    this.order = order;
  }

  @Override
  public PagingParametersExtension getPreviousPage() {
    if (super.getPreviousPage() == null) {
      return null;
    }
    return new PagingParametersExtension(super.getPreviousPage(), order);
  }

  @Override
  public PagingParametersExtension getNextPage() {
    if (super.getNextPage() == null) {
      return null;
    }
    return new PagingParametersExtension(super.getNextPage(), order);
  }
}
