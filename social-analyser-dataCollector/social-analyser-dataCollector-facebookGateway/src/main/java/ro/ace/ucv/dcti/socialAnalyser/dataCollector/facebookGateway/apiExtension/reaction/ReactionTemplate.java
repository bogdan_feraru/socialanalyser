package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.PagingParameters;
import org.springframework.social.support.URIBuilder;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.social.facebook.api.impl.PagedListUtils.getPagedListParameters;
import static org.springframework.social.facebook.api.impl.PagedListUtils.getPagingParameters;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public class ReactionTemplate implements ReactionOperations {
  private static final Logger LOGGER = LoggerFactory.getLogger(ReactionTemplate.class);

  private final Facebook graphApi;
  private ObjectMapper objectMapper;

  public ReactionTemplate(Facebook graphApi) {
    this.graphApi = graphApi;
    objectMapper = new ObjectMapper();
    objectMapper.addMixIn(Reaction.class, ReactionMixin.class);
  }

  @Override
  public PagedList<Reaction> getReactions(String objectId, PagingParameters pagedListParameters) {
    if (pagedListParameters == null) {
      return new PagedList<>(new ArrayList<>(), null, null);
    }
    MultiValueMap<String, String> pagingParameters = getPagingParameters(pagedListParameters);
    pagingParameters.add("summary", "1");
    try {
      return fetchConnections(objectId, "reactions", Reaction.class, pagingParameters);
    } catch (UncategorizedApiException ex) {
      try {
        return fetchConnections(getId(objectId), "reactions", Reaction.class, pagingParameters);
      }catch (UncategorizedApiException e) {
        return new PagedList<>(new ArrayList<>(), null, null);
      }
    }
  }

  private <T> PagedList<T> fetchConnections(String objectId, String connectionType, Class<T> type,
                                            MultiValueMap<String, String> queryParameters) {
    String connectionPath = connectionType != null && connectionType.length() > 0 ? "/" + connectionType : "";
    URIBuilder uriBuilder =
        URIBuilder.fromUri(graphApi.getBaseGraphApiUrl() + objectId + connectionPath).queryParams(queryParameters);
    JsonNode jsonNode = graphApi.restOperations().getForObject(uriBuilder.build(), JsonNode.class);
    return pagify(type, jsonNode);
  }

  private <T> PagedList<T> pagify(Class<T> type, JsonNode jsonNode) {
    List<T> data = deserializeDataList(jsonNode.get("data"), type);
    if (!jsonNode.has("paging")) {
      return new PagedList<T>(data, null, null);
    }

    JsonNode pagingNode = jsonNode.get("paging");
    PagingParameters previousPage = getPagedListParameters(pagingNode, "previous");
    PagingParameters nextPage = getPagedListParameters(pagingNode, "next");

    Integer totalCount = null;
    if (jsonNode.has("summary")) {
      JsonNode summaryNode = jsonNode.get("summary");
      if (summaryNode.has("total_count")) {
        totalCount = summaryNode.get("total_count").intValue();
      }
    }

    return new PagedList<T>(data, previousPage, nextPage, totalCount);
  }

  @SuppressWarnings("unchecked")
  private <T> List<T> deserializeDataList(JsonNode jsonNode, final Class<T> elementType) {
    try {
      CollectionType listType = TypeFactory.defaultInstance().constructCollectionType(List.class, elementType);
      return objectMapper.readerFor(listType).readValue(jsonNode.toString());
    } catch (IOException e) {
      throw new UncategorizedApiException("facebook", "Error deserializing data from Facebook: " + e.getMessage(), e);
    }
  }

  private String getId(String id) {
    return !id.contains("_") ? id : id.substring(id.indexOf("_") + 1, id.length());
  }
}
