package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class SimpleUserRepository {

  private static final String SELECT_USER_IDS =
      "SELECT userconnection.userId, userconnection.providerUserId FROM userconnection;";

  private static final String GET_USER_BY_USER_ID =
      "SELECT userconnection.userId, userconnection.providerUserId FROM userconnection WHERE"
      + " userConnection.providerUserId=?;";

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<SimpleUser> getAllUsers() {
    return jdbcTemplate.query(SELECT_USER_IDS, new SimpleUserRowMapper());
  }

  public SimpleUser getFacebookById(String facebookId) {
    List<SimpleUser> user =
        jdbcTemplate.query(GET_USER_BY_USER_ID, new String[]{facebookId}, new SimpleUserRowMapper());
    if (user.size() == 0) {
      return null;
    }
    return user.get(0);
  }

  protected class SimpleUserRowMapper implements RowMapper<SimpleUser> {

    @Override
    public SimpleUser mapRow(ResultSet rs, int rowNum) throws SQLException {
      return new SimpleUser(rs.getString(1), rs.getString(2));
    }
  }
}
