package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment;

import org.springframework.social.UncategorizedApiException;
import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.GraphApi;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by bogdan.feraru on 4/15/2017.
 */
public class CommentOperationsExtension {

  private final GraphApi graphApi;

  public CommentOperationsExtension(GraphApi graphApi) {
    this.graphApi = graphApi;
  }

  public PagedListExtension<Comment> getOrderedComments(String facebookEntityId,
                                                        PagingParametersExtension pagingParametersExtension) {
    MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    if (pagingParametersExtension != null) {
      parameters.add("order", pagingParametersExtension.getOrder().toString());
      parameters.add("summary", "1");
      parameters.add("limit", String.valueOf(pagingParametersExtension.getLimit()));
      if (pagingParametersExtension.getAfter() != null) {
        parameters.add("after", pagingParametersExtension.getAfter());
      }
      try {
        return new PagedListExtension<>(graphApi.fetchConnections(
            facebookEntityId, "comments", Comment.class, parameters), pagingParametersExtension.getOrder());
      } catch (UncategorizedApiException ex) {
        try {
          return new PagedListExtension<>(graphApi.fetchConnections(
              getId(facebookEntityId), "comments", Comment.class, parameters), pagingParametersExtension.getOrder());
        } catch (UncategorizedApiException e) {
          return new PagedListExtension<>(new PagedList<>(new ArrayList<>(), null, null), Order.CHRONOLOGICAL);
        }
      }
    }

    return new PagedListExtension<>(new PagedList<>(Collections.emptyList(), null, null), null);
  }

  private String getId(String id) {
    return !id.contains("_") ? id : id.substring(id.indexOf("_") + 1, id.length());
  }
}
