package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;

import javax.sql.DataSource;

/**
 * Spring Social Configuration.
 *
 * @author Bogdan Feraru
 */
@Configuration
@EnableSocial
@ComponentScan(basePackages = {"ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway"})
@PropertySource("classpath:application.properties")
@Import(ConnectionDatabaseConfig.class)
public class SocialConfig implements SocialConfigurer {

  @Autowired
  private DataSource dataSource;

  @Override
  public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
    cfConfig.addConnectionFactory(
        new FacebookConnectionFactory(env.getProperty("facebook.appKey"), env.getProperty("facebook.appSecret")));
  }

  @Override
  public UserIdSource getUserIdSource() {
    return () -> "1";
  }

  /**
   * Singleton data access object providing access to connections across all users.
   */
  @Override
  public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
    JdbcUsersConnectionRepository repository =
        new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
    return repository;
  }
}
