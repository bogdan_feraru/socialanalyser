package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReactionMixin {

  @JsonCreator
  ReactionMixin(
      @JsonProperty("id") String id,
      @JsonProperty("name") String name,
      @JsonProperty("type") String reactionType) {}
}
