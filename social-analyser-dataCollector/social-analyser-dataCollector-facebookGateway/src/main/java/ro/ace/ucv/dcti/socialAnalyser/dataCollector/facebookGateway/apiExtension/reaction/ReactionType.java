package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public class ReactionType {

  private String name;

  public ReactionType() {
  }

  public ReactionType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
