package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment;

/**
 * Created by bogdan.feraru on 4/15/2017.
 */
public enum Order {
  RANKED("ranked"),
  CHRONOLOGICAL("chronological"),
  REVERSE_CHRONOLOGICAL("reverse_chronological");

  private Order(String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }

  private String stringRepresentation;

  @Override
  public String toString() {
    return stringRepresentation;
  }
}
