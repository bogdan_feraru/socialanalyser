package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.connection.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Component;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.connection.FacebookConnectionRepository;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user.SimpleUser;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.user.SimpleUserRepository;

/**
 * Created by bogdan.feraru on 4/12/2017.
 */
@Component
public class FacebookConnectionRepositoryImpl implements FacebookConnectionRepository {

  @Autowired
  private UsersConnectionRepository usersConnectionRepository;

  @Autowired
  private SimpleUserRepository simpleUserRepository;

  @Override
  public Facebook getConnection(String facebookUserId) {
    SimpleUser simpleUser = simpleUserRepository.getFacebookById(facebookUserId);
    if (simpleUser == null) {
      return null;
    }

    ConnectionRepository connectionRepository =
        usersConnectionRepository.createConnectionRepository(simpleUser.getId());
    Connection<Facebook> connection = connectionRepository.findPrimaryConnection(Facebook.class);
    return connection != null ? connection.getApi() : null;
  }
}
