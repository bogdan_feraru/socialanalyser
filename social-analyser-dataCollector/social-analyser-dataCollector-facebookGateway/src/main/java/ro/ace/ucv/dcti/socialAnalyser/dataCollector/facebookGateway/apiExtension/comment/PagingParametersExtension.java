package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.comment;

import org.springframework.social.facebook.api.PagingParameters;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
public class PagingParametersExtension extends PagingParameters {

  private final Order order;

  PagingParametersExtension(PagingParameters pagingParameters, Order order) {
    super(pagingParameters.getLimit(), pagingParameters.getOffset(), pagingParameters.getSince(),
          pagingParameters.getUntil(), pagingParameters.getAfter(), pagingParameters.getBefore(),
          pagingParameters.getPagingToken(), pagingParameters.getFullUrl());
    this.order = order;
  }

  public PagingParametersExtension(Integer limit, Integer offset, Long since, Long until, Order order) {
    super(limit, offset, since, until, null, null, null, null);
    this.order = order;
  }

  public Order getOrder() {
    return order;
  }
}
