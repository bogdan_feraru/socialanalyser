package ro.ace.ucv.dcti.socialAnalyser.dataCollector.facebookGateway.apiExtension.reaction;

import org.springframework.social.facebook.api.Reference;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public class Reaction {

  private Reference user;

  private ReactionType reactionType;

  public Reaction(String facebookUserId, String userName, String reactionType) {
    user = new Reference(facebookUserId, userName);
    this.reactionType = new ReactionType(reactionType);
  }

  public Reference getUser() {
    return user;
  }

  public ReactionType getReactionType() {
    return reactionType;
  }
}
