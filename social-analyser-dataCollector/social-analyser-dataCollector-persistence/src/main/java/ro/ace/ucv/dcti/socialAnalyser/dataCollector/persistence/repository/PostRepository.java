package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Post;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
public interface PostRepository extends Neo4jRepository<Post, Long> {

  Post findByFacebookId(String facebookId);
}
