package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public interface UserRepository extends Neo4jRepository<User, Long> {

  @Query("MATCH (user:USER) WHERE user.facebookId = {0} RETURN user;")
  User findByFacebookId(String facebookId);

  @Query("MATCH (user:USER) WHERE user.name = {0} RETURN user;")
  User findByName(String name);

  @Query("MATCH (user:USER)-[:HAS]->(friendList:FRIEND_LIST)-[:HAS]->(friend:USER) WHERE "
         + "user.facebookId={0} AND friend.facebookId={1} RETURN count(user)>0 AS areFriends;")
  boolean areFriends(String userFacebookId, String friendId);

  @Query("MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(post:POST)-[:HAS]->(:COMMENTS)"
         + " WHERE post.facebookId={0} RETURN user.facebookId LIMIT 1;")
  String getOwnerIdOfPost(String postFacebookId);

  @Query("MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(comment:COMMENT)-[:HAS]->(:COMMENTS)"
         + " WHERE comment.facebookId={0} RETURN user.facebookId LIMIT 1;")
  String getOwnerIdOfComment(String commentFacebookId);

  @Query("MATCH (user:USER)-[:HAS]->(:USER_FEED)-[:HAS]->(:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(COMMENT)-[:HAS]->"
         + "(:COMMENTS)-[:HAS]->(comment:COMMENT)"
         + " WHERE comment.facebookId={0} RETURN user.facebookId LIMIT 1;")
  String getOwnerIdOfCommentComment(String commentFacebookId);
}
