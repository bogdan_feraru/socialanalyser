package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.FriendListMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface FriendListMetadataRepository extends Neo4jRepository<FriendListMetadata, Long> {

  @Query("MATCH (friendListMetadata:FRIEND_LIST_METADATA)-[:REFERENCES]->(:FRIEND_LIST)-[:REFERENCES]->(item)"
         + " WHERE item.facebookId={0} RETURN friendListMetadata;")
  FriendListMetadata findByOwningEntityId(String userFacebookId);

  @Query("MATCH (user:USER)-[:HAS]->(friendList:FRIEND_LIST)<-[:REFERENCES]-(friendListMetadata:FRIEND_LIST_METADATA)"
         + " WHERE toInteger(friendListMetadata.lastVisitedDate) <= {0} RETURN user.facebookId")
  List<String> getUsersWithNonCollectedFriendLists(Long upperLimit);
}
