package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;


import org.neo4j.ogm.annotation.NodeEntity;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserFeed;

@NodeEntity(label = "USER_FEED_METADATA")
public class UserFeedMetadata extends AbstractTimePaginatedReferenceMetadata<UserFeed> {
}

