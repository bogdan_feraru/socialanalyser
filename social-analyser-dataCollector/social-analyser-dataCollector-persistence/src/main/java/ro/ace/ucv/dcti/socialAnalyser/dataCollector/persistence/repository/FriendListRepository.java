package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FriendList;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface FriendListRepository extends Neo4jRepository<FriendList, Long> {

  @Query("MATCH (user:USER)-[:HAS]->(friendList:FRIEND_LIST) WHERE user.facebookId={0} RETURN friendList;")
  FriendList findByUserFacebookId(String userFacebookId);
}
