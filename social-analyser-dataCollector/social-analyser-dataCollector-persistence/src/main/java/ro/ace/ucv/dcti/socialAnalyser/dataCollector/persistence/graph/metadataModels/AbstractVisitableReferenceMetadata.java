package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainObject;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.property.converter.ZonedDateTimePropertyConverter;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public abstract class AbstractVisitableReferenceMetadata<T extends DomainObject> extends DomainObject {
  public static final ZonedDateTime FACEBOOK_RELEASE_DATE = ZonedDateTime.of(2004, 1, 1, 0, 0, 0, 0, ZoneId.of("UTC"));

  @Property
  @Convert(ZonedDateTimePropertyConverter.class)
  private ZonedDateTime lastVisitedDate = FACEBOOK_RELEASE_DATE;

  public ZonedDateTime getLastVisitedDate() {
    return lastVisitedDate;
  }

  public void setLastVisitedDate(ZonedDateTime lastVisitedDate) {
    this.lastVisitedDate = lastVisitedDate;
  }
}
