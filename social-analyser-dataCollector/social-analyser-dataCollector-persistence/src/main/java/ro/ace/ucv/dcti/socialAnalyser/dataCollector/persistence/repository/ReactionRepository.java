package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reaction;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public interface ReactionRepository extends Neo4jRepository<Reaction, Long> {

  @Query("MATCH (item)<-[:REFERENCES]-(:REACTIONS)-[:HAS]->(reaction:REACTION)-[:CREATED_BY]->(user:USER)"
         + " WHERE item.facebookId={0} AND user.facebookId={1} AND reaction.reactionType={2}"
         + " RETURN COUNT(reaction)>0 AS exists;")
  boolean checkExistence(String owningEntityFacebookId, String userId, String reactionType);
}
