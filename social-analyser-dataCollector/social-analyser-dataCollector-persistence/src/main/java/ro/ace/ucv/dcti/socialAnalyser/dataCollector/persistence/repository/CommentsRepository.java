package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface CommentsRepository extends Neo4jRepository<Comments, Long> {

  @Query("MATCH (comments:COMMENTS)-[:REFERENCES]->(reference) WHERE reference.facebookId={0} RETURN comments;")
  Comments findByReferenceId(String facebookReferenceId);

  @Query("MATCH (comment:COMMENT)<-[:HAS]-(comments:COMMENTS)-[:REFERENCES]->(reference) WHERE reference.facebookId={0} RETURN COUNT(comment);")
  Long countComments(String facebookId);
}
