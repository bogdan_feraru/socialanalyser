package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Tag;

/**
 * Created by bogdan.feraru on 5/7/2017.
 */
public interface TagRepository extends Neo4jRepository<Tag, Long> {

  Tag findByName(String name);
}
