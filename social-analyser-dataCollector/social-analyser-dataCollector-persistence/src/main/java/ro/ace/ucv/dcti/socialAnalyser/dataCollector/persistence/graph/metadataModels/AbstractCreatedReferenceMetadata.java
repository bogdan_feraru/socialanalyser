package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainObject;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.property.converter.ZonedDateTimePropertyConverter;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
public abstract class AbstractCreatedReferenceMetadata<T extends DomainObject> extends AbstractReferenceMetadata<T> {

  @Property
  @Convert(ZonedDateTimePropertyConverter.class)
  private ZonedDateTime lastExtractedCreationDate = FACEBOOK_RELEASE_DATE;

  public ZonedDateTime getLastExtractedCreationDate() {
    return lastExtractedCreationDate;
  }

  public void setLastExtractedCreationDate(ZonedDateTime lastExtractedCreationDate) {
    this.lastExtractedCreationDate = lastExtractedCreationDate;
  }
}
