package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainHour;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
public interface DomainHourRepository extends Neo4jRepository<DomainHour, Long> {

  @Query("MATCH (hour:HOUR) WHERE hour.externalId={0} RETURN hour;")
  DomainHour findByExternalId(String externalId);
}
