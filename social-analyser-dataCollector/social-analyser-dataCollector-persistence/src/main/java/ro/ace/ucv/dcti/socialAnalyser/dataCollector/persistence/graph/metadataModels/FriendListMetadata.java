package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.NodeEntity;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.FriendList;

@NodeEntity(label = "FRIEND_LIST_METADATA")
public class FriendListMetadata extends AbstractReferenceMetadata<FriendList> {
}
