package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Place;

/**
 * Created by bogdan.feraru on 5/6/2017.
 */
public interface PlaceRepository extends Neo4jRepository<Place, Long> {

  Place findByFacebookId(String facebookId);
}
