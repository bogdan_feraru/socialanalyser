package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserStatus;

/**
 * Created by bogdan.feraru on 4/10/2017.
 */
public interface UserStatusRepository extends Neo4jRepository<UserStatus, Long> {

  @Query("MATCH (userStatus:USER_STATUS)-[has:REFERENCES]->(user:USER) WHERE user.facebookId={0} RETURN userStatus;")
  UserStatus getByFacebookUserId(String facebookUserId);
}
