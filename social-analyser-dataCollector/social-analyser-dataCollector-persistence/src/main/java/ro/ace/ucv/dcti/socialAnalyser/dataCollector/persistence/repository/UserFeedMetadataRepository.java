package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.User;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.UserFeedMetadata;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
public interface UserFeedMetadataRepository extends Neo4jRepository<UserFeedMetadata, Long> {

  @Query("MATCH (userFeedMetadata:USER_FEED_METADATA)-[references:REFERENCES]->"
         + "(userFeed:USER_FEED)<-[has:HAS]-(user:USER) WHERE user.facebookId={0} RETURN userFeedMetadata;")
  UserFeedMetadata getByFacebookUserId(String facebookUserId);

  @Query("MATCH (post:POST)<-[has:HAS]-(feed:USER_FEED)<-[has1:HAS]-(user:USER) WHERE user.facebookId={0}"
         + " RETURN max(post.creationTime);")
  Long getLatestFeedPostOfUser(String facebookId);

  @Query("MATCH (userFeedMetadata:USER_FEED_METADATA)-[:REFERENCES]->(userFeed:USER_FEED)-[:REFERENCES]->(user:USER)"
         + " WITH user, toInteger(userFeedMetadata.lastVisitedDate) AS lastVisitedDate RETURN user"
         + " ORDER BY lastVisitedDate LIMIT 1;")
  User getUserWithOldestCollectedFeed();

  @Query("MATCH (userFeedMetadata:USER_FEED_METADATA)-[:REFERENCES]->(userFeed:USER_FEED)-[has:REFERENCES]->(user:USER)"
         + " WITH user, toInteger(userFeedMetadata.lastVisitedDate) AS lastExtractedDate"
         + " WHERE lastVisitedDate>{0} RETURN user ORDER BY lastVisitedDate LIMIT 1;")
  User getUserWithOldestCollectedFeed(Long timestampOffset);

  @Query("MATCH (userFeedMetadata:USER_FEED_METADATA)-[:REFERENCES]->(userFeed:USER_FEED)-[:REFERENCES]->(user:USER)"
         + " WITH user, toInteger(userFeedMetadata.lastVisitedDate) AS lastVisitedDate RETURN user"
         + " ORDER BY lastVisitedDate LIMIT {0};")
  Collection<User> getUsersWithOldestCollectedFeed(Integer limit);

  @Query("MATCH (userFeedMetadata:USER_FEED_METADATA)-[:REFERENCES]->(userFeed:USER_FEED)-[has:REFERENCES]->(user:USER)"
         + " WITH user, toInteger(userFeedMetadata.lastVisitedDate) AS lastVisitedDate"
         + " WHERE lastVisitedDate<{1} RETURN user ORDER BY lastVisitedDate LIMIT {0};")
  Collection<User> getUsersWithOldestCollectedFeed(Integer limit, Long timestampOffset);
}
