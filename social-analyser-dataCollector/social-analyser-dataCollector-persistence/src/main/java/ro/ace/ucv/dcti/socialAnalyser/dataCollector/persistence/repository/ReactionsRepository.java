package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface ReactionsRepository extends Neo4jRepository<Reactions, Long> {

  @Query("MATCH (reactions:REACTIONS)-[:REFERENCES]->(reference) WHERE reference.facebookId={0} RETURN reactions;")
  Reactions findByReferenceId(String facebookReferenceId);

  @Query("MATCH (reaction:REACTION)<-[:HAS]-(:REACTIONS)-[:REFERENCES]->(reference) WHERE reference.facebookId={0} RETURN count(reaction);")
  Long countReactions(String facebookId);
}
