package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.CommentsMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface CommentsMetadataRepository extends Neo4jRepository<CommentsMetadata, Long> {

  @Query("MATCH (commentsMetadata:COMMENTS_METADATA)-[:REFERENCES]->(:COMMENTS)-[:REFERENCES]->(item)"
         + " WHERE item.facebookId={0} RETURN commentsMetadata;")
  CommentsMetadata findByOwningEntityId(String owningEntityId);

  @Query("MATCH (comments:COMMENTS)<-[:REFERENCES]-(commentsMetadata:COMMENTS_METADATA) "
         + "WHERE id(commentsMetadata)={0} RETURN comments;")
  Comments getCommentsByMetadataId(Long id);

  @Query("MATCH (commentsMetadata:COMMENTS_METADATA)-[:REFERENCES]->(comments:COMMENTS)-[:REFERENCES]->(post:POST)"
         + " WITH post, toInteger(commentsMetadata.lastVisitedDate) AS lastVisitedDate "
         + "WHERE lastVisitedDate<{0} RETURN post.facebookId ORDER BY lastVisitedDate LIMIT {1};")
  Collection<String> getOldestVisitedPostComments(Long timestampOffset, Integer limit);

  @Query("MATCH (commentsMetadata:COMMENTS_METADATA)-[:REFERENCES]->(comments:COMMENTS)-[:REFERENCES]->(post:POST)"
         + " WITH post, toInteger(commentsMetadata.lastVisitedDate) AS lastVisitedDate RETURN post.facebookId"
         + " ORDER BY lastVisitedDate LIMIT {0};")
  Collection<String> getOldestVisitedPostComments(Integer limit);

  @Query("MATCH (:POST)-[:HAS]->(:COMMENTS)-[:HAS]->(comment:COMMENT)-[:HAS]->(:COMMENTS)<-[:REFERENCES]-(commentsMetadata:COMMENTS_METADATA)"
         + " WITH comment, toInteger(commentsMetadata.lastVisitedDate) AS lastVisitedDate "
         + "WHERE lastVisitedDate<{0} RETURN comment.facebookId ORDER BY lastVisitedDate LIMIT {1};")
  Collection<String> getOldestVisitedCommentComments(Long offset, Integer limit);
}
