package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.Relationship;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainObject;

/**
 * Created by bogdan.feraru on 4/23/2017.
 */
public abstract class AbstractReferenceMetadata<T extends DomainObject> extends AbstractVisitableReferenceMetadata<T> {

  @Relationship(type = "REFERENCES", direction = Relationship.OUTGOING)
  private T reference;

  public T getReference() {
    return reference;
  }

  public void setReference(T reference) {
    this.reference = reference;
  }
}
