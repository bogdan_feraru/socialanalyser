package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.UserFeed;

/**
 * Created by bogdan.feraru on 4/2/2017.
 */
public interface UserFeedRepository extends Neo4jRepository<UserFeed, Long> {

  @Query("MATCH (user:USER)-[:HAS]->(userFeed:USER_FEED) WHERE user.facebookId={0} RETURN userFeed;")
  UserFeed findByFacebookUserId(String facebookUserId);
}
