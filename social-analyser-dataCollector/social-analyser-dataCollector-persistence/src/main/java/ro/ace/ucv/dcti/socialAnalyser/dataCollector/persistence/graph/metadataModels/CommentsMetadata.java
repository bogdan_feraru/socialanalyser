package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.NodeEntity;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comments;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "COMMENTS_METADATA")
public class CommentsMetadata extends AbstractCreatedReferenceMetadata<Comments> {
}
