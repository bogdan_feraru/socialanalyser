package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.NodeEntity;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Reactions;

/**
 * Created by bogdan.feraru on 4/8/2017.
 */
@NodeEntity(label = "REACTIONS_METADATA")
public class ReactionsMetadata extends AbstractReferenceMetadata<Reactions> {
}
