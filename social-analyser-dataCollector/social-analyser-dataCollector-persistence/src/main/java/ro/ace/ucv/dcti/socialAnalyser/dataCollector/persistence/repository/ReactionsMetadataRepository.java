package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.Collection;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels.ReactionsMetadata;

/**
 * Created by bogdan.feraru on 4/9/2017.
 */
public interface ReactionsMetadataRepository extends Neo4jRepository<ReactionsMetadata, Long> {

  @Query("MATCH (reactionsMetadata:REACTIONS_METADATA)-[:REFERENCES]->(:REACTIONS)-[:REFERENCES]->(item)"
         + " WHERE item.facebookId={0} RETURN reactionsMetadata;")
  ReactionsMetadata findByOwningEntityId(String owningEntityId);

  @Query("MATCH (reactionsMetadata:REACTIONS_METADATA)-[:REFERENCES]->(reactions:REACTIONS)-[:REFERENCES]->(post:POST)"
         + " WITH post, toInteger(reactionsMetadata.lastVisitedDate) AS lastVisitedDate"
         + " WHERE lastVisitedDate<{0} RETURN post.facebookId ORDER BY lastVisitedDate LIMIT {1};")
  Collection<String> getOldestVisitedPostReactions(Long offset, int limit);

  @Query(
      "MATCH (reactionsMetadata:REACTIONS_METADATA)-[:REFERENCES]->(reactions:REACTIONS)-[:REFERENCES]->(comment:COMMENT)"
      + "<-[:HAS]-(COMMENTS)<-[:HAS]-(:POST) WITH comment, toInteger(reactionsMetadata.lastVisitedDate) AS lastVisitedDate "
      + "WHERE lastVisitedDate<{0} RETURN comment.facebookId ORDER BY lastVisitedDate LIMIT {1};")
  Collection<String> getOldestVisitedPostCommentReactions(Long offset, int limit);

  @Query(
      "MATCH (reactionsMetadata:REACTIONS_METADATA)-[:REFERENCES]->(reactions:REACTIONS)-[:REFERENCES]->(comment:COMMENT)"
      + "<-[:HAS]-(:COMMENTS)<-[:HAS]-(:COMMENT)<-[:HAS]-(COMMENTS)<-[:HAS]-(:POST)"
      + " WITH comment, toInteger(reactionsMetadata.lastVisitedDate) AS lastVisitedDate "
      + "WHERE lastVisitedDate<{0} RETURN comment.facebookId ORDER BY lastVisitedDate LIMIT {1};")
  Collection<String> getOldestVisitedCommentCommentReactions(Long offset, int limit);
}
