package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.graph.metadataModels;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.time.ZonedDateTime;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.DomainObject;
import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.property.converter.ZonedDateTimePropertyConverter;

public abstract class AbstractTimePaginatedReferenceMetadata<T extends DomainObject>
    extends AbstractCreatedReferenceMetadata<T> {

  @Property
  @Convert(ZonedDateTimePropertyConverter.class)
  private ZonedDateTime lowerBoundCursor = FACEBOOK_RELEASE_DATE;

  @Property
  @Convert(ZonedDateTimePropertyConverter.class)
  private ZonedDateTime upperBoundCursor = FACEBOOK_RELEASE_DATE.plusMonths(6);

  public ZonedDateTime getLowerBoundCursor() {
    return lowerBoundCursor;
  }

  public void setLowerBoundCursor(ZonedDateTime lowerBoundCursor) {
    this.lowerBoundCursor = lowerBoundCursor;
  }

  public ZonedDateTime getUpperBoundCursor() {
    return upperBoundCursor;
  }

  public void setUpperBoundCursor(ZonedDateTime upperBoundCursor) {
    this.upperBoundCursor = upperBoundCursor;
  }
}
