package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.Comment;

/**
 * Created by bogdan.feraru on 4/22/2017.
 */
public interface CommentRepository extends GraphRepository<Comment> {

  @Query("MATCH (comment:COMMENT) WHERE comment.facebookId = {0} RETURN comment;")
  Comment findByFacebookId(String facebookId);
}
