package ro.ace.ucv.dcti.socialAnalyser.dataCollector.persistence.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import ro.ace.ucv.dcti.socialAnalyser.dataCollector.graph.domain.ReactionType;

/**
 * Created by bogdan.feraru on 6/14/2017.
 */
public interface ReactionTypeRepository extends Neo4jRepository<ReactionType, Long> {

  @Query("MATCH (reactionType:REACTION_TYPE) WHERE reactionType.name={0} RETURN reactionType;")
  ReactionType findByName(String name);
}
